import { TestBed } from '@angular/core/testing';

import { ReportBuilderService } from './report-builder.service';

describe('ReportBuilderService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: ReportBuilderService = TestBed.get(ReportBuilderService);
        expect(service).toBeTruthy();
    });
});
