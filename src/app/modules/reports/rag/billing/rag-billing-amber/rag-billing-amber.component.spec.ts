import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RagBillingAmberComponent } from './rag-billing-amber.component';

describe('RagBillingAmberComponent', () => {
    let component: RagBillingAmberComponent;
    let fixture: ComponentFixture<RagBillingAmberComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RagBillingAmberComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RagBillingAmberComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
