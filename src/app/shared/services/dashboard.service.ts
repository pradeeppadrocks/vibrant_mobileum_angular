import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';
import { YearChart } from '../models/dashboard.model';
import { environment } from '../../../environments/environment';
import { RegionChangeService } from './region-change.service';

@Injectable()
export class DashboardService {
    region;

    DevURL = environment.host;
    getDashboardRevenueReportURL = `${this.DevURL}getDashboardRevenueReport`;
    getDashboardBillingReportURL = `${this.DevURL}getDashboardBillingReport`;
    getCurrentQtrRagURL = `${this.DevURL}getCurrentQtrRag`;

    // Billing
    getBillingHistoryViewURL = `${this.DevURL}getBillingHistoryView`;
    getBillingHistoryViewLOBURL = `${this.DevURL}getBillingHistoryViewLOB`;
    getBillingHistoryViewDashbordURL = `${this.DevURL}getBillingHistoryViewDashbord`;
    getBillingForcastedLOBURL = `${this.DevURL}getBillingForcastedLOB`;
    getBillingBacklogConverstionURL = `${this.DevURL}getBillingBacklogConverstion`;
    getBillingBacklogDependenciesURL = `${this.DevURL}getBillingBacklogDependencies`;
    getBillingEntityViewUrl = `${this.DevURL}getBillingEntityView`;
    getBillingRegionUrl = `${this.DevURL}getBillingRegionsView`;

    getBillingForcastedBacklogPieURL = `${this.DevURL}getBillingForcastedBacklogPie`;
    getBillingForcastedDependenciesPieURL = `${this.DevURL}getBillingForcastedDependenciesPie`;
    getBillingBacklogSummaryURL = `${this.DevURL}getBillingBacklogSummaryPie`;

    // Revenue
    getRevenueHistoryViewURL = `${this.DevURL}getRevenueHistoryView`;
    getRevenueHistoryViewDashbordURL = `${this.DevURL}getRevenueHistoryViewDashbord`;
    getRevenueHistoryViewLOBURL = `${this.DevURL}getRevenueHistoryViewLOB`;
    getRevenueRecuringURL = `${this.DevURL}getRevenueRecuring`;
    getRevenueForcastedLOBURL = `${this.DevURL}getRevenueForcastedLOB`;
    getRevenueForcastedBacklogPieURL = `${this.DevURL}getRevenueForcastedBacklogPie`;
    getRevenueForcastedDependencyPieURL = `${this.DevURL}getRevenueForcastedDependencyPie`;
    getRevenueForecastedProfileUrl = `${this.DevURL}getRevenueForcastedRevProfile`;

    getRevenueBacklogDependenciesURL = `${this.DevURL}getRevenueBacklogDependencies`;
    getRevenueBacklogConversionURL = `${this.DevURL}getRevenueBacklogConversion`;
    getRevenueBacklogPieURL = `${this.DevURL}getRevenueBacklogPie`;

    dashboardFilterUrl = `${this.DevURL}dashbordFilter`;

    constructor(
        private endpointService: EndpointService,
        private baseService: BaseService,
        private regionUpdateService: RegionChangeService
    ) {
        this.regionUpdateService.region.subscribe(value => {
            this.region = value;
        });
    }

    getDashboardRevenueReportGraph(body) {
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getDashboardRevenueReportURL, body);
    }

    getDashboardBillingReportGraph(body) {
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getDashboardBillingReportURL, body);
    }

    getBillingHistoryView(year: any) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getBillingHistoryViewURL, year);
    }

    getBillingEntityHistoryView(year: any) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getBillingEntityViewUrl, year);
    }

    getBillingRegionHistoryView(year: any) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getBillingRegionUrl, year);
    }

    getBillingHistoryViewLOB(year: any) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getBillingHistoryViewLOBURL, year);
    }

    getBillingHistoryViewDashbord(year: YearChart) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getBillingHistoryViewDashbordURL, year);
    }

    getBillingForcastedLOB() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getBillingForcastedLOBURL, body);
    }

    getBillingBacklogConverstion() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getBillingBacklogConverstionURL, body);
    }

    getBillingBacklogDependencies() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getBillingBacklogDependenciesURL, body);
    }

    getBillingForcastedBacklogPie() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getBillingForcastedBacklogPieURL, body);
    }

    getBillingForcastedDependenciesPie() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getBillingForcastedDependenciesPieURL, body);
    }

    getBillingBacklogSummaryPieUrl() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getBillingBacklogSummaryURL, body);
    }

    // Revenue API

    getRevenueHistoryView(year: YearChart) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getRevenueHistoryViewURL, year);
    }

    getRevenueHistoryViewDashbord(year: YearChart) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getRevenueHistoryViewDashbordURL, year);
    }

    getRevenueHistoryViewLOB(year: YearChart) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getRevenueHistoryViewLOBURL, year);
    }

    getRevenueRecuring(year: YearChart) {
        if (this.region) {
            year.regions = this.region;
        }
        return this.baseService.post(this.getRevenueRecuringURL, year);
    }

    getRevenueForcastedLOB() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getRevenueForcastedLOBURL, body);
    }

    getRevenueForcastedBacklogPie() {
        return this.baseService.post(this.getRevenueForcastedBacklogPieURL);
    }

    getRevenueForcastedDependencyPie() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getRevenueForcastedDependencyPieURL, body);
    }

    getRevenueBacklogDependencies() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getRevenueBacklogDependenciesURL, body);
    }

    getRevenueBacklogConversion() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getRevenueBacklogConversionURL, body);
    }

    getRevenueBacklogPie() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getRevenueBacklogPieURL, body);
    }

    getDashboardChartFiterData() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.dashboardFilterUrl, body);
    }

    getRevenueForcastedProfile() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getRevenueForecastedProfileUrl, body);
    }

    getCurrentQtrRagViewData() {
        const body = {};
        if (this.region) {
            body['regions'] = this.region;
        }
        return this.baseService.post(this.getCurrentQtrRagURL, body);
    }
}
