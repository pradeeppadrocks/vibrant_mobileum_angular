import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Region, EditRegion, DeleteMasterdata } from './../master.model';
import { MasterService } from '../master.service';
import { ToasterService } from '../../../shared';
import { DataTableDirective } from 'angular-datatables';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-master-region',
    templateUrl: './region.component.html',
    styleUrls: ['./region.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class RegionComponent implements OnInit, AfterViewInit {
    @ViewChild('editregion') editRegionpopup: ModalDirective;
    @ViewChild('confirmdialog') confirmDialog: ModalDirective;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

    dtOptions: any = {};
    regionList: any[];
    pageIndex = 0;
    pageLength = 10;
    deleteId = '';

    regionForm: FormGroup;
    regionname: FormControl;
    regId: FormControl;
    status: FormControl;
    isFormSubmitted = false;

    isSearched;
    searchData;

    constructor(
        private http: HttpClient,
        private masterservice: MasterService,
        private ngxService: NgxUiLoaderService,
        private toasterservice: ToasterService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.gridBinding();
        this.createFormControls();
        this.createFormDetails();
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata: Region = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    regId: ''
                };

                that.masterservice.getRegionList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.regionList = pagedData.data.regions;

                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.regionList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'reg_id', orderable: false },
                { data: 'reg_name' },
                { data: 'reg_created_date' },
                { data: 'reg_updated_date' },
                { data: 'moredetails', orderable: false }
            ]
        };
    }

    createFormControls() {
        this.regionname = new FormControl('', [Validators.required]);
        this.regId = new FormControl('');
        this.status = new FormControl('');
    }

    createFormDetails() {
        this.regionForm = new FormGroup({
            regionname: this.regionname,
            regId: this.regId,
            status: this.status
        });
    }

    addRegion() {
        this.regId.setValue('');
        this.regionname.setValue('');
        this.status.setValue(1);
        this.editRegionpopup.show();
    }

    editRegion(data) {
        this.regId.setValue(data.reg_id);
        this.regionname.setValue(data.reg_name);
        this.status.setValue(1);
        this.editRegionpopup.show();
    }

    addUpdateRegion() {
        if (this.regionForm.valid) {
            this.isFormSubmitted = false;
            this.ngxService.start();
            this.masterservice.addUpdateRegion(this.regionForm.value).subscribe(
                result => {
                    if (result && result.success) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                        this.cancelRegion();
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    cancelRegion() {
        this.regionForm.reset();
        this.editRegionpopup.hide();
        this.regId.setValue('');
        this.status.setValue('');
        this.regionname.setValue('');
    }

    openDeletePopup(id) {
        this.status.setValue(0);
        this.deleteId = id;
        this.confirmDialog.show();
    }

    cancelDelete() {
        this.deleteId = '';
        this.status.setValue('');
        this.confirmDialog.hide();
    }

    deleteRegion() {
        const data: DeleteMasterdata = {
            deleteArray: [this.deleteId],
            status: this.status.value
        };
        this.ngxService.start();
        this.masterservice.deleteRegion(data).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    this.cancelDelete();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
