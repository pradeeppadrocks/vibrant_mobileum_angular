import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperWorkComponent } from './paper-work.component';

describe('PaperWorkComponent', () => {
    let component: PaperWorkComponent;
    let fixture: ComponentFixture<PaperWorkComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PaperWorkComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaperWorkComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
