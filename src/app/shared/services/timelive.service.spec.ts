import { TestBed } from '@angular/core/testing';

import { TimeliveService } from './timelive.service';

describe('TimeliveService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: TimeliveService = TestBed.get(TimeliveService);
        expect(service).toBeTruthy();
    });
});
