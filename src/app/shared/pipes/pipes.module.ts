import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitleCasePipe } from './title-case.pipe';
import { DistinctValuePipe } from './distinct-value.pipe';
import { UnderscoreReplacePipe } from './underscore-replace.pipe';

@NgModule({
    declarations: [TitleCasePipe, DistinctValuePipe, UnderscoreReplacePipe],
    imports: [CommonModule],
    exports: [TitleCasePipe, DistinctValuePipe, UnderscoreReplacePipe]
})
export class PipesModule {}
