import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityCompanyCodeComponent } from './entity-company-code.component';

describe('EntityCompanyCodeComponent', () => {
    let component: EntityCompanyCodeComponent;
    let fixture: ComponentFixture<EntityCompanyCodeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EntityCompanyCodeComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EntityCompanyCodeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
