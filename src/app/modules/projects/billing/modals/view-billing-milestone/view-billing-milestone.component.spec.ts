import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBillingMilestoneComponent } from './view-billing-milestone.component';

describe('ViewBillingMilestoneComponent', () => {
    let component: ViewBillingMilestoneComponent;
    let fixture: ComponentFixture<ViewBillingMilestoneComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ViewBillingMilestoneComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ViewBillingMilestoneComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
