export class Revenue {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    revYear: string;
    revMonth: string;
    revAmount: any;
    revProjId: string;
}

export class ProjectDetails {
    projId: any;
}

export class ImportRev {
    fileValue: string;
    projId: string;
}
