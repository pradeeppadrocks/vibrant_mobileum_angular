import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BillingModule } from './billing/billing.module';
import { RevenueModule } from './revenue/revenue.module';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { SharedModule } from '../../shared/shared-module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReportsModule } from './reports/reports.module';
import { DealDistributionModule } from './deal-distribution/deal-distribution.module';
import { SwDepComponent } from './SWDep/sw-dep/sw-dep.component';
import { TimeliveComponent } from './timelive/timelive/timelive.component';
import { ProjectService } from './project.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProjectSummaryComponent } from './project-summary/project-summary.component';
import { EditProjectModalComponent } from './modals/edit-project-modal/edit-project-modal.component';

@NgModule({
    declarations: [
        ProjectsComponent,
        ProjectDetailsComponent,
        SwDepComponent,
        TimeliveComponent,
        ProjectSummaryComponent,
        EditProjectModalComponent
    ],
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        BillingModule,
        RevenueModule,
        ReportsModule,
        DealDistributionModule,
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ProjectsRoutingModule,
        SharedModule
    ],
    exports: [BillingModule, RevenueModule, ProjectsRoutingModule, DealDistributionModule, ReportsModule],
    providers: [ProjectService],
    entryComponents: [EditProjectModalComponent]
})
export class ProjectsModule {}
