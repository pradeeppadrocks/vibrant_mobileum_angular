import { AfterViewInit, Component, ElementRef, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { SessionService } from 'src/app/core';
import { ToasterService } from '../../services/toaster.service';
import { Router } from '@angular/router';
import { RegionChangeService } from '../..';

@Component({
    selector: 'app-top-nav',
    templateUrl: './top-nav.component.html',
    styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit, AfterViewInit {
    @ViewChild('regionSelect') regionSelect: ElementRef;

    constructor(
        private sessionService: SessionService,
        public customToasterService: ToasterService,
        private router: Router,
        private regionUpdateService: RegionChangeService
    ) {}

    regionList;
    regions;

    ngOnInit() {
        this.getRegionListOfUser();
    }

    logout() {
        this.sessionService.clearSession();
        this.customToasterService.showSuccess('You are successfully logged out');
        this.router.navigate(['', 'auth', 'login']);
    }

    getRegionListOfUser() {
        this.regionList = this.sessionService.getSession().data.regions;
        // this.regions = this.regionList;
    }

    regionChanged(regions) {
        const selectedRegion = this.filterRegionIdFromSelectedRegionList(regions);
        this.regionUpdateService.updateRegion(selectedRegion);
    }

    filterRegionIdFromSelectedRegionList(selectedRegions) {
        const regId = [];
        selectedRegions.forEach(item => {
            regId.push(item.reg_id);
        });
        return regId.toString();
    }

    ngAfterViewInit() {}

    maxSelection(regions) {
        if (regions && regions.length >= 5) {
            this.customToasterService.warning('Max selection reached');
        }
    }
}
