export class ConversionType {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    conversionTypeId: any;
}

export class EditConversionType {
    conversionTypeId: any;
    conersionTypeName: string;
    status: any;
}

export class DeleteMasterdata {
    deleteArray: any;
    status: any;
}

export class LineOfBusiness {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    lineOfBusinessId: any;
}

export class EditLineOfBusiness {
    lineOfBusinessId: any;
    lineOfBusinessName: string;
    status: any;
}

export class Milestone {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    milestoneId: any;
}

export class EditMilestone {
    milestoneId: any;
    milestoneTypeName: string;
    status: any;
}

export class Opportunity {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    opportunityId: any;
}

export class EditOpportunity {
    opportunityId: any;
    opportunityName: string;
    status: any;
}

export class PMO {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    pdtId: any;
}

export class EditPMO {
    pdtId: any;
    pdtName: string;
    status: any;
}

export class Module {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    moduleId: any;
}

export class EditModule {
    moduleId: any;
    moduleName: string;
    status: any;
}

export class Region {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    regId: any;
}

export class EditRegion {
    regId: any;
    regionname: string;
    status: any;
}

export class UserType {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    userTypeId: any;
}

export class EditUserType {
    userTypeId: any;
    userTypeName: string;
    userDefaultAccess: any;
    status: any;
}

export class User {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    userId: any;
}

export class EditUser {
    userId: any;
    status: any;
    username: string;
    password: string;
    name: string;
    mobile: string;
    email: string;
    userType: string;
    regId: any;
}

export class SFDCUsers {
    searchKey: string;
}

export class DealType {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    dealTypeId: any;
}

export class EditDealType {
    dealTypeId: any;
    dealType: string;
    status: any;
}

export class ProjType {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    projTypeId: any;
}

export class EditProjType {
    projTypeId: any;
    projType: string;
    status: any;
}

export class ProjDependency {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    dependencyId: any;
}

export class EditProjDependency {
    dependencyId: any;
    dependencyName: string;
    status: any;
}
