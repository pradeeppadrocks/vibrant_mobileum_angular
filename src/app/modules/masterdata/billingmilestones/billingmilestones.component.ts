import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Milestone, EditMilestone, DeleteMasterdata } from './../master.model';
import { MasterService } from '../master.service';
import { ToasterService } from '../../../shared';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-master-billingmilestones',
    templateUrl: './billingmilestones.component.html',
    styleUrls: ['./billingmilestones.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class BillingMilestoneComponent implements OnInit, AfterViewInit {
    @ViewChild('editmilestone') editMilestonepopup: ModalDirective;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    @ViewChild('confirmdialog') confirmDialog: ModalDirective;

    dtOptions: any = {};
    MilestoneList: any[];
    pageIndex = 0;
    pageLength = 10;
    deleteId = '';

    milestoneForm: FormGroup;
    milestoneTypeName: FormControl;
    milestoneId: FormControl;
    status: FormControl;
    isFormSubmitted = false;

    isSearched;
    searchData;

    constructor(
        private http: HttpClient,
        private masterservice: MasterService,
        private ngxService: NgxUiLoaderService,
        private toasterservice: ToasterService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.gridBinding();
        this.createFormControls();
        this.createFormDetails();
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata: Milestone = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    milestoneId: ''
                };
                that.masterservice.getMilestonesTypeList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.MilestoneList = pagedData.data.milestones;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.MilestoneList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'bmt_id', orderable: false },
                { data: 'bmt_name' },
                { data: 'bmt_created_date' },
                { data: 'bmt_updated_date' },
                { data: 'moredetails', orderable: false }
            ]
        };
    }

    createFormControls() {
        this.milestoneTypeName = new FormControl('', [Validators.required]);
        this.milestoneId = new FormControl('');
        this.status = new FormControl('');
    }

    createFormDetails() {
        this.milestoneForm = new FormGroup({
            milestoneTypeName: this.milestoneTypeName,
            milestoneId: this.milestoneId,
            status: this.status
        });
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    addMilestone() {
        this.milestoneId.setValue('');
        this.milestoneTypeName.setValue('');
        this.status.setValue(1);
        this.editMilestonepopup.show();
    }

    editMilestone(data) {
        this.milestoneId.setValue(data.bmt_id);
        this.milestoneTypeName.setValue(data.bmt_name);
        this.status.setValue(1);
        this.editMilestonepopup.show();
    }

    addUpdateMilestone() {
        if (this.milestoneForm.valid) {
            this.isFormSubmitted = false;
            this.ngxService.start();
            this.masterservice.addUpdateMilestonesType(this.milestoneForm.value).subscribe(
                result => {
                    if (result && result.success) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                        this.cancelMilestone();
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }

    cancelMilestone() {
        this.milestoneTypeName.setValue('');
        this.milestoneId.setValue('');
        this.status.setValue('');
        this.milestoneForm.reset();
        this.editMilestonepopup.hide();
    }

    openDeletePopup(id) {
        this.deleteId = id;
        this.status.setValue(0);
        this.confirmDialog.show();
    }

    cancelDelete() {
        this.deleteId = '';
        this.status.setValue('');
        this.confirmDialog.hide();
    }

    deleteMilestone() {
        const data: DeleteMasterdata = {
            deleteArray: [this.deleteId],
            status: this.status.value
        };
        this.ngxService.start();
        this.masterservice.deleteMilestonesType(data).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    this.cancelDelete();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    /*keyDownFunction(event, x) {
    if (event.keyCode === 13) {
        this.editMilestone(x);
      // rest of your code
    }
  }*/

    OnFocusOut() {
        const currentElement = document.getElementById('edit'); // ID set by OnFOcusIn
        const curIndex = currentElement.tabIndex; // get current elements tab index
        currentElement.focus(); // if it's the one we want, focus it and exit the loop
    }
}
