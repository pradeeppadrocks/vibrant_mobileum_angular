import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { BaseService, EndpointService, RegionChangeService, ReportBuilderService, ToasterService } from '../../../../../shared';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HttpHeaders } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../../masterdata/master.model';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-view-report',
    templateUrl: './view-report.component.html',
    styleUrls: ['./view-report.component.css', '../../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class ViewReportComponent implements OnInit, OnDestroy, AfterViewInit {
    reportId;
    reportName;
    reportData: any[];

    keys;
    regions = '';
    subscriptions;
    reloadTableOnRegionChange = true;

    @ViewChild('example-datatable') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    dtOptions: any = {};

    pageIndex = 0;
    pageLength = 10;

    constructor(
        private customToasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private reportBuilderService: ReportBuilderService,
        private baseService: BaseService,
        private endpointService: EndpointService,
        private fileSaverService: FileSaverService,
        private route: ActivatedRoute,
        private router: Router,
        private regionUpdateService: RegionChangeService
    ) {}

    ngOnInit() {
        this.regions = this.reportBuilderService.getUpdatedRegion();
        this.subscriptions = this.regionUpdateService.region.subscribe(value => {
            this.regions = value;
            this.reRenderReportListingTable();
        });

        this.reportId = this.route.snapshot.paramMap.get('id');
        if (this.reportId) {
            this.gridBinding();
        }
    }

    reloadTable() {
        this.reloadTableOnRegionChange = true;
        this.gridBinding();
    }

    ngAfterViewInit() {
        this.dtTrigger.next();
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    getReportdata() {
        /* // TODO: later need to send remarks to api
        this.ngxService.start();
        this.reportBuilderService.getReportDetailsOnView(this.reportId, this.regions).subscribe((res: any) => {
            if (res.success) {
                this.reportData = res.data.CustomReportData;
                this.reportName = res.data.reportName;
                this.keys = Object.keys(res.data.CustomReportData[0]);
                this.ngxService.stop();
            } else {
                this.reportData = [];
                this.customToasterservice.error(res.message);
                this.ngxService.stop();
            }
        });*/
    }

    gridBinding() {
        this.dtOptions = {
            searching: false,
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;

                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort: '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : -1,
                    // searchKey: dataTablesParameters.search.value,
                    reportId: this.reportId,
                    regions: this.regions,
                    xlsExport: false
                };
                this.reportBuilderService.getReportDetailsOnView(temdata).subscribe((res: any) => {
                    if (res.success) {
                        this.reportData = res.data.CustomReportData;
                        this.reportName = res.data.reportName;
                        this.keys = Object.keys(res.data.CustomReportData[0]);
                        callback({
                            recordsTotal: res.data.totalCount,
                            recordsFiltered: res.data.totalCount,
                            data: []
                        });
                    } else {
                        this.reportData = [];
                        this.customToasterservice.error(res.message);
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                //  { data: 'Opportunity_Name', orderable: false }
            ]
        };
    }

    typeof(data) {
        return typeof data;
    }

    export() {
        const body = {
            reportId: this.reportId,
            pageSize: 10,
            pageNo: 1,
            order: 1,
            sort: '',
            regions: this.regions,
            xlsExport: true
        };
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        this.baseService
            .post(this.endpointService.getReportDetailsViewUrl(), body, { responseType: 'arraybuffer', headers })
            .subscribe((res: any) => {
                this.fileSaverService.save(
                    new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
                    this.reportName
                );
            });
    }

    backToReportBuilder() {
        this.router.navigate(['', 'reports', 'builder']);
    }

    reRenderReportListingTable() {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.draw();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        if (!!this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }
}
