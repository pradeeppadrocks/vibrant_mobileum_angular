import { TestBed } from '@angular/core/testing';

import { ChartExportService } from './chart-export.service';

describe('ChartExportService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: ChartExportService = TestBed.get(ChartExportService);
        expect(service).toBeTruthy();
    });
});
