import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project, Company, Region, ConversionType } from './../project.model';
import { ProjectService } from '../project.service';
import { RegionChangeService, ToasterService } from '../../../shared';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DataTableDirective } from 'angular-datatables';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { ContentType } from '@angular/http/src/enums';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SessionService } from '../../../core';

@Component({
    selector: 'app-projects',
    templateUrl: './projects.component.html',
    styleUrls: ['./projects.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class ProjectsComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('editprojectmodel') public editprojectmodel: ModalDirective;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

    dtOptions: any = {};
    projectList: any[];
    companyList: any = [];
    regionList: any = [];
    quarterList: any = [{ id: 1, value: 'Q1' }, { id: 2, value: 'Q2' }, { id: 3, value: 'Q3' }, { id: 4, value: 'Q4' }];
    yearList: any = [];
    contentTypeList: any = [];
    pageIndex = 0;
    pageLength = 10;

    projectfilterform: FormGroup;
    // project: FormControl;
    region: FormControl;
    year: FormControl;
    quarter: FormControl;
    company: FormControl;
    // lob: FormControl;

    editProjectForm: FormGroup;
    projId: FormControl;
    dealTypeId: FormControl;
    conversionTypeId: FormControl;
    dealWarranty: FormControl;
    dealWarrantyStartDate: FormControl;
    uatDate: FormControl;
    faDate: FormControl;
    entityCompanyCode: FormControl;
    revenueProfile: FormControl;

    isSearched;
    searchData;

    selectedRegions;
    userRegionList;

    updatedRegionSelect = '';
    subscription;

    constructor(
        private http: HttpClient,
        private projectservice: ProjectService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private regionUpdateService: RegionChangeService,
        private sessionService: SessionService
    ) {
        this.updatedRegionSelect = this.projectservice.getUpdatedRegion();
    }

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.subscription = this.regionUpdateService.region.subscribe(value => {
            this.updatedRegionSelect = value;
            this.refreshPage();
        });

        this.getRegionListOfUser();
        this.gridBinding();
        this.pageLoadBinding();
        this.createFormControls();
        this.createFormDetails();
    }

    refreshPage() {
        this.rerenderGrid();
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [[3, 'desc']],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                // dataTablesParameters.start = this.pageLength * this.pageIndex;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    projDealYear: this.year.value ? this.year.value : '',
                    projDealQtr: this.quarter.value ? this.quarter.value : '',
                    companyId: this.company.value ? this.company.value : '',
                    regId: this.region.value ? this.region.value : '',
                    regions: this.updatedRegionSelect ? this.updatedRegionSelect : '',
                    projId: ''
                };
                that.projectservice.getProjectList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.projectList = pagedData.data.projects;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.projectList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'Sl No.', orderable: false },
                { data: 'projOppID' },
                { data: 'projCode' },
                { data: 'sfdcAccName' },
                { data: 'proj_updated_date', orderable: true },
                { data: 'dealAmount' },
                { data: 'moredetails', orderable: false }
            ]
        };
    }

    getUniqueCompany(companyList) {
        const resArr = [];
        companyList.filter(item => {
            const i = resArr.findIndex(x => x.companyName === item.companyName);
            if (i <= -1) {
                resArr.push(item);
            }
        });
        return resArr;
    }

    getRegionListOfUser() {
        this.userRegionList = this.sessionService.getSession().data.regions;
        this.selectedRegions = this.filterRegionIdFromSelectedRegionList(this.userRegionList);
    }

    filterRegionIdFromSelectedRegionList(selectedRegions) {
        const regId = [];
        selectedRegions.forEach(item => {
            regId.push(item.reg_id);
        });
        return regId;
    }

    getExistingRegionsForUser(regionList) {
        const resArr = [];
        regionList.filter(item => {
            const i = this.selectedRegions.findIndex(x => x === item.regId);
            if (i <= -1) {
                item.disabled = true;
                resArr.push(item);
            } else {
                item.disabled = false;
                resArr.push(item);
            }
        });
        return resArr;
    }

    pageLoadBinding() {
        // Get RegionList
        this.ngxService.start();
        const regdata: Region = {
            searchKey: ''
        };
        this.projectservice.getRegionList(regdata).subscribe(
            result => {
                if (result.success && result.data) {
                    if (this.selectedRegions) {
                        this.regionList = this.getExistingRegionsForUser(result.data.region);
                    } else {
                        this.regionList = [];
                    }
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );

        // Get CompanyList
        const comdata: Company = {
            searchKey: ''
        };

        this.projectservice.getCompanyList(comdata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.companyList = this.getUniqueCompany(result.data.company);
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );

        // Get Content Type List
        const conversiondata: ConversionType = {
            pageNo: 1,
            pageSize: 1000,
            sort: '',
            order: '',
            searchKey: '',
            conversionTypeId: ''
        };
        this.projectservice.getConversionType(conversiondata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.contentTypeList = result.data.conversions;
                    // result.data.company.forEach(element => {
                    //  this.contentTypeList = [...this.companyList, element];
                    // });
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
        // Year Binding
        const lastYear = 2002;
        const today = new Date();
        const year = today.getFullYear();
        for (let i = year; i >= lastYear; i--) {
            const yearData = { id: i, value: i };
            this.yearList = [...this.yearList, yearData];
        }
    }

    createFormControls() {
        // this.project = new FormControl(null);
        this.region = new FormControl(null);
        this.year = new FormControl(null);
        this.quarter = new FormControl(null);
        this.company = new FormControl(null);
        // this.lob = new FormControl(null);

        this.projId = new FormControl('');
        this.dealTypeId = new FormControl('');
        this.conversionTypeId = new FormControl('');
        this.dealWarranty = new FormControl('');
        this.dealWarrantyStartDate = new FormControl('');
        this.uatDate = new FormControl('');
        this.faDate = new FormControl('');
        this.entityCompanyCode = new FormControl('');
        this.revenueProfile = new FormControl('');
    }

    createFormDetails() {
        this.projectfilterform = new FormGroup({
            // project: this.project,
            region: this.region,
            year: this.year,
            quarter: this.quarter,
            company: this.company
            // lob: this.lob
        });

        this.editProjectForm = new FormGroup({
            projId: this.projId,
            dealTypeId: this.dealTypeId,
            conversionTypeId: this.conversionTypeId,
            dealWarranty: this.dealWarranty,
            dealWarrantyStartDate: this.dealWarrantyStartDate,
            uatDate: this.uatDate,
            faDate: this.faDate,
            entityCompanyCode: this.entityCompanyCode,
            revenueProfile: this.revenueProfile
        });
    }

    filterProject() {
        if (this.region.value || this.year.value || this.quarter.value || this.company.value) {
            this.pageIndex = 0;
            this.pageLength = 10;
            this.rerenderGrid();
        }
    }

    resetProjectFilter() {
        if (this.region.value || this.year.value || this.quarter.value || this.company.value) {
            this.pageIndex = 0;
            this.pageLength = 10;
            this.rerenderGrid();
        }
        this.region.setValue(null);
        this.year.setValue(null);
        this.quarter.setValue(null);
        this.company.setValue(null);
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        // to resolve object unsbuscription error
        if (!!this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    editProjectModelOpen(data) {
        this.projId.setValue(data.projId);
        this.dealTypeId.setValue('');
        this.conversionTypeId.setValue(data.projConversionType ? data.projConversionType : '');
        this.dealWarranty.setValue(false);
        this.dealWarrantyStartDate.setValue(data.projDealWarrantyStartDate);
        this.uatDate.setValue(data.projUatDate);
        this.faDate.setValue(data.projFaDate);
        this.entityCompanyCode.setValue('');
        this.revenueProfile.setValue('');
        this.editprojectmodel.show();
    }

    cancelEditProject() {
        this.projId.setValue('');
        this.dealTypeId.setValue('');
        this.conversionTypeId.setValue('');
        this.dealWarranty.setValue('');
        this.dealWarrantyStartDate.setValue('');
        this.uatDate.setValue('');
        this.faDate.setValue('');
        this.entityCompanyCode.setValue('');
        this.revenueProfile.setValue('');
        this.editProjectForm.reset();
        this.editprojectmodel.hide();
    }

    UpdateProject() {
        if (this.editProjectForm.value.projId) {
            this.ngxService.start();
            this.editProjectForm.value.dealWarranty = this.editProjectForm.value.dealWarranty ? 1 : 0;
            this.projectservice.updateProjectDetail(this.editProjectForm.value).subscribe(
                result => {
                    if (result && result.success) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                        this.cancelEditProject();
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        }
    }

    importFile($event, id): void {
        this.readThis($event.target, id);
    }

    readThis(inputValue: any, id): void {
        const file: File = inputValue.files[0];
        if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            this.toasterservice.error('Only xlsx files are supported.');
            return;
        }
        const myReader: FileReader = new FileReader();

        myReader.onloadend = e => {
            const strresult = myReader.result.toString();
            const base64 = strresult.split('base64,');
            const prodata = {
                fileValue: base64[1]
            };
            this.ngxService.start();
            this.projectservice.importAllMilestone(prodata).subscribe(
                result => {
                    if (result.success && result.data) {
                        this.toasterservice.showSuccess(result.message);
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    document.getElementById(id)['value'] = '';
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
            // this.image = myReader.result;
        };
        myReader.readAsDataURL(file);
    }
}
