import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, Input, ViewChildren, QueryList } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { BaseService, EndpointService, ReportsService } from '../../../../shared';
import { HttpHeaders } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class ReportsComponent implements OnInit, AfterViewInit {
    @Input() projectDetails;
    @ViewChildren(DataTableDirective)
    dtElements: QueryList<any>;

    dtOptions: any[] = [];

    persons: any[] = [];
    billingBacklog: any[];
    pageIndex1 = 0;
    pageLength1 = 10;

    revenueBacklog: any[];
    pageIndex2 = 0;
    pageLength2 = 10;

    currentYear = new Date().getFullYear();
    totalBillingLastYear = 'totalBilling' + (this.currentYear - 1);
    billingBacklogLastYear = 'billingBacklog' + (this.currentYear - 1);
    billingCurrentYear = 'totalBilling' + this.currentYear;
    billingNextYear = 'totalBilling' + (this.currentYear + 1);
    biliingAfter2yr = 'totalBilling' + (this.currentYear + 2);
    currentQtr = 'Q' + this.getCurrentQtr();
    totalBillingCrntQtr = 'totalBilling' + this.currentQtr;

    totalBacklogLastYr = 'totalBacklog' + (this.currentYear - 1);
    totalBacklogCrntYr = 'totalBacklog' + this.currentYear;
    mntBacklogLastYr = 'mntBacklog' + (this.currentYear - 1);
    mntBacklogCrntYr = 'mntBacklog' + this.currentYear;

    constructor(
        private reportService: ReportsService,
        private fileSaverService: FileSaverService,
        private baseService: BaseService,
        private endpointService: EndpointService
    ) {}

    ngAfterViewInit() {
        // This is to get table instance
        // Get event when page number is changed

        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                dtInstance.on('page.dt', () => {
                    const pageInfo = dtInstance.page.info();
                    dtInstance.page.len();
                    if (index === 0) {
                        this.pageIndex1 = pageInfo.page;
                        this.pageLength1 = pageInfo.length;
                    } else if (index === 1) {
                        this.pageIndex2 = pageInfo.page;
                        this.pageLength2 = pageInfo.length;
                    }
                });
            });

            dtElement.dtInstance.then((dtInstance: any) => {
                dtInstance.on('length.dt', (e, settings, len) => {
                    const pageInfo = dtInstance.page.info();
                    if (index === 0) {
                        this.pageIndex1 = pageInfo.page;
                        this.pageLength1 = pageInfo.length;
                    } else if (index === 1) {
                        this.pageIndex2 = pageInfo.page;
                        this.pageLength2 = pageInfo.length;
                    }
                });
            });
        });
    }

    ngOnInit() {
        this.renderBillingBacklog();
        this.renderRevenueBacklog();
    }

    getCurrentQtr() {
        const today = new Date();
        return Math.floor((today.getMonth() + 3) / 3);
    }

    renderBillingBacklog() {
        this.dtOptions[0] = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                const temdata = {
                    pageNo: this.pageIndex1 + 1,
                    pageSize: this.pageLength1,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    projId: this.projectDetails.projId
                };

                this.reportService.getBillingBacklogWaterfallReportData(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.billingBacklog = pagedData.data.billingWaterFall.projectData;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.billingBacklog = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: []
        };
    }

    renderRevenueBacklog() {
        this.dtOptions[1] = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            searching: false,
            paging: false,
            info: false,
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                const temdata = {
                    pageNo: this.pageIndex2 + 1,
                    pageSize: this.pageLength2,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    projId: this.projectDetails.projId
                };

                this.reportService.getRevenueBacklogWaterfallReportData(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.revenueBacklog =
                            pagedData.data.revenueWaterFall.projectData.length > 0 ? pagedData.data.revenueWaterFall.projectData : [];
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.revenueBacklog = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [{ data: 'oppId', orderable: false }]
        };
    }

    exportBilling() {
        const body = {
            projId: this.projectDetails.projId
        };
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        this.baseService
            .post(this.endpointService.getBillingwaterfallExportUrl(), body, { responseType: 'arraybuffer', headers })
            .subscribe((res: any) => {
                this.fileSaverService.save(
                    new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
                    this.projectDetails.projectCode + ' Billing Report.xlsx'
                );
            });
    }

    exportRevenue() {
        const body = {
            projId: this.projectDetails.projId
        };
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        this.baseService
            .post(this.endpointService.getRevenueWaterfallExportUrl(), body, { responseType: 'arraybuffer', headers })
            .subscribe((res: any) => {
                this.fileSaverService.save(
                    new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
                    this.projectDetails.projectCode + ' Revenue Report.xlsx'
                );
            });
    }
}
