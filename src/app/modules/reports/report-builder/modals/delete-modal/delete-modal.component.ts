import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../../../../shared/services/billing.service';
import { ReportBuilderService, ToasterService } from '../../../../../shared';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-delete-modal',
    templateUrl: './delete-modal.component.html',
    styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
    @Input() reportId;

    constructor(
        public activeModal: NgbActiveModal,
        private customToasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private modalService: NgbModal,
        config: NgbModalConfig,
        private reportBuilderService: ReportBuilderService
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {}

    deleteReport() {
        // TODO: later need to send remarks to api
        this.ngxService.start();
        this.reportBuilderService.deleteReport(this.reportId).subscribe((res: any) => {
            if (res.success) {
                this.customToasterservice.showSuccess(res.message);
                this.activeModal.close(res);
                this.ngxService.stop();
            } else {
                this.customToasterservice.error(res.message);
                this.ngxService.stop();
            }
        });
    }
}
