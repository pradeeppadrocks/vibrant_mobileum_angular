import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared-module';
import { DataTableComponent } from './data-table/data-table.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BillingHistoricalViewComponent } from './dashboard-views/billing-historical-view/billing-historical-view.component';
import { RevenueHistoricalViewComponent } from './dashboard-views/revenue-historical-view/revenue-historical-view.component';
import { BillingForecastViewComponent } from './dashboard-views/billing-forecast-view/billing-forecast-view.component';
import { RevenueForecastViewComponent } from './dashboard-views/revenue-forecast-view/revenue-forecast-view.component';
import { BillingBacklogViewComponent } from './dashboard-views/billing-backlog-view/billing-backlog-view.component';
import { RevenueBacklogViewComponent } from './dashboard-views/revenue-backlog-view/revenue-backlog-view.component';
import { ApprovalComponent } from './approval/approval.component';
import { ApproveModalComponent } from './approve-modal/approve-modal.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
    declarations: [
        HomeComponent,
        DashboardComponent,
        SideNavComponent,
        TopNavComponent,
        DataTableComponent,
        BillingHistoricalViewComponent,
        RevenueHistoricalViewComponent,
        BillingForecastViewComponent,
        RevenueForecastViewComponent,
        BillingBacklogViewComponent,
        RevenueBacklogViewComponent,
        ApprovalComponent,
        ApproveModalComponent,
        UserProfileComponent,
        FooterComponent
    ],
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        RouterModule,
        SharedModule,
        FormsModule,
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot()
    ],
    entryComponents: [ApproveModalComponent]
})
export class CommonComponentModule {}
