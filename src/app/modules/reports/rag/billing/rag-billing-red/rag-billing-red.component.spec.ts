import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RagBillingRedComponent } from './rag-billing-red.component';

describe('RagBillingRedComponent', () => {
    let component: RagBillingRedComponent;
    let fixture: ComponentFixture<RagBillingRedComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RagBillingRedComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RagBillingRedComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
