import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingForecastViewComponent } from './billing-forecast-view.component';

describe('BillingForecastViewComponent', () => {
    let component: BillingForecastViewComponent;
    let fixture: ComponentFixture<BillingForecastViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BillingForecastViewComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BillingForecastViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
