import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenueForecastViewComponent } from './revenue-forecast-view.component';

describe('RevenueForecastViewComponent', () => {
    let component: RevenueForecastViewComponent;
    let fixture: ComponentFixture<RevenueForecastViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RevenueForecastViewComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RevenueForecastViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
