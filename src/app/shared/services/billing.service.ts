import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';
import { ForecastedMilestone } from '../models/billing.model';
import { HttpClient } from '@angular/common/http';
import { ImportDeal } from '../../modules/projects/deal-distribution/deal-distribution.model';

@Injectable({
    providedIn: 'root'
})
export class BillingService {
    constructor(private endpointService: EndpointService, private baseService: BaseService) {}

    getForecastedMilestone(data: ForecastedMilestone) {
        return this.baseService.post(this.endpointService.getForecastedMilestone(), data);
    }

    deleteMsData(data) {
        const body = {
            deleteArray: [data.milestoneId],
            status: data.status,
            bmRemarks: data.bmRemarks
        };
        return this.baseService.delete(this.endpointService.deleteMilestoneUrl, body);
    }

    getAllPojectDependency(searchKey: string) {
        const body = {
            searchKey
        };
        return this.baseService.post(this.endpointService.getAllPojectDependency(), body);
    }

    getProjectDetailsByProjectId(projId) {
        const body = {
            pageNo: 1,
            pageSize: 10,
            sort: '',
            order: '1',
            searchKey: '',
            projDealYear: '',
            projDealQtr: '',
            projId: projId,
            companyId: '',
            regId: ''
        };

        return this.baseService.post(this.endpointService.getProjectsUrl(), body);
    }

    addBillingMilestone(milestone, msType) {
        let url = '';
        if (msType === 0) {
            url = this.endpointService.getUpdatedPmsUrl();
        } else if (msType === 1) {
            url = this.endpointService.getUpdateForecastedUrl();
        } else {
            url = this.endpointService.getAddForecastedMilestoneUrl();
        }
        return this.baseService.post(url, milestone);
    }

    getMilestones() {
        const body = {
            searchKey: ''
        };
        return this.baseService.post(this.endpointService.getMilestoneUrl(), body);
    }

    finalizeMilestoneBasedOnProject(projectId, msType) {
        const body = {
            bmProjId: projectId
            /*
                  bmForcasted: msType
      */
        };
        return this.baseService.post(this.endpointService.getFinalizeMilestoneUrl(), body);
    }

    acceptMilestone(msId, levelType) {
        const body = {
            bmID: msId,
            level: levelType,
            sentlevel: levelType
        };

        return this.baseService.post(this.endpointService.getApproveMilestoneUrl(), body);
    }

    sendToLevel2(msId, levelType) {
        const body = {
            bmID: msId,
            level: levelType,
            sentlevel: 2 // always 2
        };

        return this.baseService.post(this.endpointService.getApproveMilestoneUrl(), body);
    }

    rejectMilestoneLevel(msId, levelType) {
        const body = {
            bmID: msId,
            level: levelType
        };

        return this.baseService.post(this.endpointService.getRejectMilestoneUrl(), body);
    }

    importBmMilestone(data: ImportDeal) {
        return this.baseService.post(this.endpointService.getImportMilestoneUrl(), data);
    }

    getLatesAddedForecastData(projId) {
        const body = {
            bmProjID: projId,
            bmForcasted: 1
        };
        return this.baseService.post(this.endpointService.getLatesAddedMilestoneUrl(), body);
    }

    mappPms(forId, pmsId) {
        const body = {
            bmID: forId,
            actualbmID: pmsId
        };
        return this.baseService.post(this.endpointService.getMapPmsUrl(), body);
    }

    getDependencyOwnerList() {
        const body = {
            searchKey: ''
        };

        return this.baseService.post(this.endpointService.getDependencyOwnerListUrl(), body);
    }

    triggerMs(msId) {
        const body = {
            bmID: msId
        };
        return this.baseService.post(this.endpointService.getTriggerMsUrl(), body);
    }

    getMsType() {
        const body = {};
        return this.baseService.post(this.endpointService.getMsTypeUrl(), body);
    }
}
