import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EntityService, ToasterService } from '../../../../../shared';

@Component({
    selector: 'app-delete-entity-code',
    templateUrl: './delete-entity-code.component.html',
    styleUrls: ['./delete-entity-code.component.css']
})
export class DeleteEntityCodeComponent implements OnInit {
    @Input() entity;

    constructor(public activeModal: NgbActiveModal, private toasterService: ToasterService, private entityService: EntityService) {}

    ngOnInit() {}

    deleteEntity() {
        const data = {
            deleteArray: [this.entity.entityId],
            status: 0
        };

        this.entityService.deleteEntity(data).subscribe((res: any) => {
            if (res.success) {
                this.toasterService.showSuccess(res.message);
                this.activeModal.close(res);
            } else {
                this.toasterService.error(res.message);
            }
        });
    }
}
