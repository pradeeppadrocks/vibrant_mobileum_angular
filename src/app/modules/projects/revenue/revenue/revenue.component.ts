import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Revenue, ProjectDetails, ImportRev } from '../revenue.model';
import { RevenueService } from '../revenue.service';
import { ToasterService } from '../../../../shared';
import { LOBRevenueModalComponent } from '../modals/lob-revenue-modal.component';
import { DataTableDirective } from 'angular-datatables';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-revenue',
    templateUrl: './revenue.component.html',
    styleUrls: ['./revenue.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class RevenueComponent implements OnInit, AfterViewInit {
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    @Input() projectDetails;

    dtOptions: any = {};
    revenueList: any[];
    pageIndex = 0;
    pageLength = 10;
    projectId = '';

    STDM = '';
    OppID = '';
    DealType = '';
    ValueUSD = '';
    ActualStartDate = '';
    ActualEndDate = '';
    TotalBill = '';
    TotalRevenue = '';
    TotalUnbilled = '';
    RevenueBacklog = '';
    bmName = '';
    LOBList = [];
    ProjectCode = '';

    isSearched;
    searchData;

    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    constructor(
        private revenueservice: RevenueService,
        private toasterservice: ToasterService,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        config: NgbModalConfig,
        private ngxService: NgxUiLoaderService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    search() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    reset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }
    ngOnInit() {
        this.projectId = this.route.snapshot.paramMap.get('id');
        this.gridBinding();
        this.pageLoadBinding();
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    revYear: '',
                    revMonth: '',
                    revAmount: '',
                    revProjId: this.projectId
                };
                that.revenueservice.getRevenueList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.revenueList = pagedData.data.revenue;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.revenueList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'Sl No.', orderable: false },
                { data: 'revMonth' },
                { data: 'revYear' },
                { data: 'revAmount' }
                /* { data: 'revInvoice' },
        { data: 'revDelta' },
        { data: 'revCreatedDate' },
        { data: 'revUpdatedDate' }*/
            ]
        };
    }

    pageLoadBinding() {
        // Get Project Details
        const prodata: ProjectDetails = {
            projId: this.projectId
        };
        this.revenueservice.getProjectDetails(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    //this.regionList = result.data.projects;
                    this.STDM = '';
                    this.OppID = result.data.oppId;
                    this.DealType = result.data.dealType;
                    this.ValueUSD = result.data.amountUSD;
                    this.ActualStartDate = result.data.startDate;
                    this.ActualEndDate = result.data.closeDate;
                    this.TotalBill = result.data.totalBill;
                    this.TotalRevenue = result.data.totalRevenue;
                    this.TotalUnbilled = result.data.totalUnbilled;
                    this.RevenueBacklog = result.data.revenueBackLog;
                    this.bmName = result.data.bmName;
                    this.LOBList = result.data.lob;
                    this.ProjectCode = result.data.projectCode;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    LOBRevenue() {
        const modalRef1 = this.modalService.open(LOBRevenueModalComponent, { size: 'xl' as any, centered: true });
        modalRef1.componentInstance.LOBList = this.LOBList;
        modalRef1.componentInstance.ProjectCode = this.projectDetails ? this.projectDetails.projectCode : '';
        modalRef1.result.then(
            result => {
                //if (result.success) {
                //  this.toasterservice.showSuccess(result.message);
                //} else {
                //  this.toasterservice.error(result.message);
                //}
            },
            reason => {}
        );
    }

    importFile($event, id): void {
        this.readThis($event.target, id);
    }

    readThis(inputValue: any, id): void {
        var file: File = inputValue.files[0];
        if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            this.toasterservice.error('Only xlsx files are supported.');
            return;
        }
        var myReader: FileReader = new FileReader();

        myReader.onloadend = e => {
            var strresult = myReader.result.toString();
            let base64 = strresult.split('base64,');
            const prodata: ImportRev = {
                fileValue: base64[1],
                projId: this.projectId
            };
            this.ngxService.start();
            this.revenueservice.importRevenue(prodata).subscribe(
                result => {
                    if (result.success && result.data) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    document.getElementById(id)['value'] = '';
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
            //this.image = myReader.result;
        };
        myReader.readAsDataURL(file);
    }
}
