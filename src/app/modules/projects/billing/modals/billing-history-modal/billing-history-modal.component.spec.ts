import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingHistoryModalComponent } from './billing-history-modal.component';

describe('BillingHistoryModalComponent', () => {
    let component: BillingHistoryModalComponent;
    let fixture: ComponentFixture<BillingHistoryModalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BillingHistoryModalComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BillingHistoryModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
