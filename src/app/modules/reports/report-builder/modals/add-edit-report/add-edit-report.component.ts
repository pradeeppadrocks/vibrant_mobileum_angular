import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportBuilderService, ToasterService } from '../../../../../shared';

@Component({
    selector: 'app-add-edit-report',
    templateUrl: './add-edit-report.component.html',
    styleUrls: ['./add-edit-report.component.css']
})
export class AddEditReportComponent implements OnInit {
    @Input() btnContent;
    @Input() reportId;
    @Input() reportName;

    duration = '';

    title = 'NG7 CheckList With Parents and Child Structure';
    data: any;
    editData: any;

    childItemlistChange = false;
    parentItemListChange = false;
    selectUnselectAllChange = false;
    isReportNameChanged = false;
    isDurationChanged = false;
    isAnyCheckboxSelected;

    constructor(
        private formBuilder: FormBuilder,
        public activeModal: NgbActiveModal,
        private reportBuilderService: ReportBuilderService,
        private customToasterService: ToasterService
    ) {
        this.data = {};
        this.data.isAllSelected = false;
        this.data.isAllCollapsed = false;
        this.getReportsField();
    }

    getReportsField() {
        this.reportBuilderService.getReportMenu().subscribe((res: any) => {
            if (res.success) {
                this.data.ParentChildchecklist = res.data.reportList;

                // Based on report id get report details for editing
                if (this.reportId) {
                    this.getReportsMetaDetails();
                }
            } else {
                this.customToasterService.error(res.message);
            }
        });
    }

    ngOnInit() {}

    getReportsMetaDetails() {
        const data = {
            reportId: this.reportId,
            pageSize: 10,
            pageNo: 1,
            order: 1,
            sort: ''
        };
        this.reportBuilderService.getReportDetailsBasedOnId(data).subscribe((res: any) => {
            if (res.success) {
                this.data.ParentChildchecklist = res.data.reportList;
                this.reportName = res.data.reportName;
                this.duration = res.data.duration;
                this.checkAnyFieldIsSelected();
            } else {
                this.customToasterService.error(res.message);
            }
        });
    }

    saveReport() {
        if (this.btnContent === 'EDIT') {
            this.reportBuilderService.editReport(this.data, this.duration, this.reportName, this.reportId).subscribe((res: any) => {
                if (res.success) {
                    this.customToasterService.showSuccess(res.message);
                    this.activeModal.close(res);
                } else {
                    this.customToasterService.error(res.message);
                }
            });
        } else {
            this.reportBuilderService.addReport(this.data, this.duration, this.reportName).subscribe((res: any) => {
                if (res.success) {
                    this.customToasterService.showSuccess(res.message);
                    this.activeModal.close(res);
                } else {
                    this.customToasterService.error(res.message);
                }
            });
        }
    }

    // Click event on parent checkbox
    parentCheck(parentObj) {
        this.parentItemListChange = true;
        for (let i = 0; i < parentObj.childList.length; i++) {
            parentObj.childList[i].isSelected = parentObj.isSelected;
        }
        this.checkAnyFieldIsSelected();
    }

    // Click event on child checkbox
    childCheck(parentObj, childObj) {
        this.childItemlistChange = true;
        parentObj.isSelected = childObj.every((itemChild: any) => {
            return itemChild.isSelected === true;
        });
        this.checkAnyFieldIsSelected();
    }

    // Click event on master select
    selectUnselectAll(obj) {
        this.selectUnselectAllChange = true;
        obj.isAllSelected = !obj.isAllSelected;
        for (let i = 0; i < obj.ParentChildchecklist.length; i++) {
            obj.ParentChildchecklist[i].isSelected = obj.isAllSelected;
            for (let j = 0; j < obj.ParentChildchecklist[i].childList.length; j++) {
                obj.ParentChildchecklist[i].childList[j].isSelected = obj.isAllSelected;
            }
        }
        this.checkAnyFieldIsSelected();
    }

    // Expand/Collapse event on each parent
    expandCollapse(obj) {
        obj.isClosed = !obj.isClosed;
    }

    // Master expand/ collapse event
    expandCollapseAll(obj) {
        for (let i = 0; i < obj.ParentChildchecklist.length; i++) {
            obj.ParentChildchecklist[i].isClosed = !obj.isAllCollapsed;
        }
        obj.isAllCollapsed = !obj.isAllCollapsed;
    }

    // Just to show updated JSON object on view
    stringify(obj) {
        return JSON.stringify(obj);
    }

    // TODO:
    checkAnyFieldIsSelected() {
        /*
const checkListData = this.data.ParentChildchecklist.filter(data => {
  if (data.isSelected) {
    this.isAnyCheckboxSelected = true;
    return true;
  } else {
    if (data.childList.length > 0) {
      data.childList.filter(child => {
        if (child.isSelected) {
          this.isAnyCheckboxSelected = true;
          return true;
        } else {
          this.isAnyCheckboxSelected = false;
        }
      });
    }
  }
});
*/

        for (let p = 0; p < this.data.ParentChildchecklist.length; p++) {
            const parent = this.data.ParentChildchecklist[p];
            if (parent.isSelected) {
                this.isAnyCheckboxSelected = true;
                return;
            } else if (parent.childList.length > 0) {
                for (let c = 0; c < parent.childList.length; c++) {
                    const child = parent.childList[c];
                    if (child.isSelected) {
                        this.isAnyCheckboxSelected = true;
                        return;
                    }
                }
            }
        }
        this.isAnyCheckboxSelected = false;
        return;
    }

    reportNameChanged(e) {
        this.isReportNameChanged = true;
    }

    durationChanged(e) {
        this.isDurationChanged = true;
    }

    checkChildExistOrNot(childList) {
        for (const child of childList) {
            if (child.isSelected) {
                return true;
            }
        }
        return false;
    }
}
