import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { EncDecrService } from './enc-decr.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class BaseService {
    constructor(private http: HttpClient, private cryptoService: EncDecrService) {}

    post(url: string, options?: any, header?: object): Observable<any> {
        const decData = options;
        return this.http.post(url, decData, header).pipe(
            map((res: Response) => {
                if (res && res.success) {
                    const data: any = this.cryptoService.decryptData(res.data);
                    res.data = data;
                }
                return res;
            }),
            catchError((error: HttpErrorResponse) => this.err(error))
        );
    }

    patch(url: string, options?: any, header?: object): Observable<any> {
        const decData = options;
        return this.http.patch(url, decData, header).pipe(
            map((res: Response) => {
                if (res && res.success) {
                    const data: any = this.cryptoService.decryptData(res.data);
                    res.data = data;
                }
                return res;
            }),
            catchError((error: HttpErrorResponse) => this.err(error))
        );
    }

    delete(url: string, options?: any, header?: object): Observable<any> {
        const decData = options;
        const tempoptions = {
            headers: new HttpHeaders({}),
            body: decData
        };
        return this.http.delete(url, tempoptions).pipe(
            map((res: Response) => {
                if (res && res.success) {
                    const data: any = this.cryptoService.decryptData(res.data);
                    res.data = data;
                }
                return res;
            }),
            catchError((error: HttpErrorResponse) => this.err(error))
        );
    }

    err(error: HttpErrorResponse) {
        return throwError(error.message);
    }

    private buildParams(params: any) {
        const outputParams = new HttpParams();
        for (const key in params) {
            outputParams.set(key, params[key]);
        }
        return params || {};
    }
}

export interface Response {
    code: number;
    data: string;
    message: string;
    success: boolean;
}
