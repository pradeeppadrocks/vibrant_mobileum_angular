import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../../shared/common-component/dashboard/dashboard.component';
import { AuthGuard } from '../../core/guards/auth/auth.guard';
import { AuthorizationGuard } from '../../core/guards/auth/authorization.guard';

import { BillingMilestoneComponent } from './billingmilestones/billingmilestones.component';
import { ConversionTypeComponent } from './conversiontype/conversiontype.component';
import { LineOfBusinessComponent } from './lineofbusiness/lineofbusiness.component';
import { ModuleComponent } from './module/module.component';
import { ModuleMasterComponent } from './modulemaster/modulemaster.component';
import { OpportunityDealTypeComponent } from './opportunity_dealtype/opportunity_dealtype.component';
import { PMODealTypeComponent } from './pmo_dealtype/pmo_dealtype.component';
import { RegionComponent } from './region/region.component';
import { UserListComponent } from './userlist/userlist.component';
import { UserTypeComponent } from './usertype/usertype.component';
import { DealTypeComponent } from './dealtype/dealtype.component';
import { ProjectTypeComponent } from './projecttype/projecttype.component';
import { ProjectDependencyComponent } from './projectdependency/projectdependency.component';
import { EntityCompanyCodeComponent } from './entity_company/entity-company-code/entity-company-code.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Master Data' },
        children: [
            { path: '', redirectTo: 'region' },
            {
                path: 'billingmilestone',
                component: BillingMilestoneComponent,
                data: { title: 'Billing Milestones', name: 'BillingMilestone' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'conversiontype',
                component: ConversionTypeComponent,
                data: { title: 'Conversion Type', name: 'ConversionType' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'lineofbusiness',
                component: LineOfBusinessComponent,
                data: { title: 'Line Of Business', name: 'LineOfBussiness' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'module',
                component: ModuleComponent,
                data: { title: 'Module', name: 'ModuleMaster' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'modulemaster',
                component: ModuleMasterComponent,
                data: { title: 'Module Master', name: 'ModuleMaster' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'opportunitydealtype',
                component: OpportunityDealTypeComponent,
                data: { title: 'Opportunity Deal Type', name: 'OpportunityDealType' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'pmodealtype',
                component: PMODealTypeComponent,
                data: { title: 'PMO - Deal Type', name: 'PMODealType' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'region',
                component: RegionComponent,
                data: { title: 'Region', name: 'Region' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'userlist',
                component: UserListComponent,
                data: { title: 'User List', name: 'UserList' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'usertype',
                component: UserTypeComponent,
                data: { title: 'User Type', name: 'UserType' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'dealtype',
                component: DealTypeComponent,
                data: { title: 'Deal Type', name: 'DealType' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'projecttype',
                component: ProjectTypeComponent,
                data: { title: 'Project Type', name: 'ProjectType' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'projectdependency',
                component: ProjectDependencyComponent,
                data: { title: 'Project Dependency', name: 'ProjectDependency' },
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'entitycompanycode',
                component: EntityCompanyCodeComponent,
                data: { title: 'Entity Company Code', name: 'EntityCompanyCode' },
                canActivate: [AuthGuard, AuthorizationGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MasterDataRoutingModule {}
