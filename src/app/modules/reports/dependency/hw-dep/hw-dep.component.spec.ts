import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HwDepComponent } from './hw-dep.component';

describe('HwDepComponent', () => {
    let component: HwDepComponent;
    let fixture: ComponentFixture<HwDepComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HwDepComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HwDepComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
