import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RagRevenueAmberComponent } from './rag-revenue-amber.component';

describe('RagRevenueAmberComponent', () => {
    let component: RagRevenueAmberComponent;
    let fixture: ComponentFixture<RagRevenueAmberComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RagRevenueAmberComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RagRevenueAmberComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
