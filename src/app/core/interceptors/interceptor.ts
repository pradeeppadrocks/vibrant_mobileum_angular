import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { SessionService } from '..';
import { catchError, tap } from 'rxjs/operators';
import { AuthenticationService, EncDecrService, ToasterService } from '../../shared';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class HeaderInterceptor implements HttpInterceptor {
    constructor(
        private sessionService: SessionService,
        private encrDecrService: EncDecrService,
        private authenticationService: AuthenticationService,
        private router: Router,
        private customToasterService: ToasterService
    ) {}

    // Global Error Handler.
    // Using this we can send a user-friendly error message.

    private static handleError(error: HttpErrorResponse) {
        let errMsg = '';
        // Client Side Error
        if (error.error instanceof ErrorEvent) {
            errMsg = `Error: ${error.error.message}`;
        } else {
            // Server Side Error
            if (error.status === 400 || error.status === 401) {
                // write proper msg
            }
            errMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        // return an observable
        return throwError(errMsg);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const headers = {
            setHeaders: {}
        };

        if (!req.headers.has('Content-Type')) {
            headers.setHeaders['Content-Type'] = 'application/json';
        }

        if (this.sessionService.getJwtToken() && req.url !== 'https://angular-datatables-demo-server.herokuapp.com') {
            headers.setHeaders['authorization'] = 'MOBJWT ' + this.sessionService.getJwtToken();
        }

        if (!req.url.includes('login') && this.authenticationService.isTokenExpired()) {
            // this.customToasterService.error('Token expired.Please login');
            this.router.navigate(['', 'auth', 'login']);
        }

        // Request encryption
        /*if (req.body && req.url !== 'https://angular-datatables-demo-server.herokuapp.com/') {
            const reqData = this.encrDecrService.encryptData(req.body);
            if (reqData) {
                req = req.clone({
                    body: { data: reqData }
                });
            }
        }*/

        const authReq = req.clone(headers);

        return next.handle(authReq).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    if (event.body) {
                        const decryptedData = this.encrDecrService.decryptData(event.body.data);
                        const decryptedBody = event.body;
                        decryptedBody.data = decryptedData;
                        // Set token

                        const token = event.headers.get('Access-Token');
                        this.sessionService.setJwtToken(token);
                        if (!event.body.success && event.body.code === 994 && event.body.message === 'Token has expired') {
                            //  this.customToasterService.error('Token expired.Please login');
                            this.router.navigate(['', 'auth', 'login']);
                        }
                        // change the response body here
                        return event.clone({
                            body: decryptedBody
                        });
                    }
                }
            }),
            catchError((error: HttpErrorResponse) => {
                return HeaderInterceptor.handleError(error);
            })
        );
    }
}
