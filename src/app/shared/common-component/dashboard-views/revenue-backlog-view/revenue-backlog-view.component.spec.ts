import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenueBacklogViewComponent } from './revenue-backlog-view.component';

describe('RevenueBacklogViewComponent', () => {
    let component: RevenueBacklogViewComponent;
    let fixture: ComponentFixture<RevenueBacklogViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RevenueBacklogViewComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RevenueBacklogViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
