import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';

@Injectable({
    providedIn: 'root'
})
export class EntityService {
    constructor(private baseService: BaseService, private endpointService: EndpointService) {}

    addEntity(data) {
        return this.baseService.post(this.endpointService.getAddEntityUrl(), data);
    }

    editEntity(data) {
        return this.baseService.post(this.endpointService.getEditEntityUrl(), data);
    }

    getEntityList(data) {
        return this.baseService.post(this.endpointService.getEntityListUrl(), data);
    }

    deleteEntity(data) {
        return this.baseService.delete(this.endpointService.getEntityDeleteUrl(), data);
    }

    getProjectEntity() {
        const data = {
            searchKey: ''
        };
        return this.baseService.post(this.endpointService.getProjectEntityurl(), data);
    }
}
