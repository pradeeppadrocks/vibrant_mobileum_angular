import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class ApprovalService {
    constructor(private endpointService: EndpointService, private baseService: BaseService) {}

    getApprovalProjectList(body) {
        return this.baseService.post(this.endpointService.getApproveProjectListUrl(), body);
    }

    getProjectDetails(body) {
        return this.baseService.post(this.endpointService.getProjectDetailsUrl(), body);
    }
}
