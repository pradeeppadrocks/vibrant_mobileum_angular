import { Component, Input, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../../../../shared/services/billing.service';
import { ToasterService } from '../../../../../shared';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DependenciesModalComponent } from './dependencies-modal/dependencies-modal.component';
import { CONSTANTS } from '../../../../../shared/constant';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';

@Component({
    selector: 'app-create-milestone-modal',
    templateUrl: './create-milestone-modal.component.html',
    styleUrls: ['./create-milestone-modal.component.css']
})
export class CreateMilestoneModalComponent implements OnInit {
    @Input() projectDetails;
    @Input() fiterValue;
    @Input() msType;
    @Input() msId;
    @Input() projectId;

    msData;
    // Date Picker
    bsConfigStartDate: Partial<BsDatepickerConfig>;
    bsConfigEndDate: Partial<BsDatepickerConfig>;
    currentDate: Date = new Date();
    minCurrentEndDate = new Date();

    consolidatedBillingMilestoneForm: FormGroup;
    bmProjId: FormControl;
    bmType: FormControl;
    bmRisk: FormControl;
    bmRiskRemark: FormControl;
    bmMsStartDate: FormControl;
    bmActualDate: FormControl;
    bmDependencyOwner: FormControl;
    bmPoNum: FormControl;
    bmInvoiceNumber: FormControl;
    bmInvoiceDate: FormControl;
    bmMilestone: FormControl;
    bmPercentage: FormControl;
    bmAmount: FormControl;
    bmPlanDate: FormControl;
    bmMsEndDate: FormControl;
    bmDependencies: FormControl;
    bmDependencyRosolutionDate: FormControl;
    bmPmsMsRefNo: FormControl;
    bmInvoiceAmount: FormControl;
    bmDifference: FormControl;
    bmTriggered: FormControl;
    bmUpside: FormControl;
    bmID: FormControl;
    bmRagTag: FormControl;

    isFormSubmitted = false;
    bmTypes = [];
    bmStatusList = [{ name: 1, value: 1 }];
    milestones;
    dependencies: any;
    dependencyTypes: any;
    sqlDefaultDate = '1900-01-01T00:00:00.000Z';

    isDepModalNotChanged = true;

    // projectId;

    constructor(
        public activeModal: NgbActiveModal,
        private billingService: BillingService,
        private customToasterService: ToasterService,
        private route: ActivatedRoute,
        private ngxService: NgxUiLoaderService,
        private modalService: NgbModal,
        config: NgbModalConfig
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {
        this.createFormControls();
        this.createForm();
        if (this.fiterValue === 'ADD') {
            this.getLatesAddedForecastData();
        }
        this.getMsType();
        this.getAllPojectDependency();
        this.getMilestoneIds();

        if (this.msId) {
            this.getMilestoneDetails();
        }
        this.dependencies = this.projectDetails.conversionAnalysis;
    }

    getMsType() {
        this.billingService.getMsType().subscribe((res: any) => {
            if (res.success) {
                this.bmTypes = res.data.milestonesTypes;
            }
        });
    }

    getFormattedDate(input) {
        if (input) {
            const date = new Date(input);
            return [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');
        } else {
            return '';
        }
    }

    getMilestoneIds() {
        this.billingService.getMilestones().subscribe((res: any) => {
            if (res.success) {
                this.milestones = res.data.milestones;
            } else {
                this.customToasterService.error(res.message);
            }
        });
    }

    createFormControls() {
        this.bmProjId = new FormControl(this.projectId, [Validators.required]);
        this.bmType = new FormControl('', [Validators.required]);
        this.bmRiskRemark = new FormControl('', [Validators.pattern(CONSTANTS.regex.preventSpecialChar)]);
        this.bmMsStartDate = new FormControl('', [Validators.required]);
        this.bmActualDate = new FormControl('', []);
        this.bmDependencyOwner = new FormControl('', []);
        this.bmPoNum = new FormControl('', [Validators.pattern(CONSTANTS.regex.allowAlphaNumeric)]);
        this.bmInvoiceNumber = new FormControl('', [Validators.pattern(CONSTANTS.regex.allowAlphaNumeric)]);
        this.bmInvoiceAmount = new FormControl('', []);
        this.bmMilestone = new FormControl(null, [Validators.required]);
        this.bmPercentage = new FormControl('', []);
        this.bmAmount = new FormControl('', [Validators.required]);
        this.bmPlanDate = new FormControl('', []);
        this.bmMsEndDate = new FormControl('', [Validators.required]);
        this.bmDependencies = new FormControl('', []);
        this.bmDependencyRosolutionDate = new FormControl('', []);
        this.bmPmsMsRefNo = new FormControl('', [Validators.pattern(CONSTANTS.regex.allowAlphaNumeric)]);
        this.bmInvoiceDate = new FormControl('', []);
        this.bmDifference = new FormControl('', []);
        this.bmRisk = new FormControl('Y');
        this.bmTriggered = new FormControl('Y');
        this.bmUpside = new FormControl(1);
        this.bmRagTag = new FormControl('green');
        if (this.msId) {
            this.bmID = new FormControl(this.msId);
        }
    }

    createForm() {
        this.consolidatedBillingMilestoneForm = new FormGroup({
            bmProjId: this.bmProjId,
            bmRisk: this.bmRisk,
            bmTriggered: this.bmTriggered,
            bmType: this.bmType,
            bmRiskRemark: this.bmRiskRemark,
            bmMsStartDate: this.bmMsStartDate,
            bmActualDate: this.bmActualDate,
            bmDependencyOwner: this.bmDependencyOwner,
            bmPoNum: this.bmPoNum,
            bmInvoiceNumber: this.bmInvoiceNumber,
            bmInvoiceAmount: this.bmInvoiceAmount,
            bmMilestone: this.bmMilestone,
            bmPercentage: this.bmPercentage,
            bmAmount: this.bmAmount,
            bmPlanDate: this.bmPlanDate,
            bmMsEndDate: this.bmMsEndDate,
            bmDependencies: this.bmDependencies,
            bmDependencyRosolutionDate: this.bmDependencyRosolutionDate,
            bmPmsMsRefNo: this.bmPmsMsRefNo,
            bmInvoiceDate: this.bmInvoiceDate,
            bmDifference: this.bmDifference,
            bmUpside: this.bmUpside,
            bmRagTag: this.bmRagTag,
            ...(this.bmID && { bmID: this.bmID })
        });
    }

    getMilestoneDetails() {
        const milestoneData = {
            pageNo: 1,
            pageSize: 5,
            sort: '',
            order: 1,
            searchKey: '',
            bmProjID: this.projectId,
            bmForcasted: this.msType,
            bmID: this.msId
        };
        this.ngxService.start();
        this.billingService.getForecastedMilestone(milestoneData).subscribe((res: any) => {
            if (res.success) {
                this.msData = res.data;
                this.dependencies = this.msData.conversionAnalysis;
                this.consolidatedBillingMilestoneForm.controls.bmProjId.setValue(this.projectId);
                this.consolidatedBillingMilestoneForm.controls.bmRisk.setValue(this.msData.bmRisk === 'Y' ? 'Y' : 'N');
                this.consolidatedBillingMilestoneForm.controls.bmTriggered.setValue(this.msData.bmTriggered);
                this.consolidatedBillingMilestoneForm.controls.bmType.setValue(this.msData.bmType);
                this.consolidatedBillingMilestoneForm.controls.bmRiskRemark.setValue(this.msData.bmRiskRemark);
                this.consolidatedBillingMilestoneForm.controls.bmMsStartDate.setValue(
                    this.msData.bmMsStartDate !== this.sqlDefaultDate ? this.msData.bmMsStartDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmActualDate.setValue(
                    this.msData.bmActualDate !== this.sqlDefaultDate ? this.msData.bmActualDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmDependencyOwner.setValue(this.msData.bmDependencyOwner);
                this.consolidatedBillingMilestoneForm.controls.bmPoNum.setValue(this.msData.bmPoNum);
                this.consolidatedBillingMilestoneForm.controls.bmInvoiceNumber.setValue(this.msData.bmInvoiceNumber);
                this.consolidatedBillingMilestoneForm.controls.bmInvoiceAmount.setValue(this.msData.bmInvoiceAmount);
                this.consolidatedBillingMilestoneForm.controls.bmMilestone.setValue(
                    this.msData.bmMilestoneName === 'FA' ? 'FA ' : this.msData.bmMilestoneName
                );
                this.consolidatedBillingMilestoneForm.controls.bmAmount.setValue(this.msData.bmAmount);
                this.consolidatedBillingMilestoneForm.controls.bmPlanDate.setValue(
                    this.msData.bmPlanDate !== this.sqlDefaultDate ? this.msData.bmPlanDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmMsEndDate.setValue(
                    this.msData.bmMsEndDate !== this.sqlDefaultDate ? this.msData.bmMsEndDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmDependencies.setValue(this.msData.bmDependencies);
                this.consolidatedBillingMilestoneForm.controls.bmDependencyRosolutionDate.setValue(
                    this.msData.bmDependencyRosolutionDate !== this.sqlDefaultDate ? this.msData.bmDependencyRosolutionDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmPmsMsRefNo.setValue(this.msData.bmPmsMsRefNo);
                this.consolidatedBillingMilestoneForm.controls.bmInvoiceDate.setValue(
                    this.msData.bmInvoiceDate !== this.sqlDefaultDate ? this.msData.bmInvoiceDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmDifference.setValue(this.msData.bmDifference);
                this.consolidatedBillingMilestoneForm.controls.bmUpside.setValue(this.msData.brand);
                this.consolidatedBillingMilestoneForm.controls.bmPercentage.setValue(this.msData.bmPercentage);
                this.consolidatedBillingMilestoneForm.controls.bmID.setValue(this.msData.bmID);
                this.consolidatedBillingMilestoneForm.controls.bmUpside.setValue(1);
                this.consolidatedBillingMilestoneForm.controls.bmRagTag.setValue(this.msData.bmRagTag);
                this.ngxService.stop();
            } else {
                this.customToasterService.error('someting went wrong.');
                this.ngxService.stop();
            }
        });
    }

    save() {
        if (this.consolidatedBillingMilestoneForm.valid && !this.isAmountExceeds()) {
            this.isFormSubmitted = false;
            this.formatDateInFormControls();
            const billingMsData = this.consolidatedBillingMilestoneForm.value;
            billingMsData.conversionAnalysis = this.dependencies;
            if (this.fiterValue !== 'ADD') {
                const modalRef = this.modalService.open(RemarksModalComponent, { size: 'md' as any, centered: true });

                modalRef.result.then(
                    result => {
                        if (result) {
                            // later need to send remarks to api
                            this.ngxService.start();
                            billingMsData['bmRemarks'] = result;
                            this.billingService.addBillingMilestone(billingMsData, this.msType).subscribe((res: any) => {
                                if (res.success) {
                                    this.activeModal.close(res);
                                } else {
                                    this.customToasterService.error(res.message);
                                }
                                this.ngxService.stop();
                            });
                        }
                    },
                    reason => {
                        console.log(reason);
                    }
                );
            } else {
                this.ngxService.start();
                this.billingService.addBillingMilestone(billingMsData, this.msType).subscribe((res: any) => {
                    if (res.success) {
                        this.activeModal.close(res);
                    } else {
                        this.customToasterService.error(res.message);
                    }
                    this.ngxService.stop();
                });
            }
        } else {
            this.isFormSubmitted = true;
        }
    }

    onDateChange(event) {
        if (event) {
            this.minCurrentEndDate = new Date(event);
        }
    }

    formatDateInFormControls() {
        this.consolidatedBillingMilestoneForm.controls.bmMsStartDate.setValue(
            this.getFormattedDate(this.consolidatedBillingMilestoneForm.controls.bmMsStartDate.value)
        );

        this.consolidatedBillingMilestoneForm.controls.bmMsEndDate.setValue(
            this.getFormattedDate(this.consolidatedBillingMilestoneForm.controls.bmMsEndDate.value)
        );

        if (this.fiterValue !== 'ADD') {
            this.consolidatedBillingMilestoneForm.controls.bmPlanDate.setValue(
                this.getFormattedDate(this.consolidatedBillingMilestoneForm.controls.bmPlanDate.value)
            );
        }

        this.consolidatedBillingMilestoneForm.controls.bmInvoiceDate.setValue(
            this.getFormattedDate(this.consolidatedBillingMilestoneForm.controls.bmInvoiceDate.value)
        );

        this.consolidatedBillingMilestoneForm.controls.bmActualDate.setValue(
            this.getFormattedDate(this.consolidatedBillingMilestoneForm.controls.bmActualDate.value)
        );

        this.consolidatedBillingMilestoneForm.controls.bmDependencyRosolutionDate.setValue(
            this.getFormattedDate(this.consolidatedBillingMilestoneForm.controls.bmDependencyRosolutionDate.value)
        );
    }

    getLatesAddedForecastData() {
        this.billingService.getLatesAddedForecastData(this.projectId).subscribe((res: any) => {
            if (res.success && res.data) {
                const latestMsData = res.data;
                this.consolidatedBillingMilestoneForm.controls.bmProjId.setValue(latestMsData.bmProjID);
                this.consolidatedBillingMilestoneForm.controls.bmRisk.setValue(latestMsData.bmRisk === 'Y' ? 'Y' : 'N');
                this.consolidatedBillingMilestoneForm.controls.bmTriggered.setValue(latestMsData.bmTriggered);
                this.consolidatedBillingMilestoneForm.controls.bmType.setValue(latestMsData.bmType);
                this.consolidatedBillingMilestoneForm.controls.bmRiskRemark.setValue(latestMsData.bmRiskRemark);
                this.consolidatedBillingMilestoneForm.controls.bmMsStartDate.setValue(
                    latestMsData.bmMsStartDate !== this.sqlDefaultDate ? latestMsData.bmMsStartDate : ''
                );
                /* this.consolidatedBillingMilestoneForm.controls.bmActualDate.setValue(
                    latestMsData.bmActualDate !== this.sqlDefaultDate ? latestMsData.bmActualDate : ''
                );*/
                this.consolidatedBillingMilestoneForm.controls.bmDependencyOwner.setValue(latestMsData.bmDependencyOwner);
                this.consolidatedBillingMilestoneForm.controls.bmPoNum.setValue(latestMsData.bmPoNum);
                this.consolidatedBillingMilestoneForm.controls.bmInvoiceNumber.setValue(latestMsData.bmInvoiceNumber);
                this.consolidatedBillingMilestoneForm.controls.bmInvoiceAmount.setValue(latestMsData.bmInvoiceAmount);
                this.consolidatedBillingMilestoneForm.controls.bmMilestone.setValue(
                    latestMsData.bmMilestoneName ? (latestMsData.bmMilestoneName === 'FA' ? 'FA ' : latestMsData.bmMilestoneName) : null
                );
                this.consolidatedBillingMilestoneForm.controls.bmAmount.setValue(latestMsData.bmAmount);
                this.consolidatedBillingMilestoneForm.controls.bmPlanDate.setValue(
                    latestMsData.bmPlanDate !== this.sqlDefaultDate ? latestMsData.bmPlanDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmMsEndDate.setValue(
                    latestMsData.bmMsEndDate !== this.sqlDefaultDate ? latestMsData.bmMsEndDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmDependencies.setValue(latestMsData.bmDependencies);
                this.consolidatedBillingMilestoneForm.controls.bmDependencyRosolutionDate.setValue(
                    latestMsData.bmDependencyRosolutionDate !== this.sqlDefaultDate ? latestMsData.bmDependencyRosolutionDate : ''
                );
                this.consolidatedBillingMilestoneForm.controls.bmPmsMsRefNo.setValue(latestMsData.bmPmsMsRefNo);
                /*this.consolidatedBillingMilestoneForm.controls.bmInvoiceDate.setValue(
                    latestMsData.bmInvoiceDate !== this.sqlDefaultDate ? latestMsData.bmInvoiceDate : ''
                );*/
                this.consolidatedBillingMilestoneForm.controls.bmDifference.setValue(latestMsData.bmDifference);
                this.consolidatedBillingMilestoneForm.controls.bmUpside.setValue(latestMsData.brand);
                this.consolidatedBillingMilestoneForm.controls.bmPercentage.setValue(latestMsData.bmPercentage);
                this.consolidatedBillingMilestoneForm.controls.bmUpside.setValue(1);
                this.consolidatedBillingMilestoneForm.controls.bmRagTag.setValue(latestMsData.bmRagTag ? latestMsData.bmRagTag : 'green');
            }
        });
    }

    openDependenciesModal() {
        const modalRef = this.modalService.open(DependenciesModalComponent, { size: 'xl' as any, centered: true });
        modalRef.componentInstance.dependencies = this.dependencies;
        if (!this.dependencyTypes) {
            this.dependencyTypes = [
                {
                    dependencyId: 1,
                    dependencyName: 'S/W'
                },
                {
                    dependencyId: 2,
                    dependencyName: 'H/W'
                },
                {
                    dependencyId: 3,
                    dependencyName: 'Cust/Dep'
                },
                {
                    dependencyId: 4,
                    dependencyName: 'Paperwork'
                }
            ];
        }
        modalRef.componentInstance.dependencyTypes = this.dependencyTypes;
        modalRef.result.then(result => {
            if (result) {
                this.dependencies = result.dependencies;
                this.isDepModalNotChanged = result.isDependencyChanged;
            }
        });
    }

    private getAllPojectDependency() {
        this.billingService.getAllPojectDependency('').subscribe((res: any) => {
            if (res.success) {
                this.dependencyTypes = res.data.pojectsdependency;
            } else {
                this.customToasterService.error(res.message);
            }
        });
    }

    isAmountExceeds() {
        return this.projectDetails.amount < this.consolidatedBillingMilestoneForm.controls.bmAmount.value;
    }

    clearDate(formControlName) {
        this.consolidatedBillingMilestoneForm.controls[formControlName].setValue('');
        this.consolidatedBillingMilestoneForm.markAsDirty();
    }
}
