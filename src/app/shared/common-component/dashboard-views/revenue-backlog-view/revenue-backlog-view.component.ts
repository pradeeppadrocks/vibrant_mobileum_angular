import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../../services/dashboard.service';
import { Router } from '@angular/router';
import { ChartExportService, RegionChangeService } from '../../..';

@Component({
    selector: 'app-revenue-backlog-view',
    templateUrl: './revenue-backlog-view.component.html',
    styleUrls: ['./revenue-backlog-view.component.css']
})
export class RevenueBacklogViewComponent implements OnInit {
    Highcharts: typeof Highcharts = Highcharts; // Highcharts, it's Highcharts

    conversionChartOptions = {};
    conversiononeToOneFlag = false;
    conversionCategoryList = [];
    conversionList = [];
    updateConversionChart = false;

    dependenciesChartOptions = {};
    dependenciesoneToOneFlag = false;
    dependenciesCategoryList = [];
    dependenciesList = [];
    updateDependenciesChart = false;

    summaryChartOptions = {};
    summaryoneToOneFlag = false;
    updateSummaryViewChart = false;
    showSummaryChart;

    showSummaryChartLoader = true;
    showConversionLoader = true;
    showDependencyLoader = true;
    revenueBacklogPieTimestamp;
    revenueBacklogConversionTimestamp;

    constructor(
        private router: Router,
        private dashboardservice: DashboardService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private regionUpdateService: RegionChangeService,
        public chartExportService: ChartExportService
    ) {}

    ngOnInit() {
        this.loadChartOptions();
        this.pageLoadBinding();

        this.regionUpdateService.region.subscribe(value => {
            this.refreshPage();
        });
    }

    refreshPage() {
        this.pageLoadBinding();
    }

    pageLoadBinding() {
        // this.ngxService.start();

        this.dashboardservice.getRevenueBacklogPie().subscribe(
            result => {
                if (result.success && result.data) {
                    this.showSummaryChartLoader = false;
                    var piechartarray = [];
                    result.data.reportData.forEach(ele => {
                        const temparray = [];
                        temparray.push(ele.name);
                        temparray.push(ele.value == null ? 0 : ele.value);
                        // temparray.push(ele.value == null || ele.name === 'CR-Change Request' ? 0 : ele.value);
                        piechartarray.push(temparray);
                    });
                    this.showSummaryChart = this.showRevenuePiechartData(piechartarray);
                    this.summaryChartOptions['series'][0].data = piechartarray;
                    this.updateSummaryViewChart = true;
                    this.summaryoneToOneFlag = true;
                    this.revenueBacklogPieTimestamp = this.getTimestampDiffInHours(result.data.timestamp);
                    //  this.ngxService.stop();
                }
                //  this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //  this.ngxService.stop();
            }
        );

        //setTimeout(() => {
        this.dashboardservice.getRevenueBacklogConversion().subscribe(
            result => {
                if (result.success && result.data) {
                    result.data.reportData.forEach(ele => {
                        this.conversionCategoryList = [];
                        for (var i in ele.value) {
                            //ele.value[i].value = Math.floor((Math.random() * 100000000) + 1);
                            this.conversionCategoryList.push(ele.value[i].name);
                        }
                    });
                    this.conversionList = this.bindingGridData(result.data.reportData, this.conversionCategoryList);
                    this.conversionChartOptions['xAxis'].categories = this.conversionCategoryList;
                    this.conversionChartOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.updateConversionChart = true;
                    this.conversiononeToOneFlag = true;
                    this.showConversionLoader = false;
                    this.revenueBacklogConversionTimestamp = this.getTimestampDiffInHours(result.data.timestamp);
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //  this.ngxService.stop();
            }
        );
        //}, 1000);

        //setTimeout(() => {
        this.dashboardservice.getRevenueBacklogDependencies().subscribe(
            result => {
                if (result.success && result.data) {
                    result.data.forEach(ele => {
                        this.dependenciesCategoryList = [];
                        for (var i in ele.value) {
                            //ele.value[i].value = Math.floor((Math.random() * 100000000) + 1);
                            this.dependenciesCategoryList.push(ele.value[i].name);
                        }
                    });
                    this.dependenciesList = this.bindingGridData(result.data, this.dependenciesCategoryList);
                    this.dependenciesChartOptions['xAxis'].categories = this.dependenciesCategoryList;
                    this.dependenciesChartOptions['series'] = this.bindingChartData(result.data);
                    this.updateDependenciesChart = true;
                    this.dependenciesoneToOneFlag = true;
                    this.showDependencyLoader = false;
                }
                //   this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
        //}, 2000);
    }

    bindingChartData(data) {
        var list = [];
        data.forEach(ele => {
            var tempdata = { name: ele.name, data: [] };
            for (var i in ele.value) {
                tempdata.data.push(ele.value[i].value);
            }
            list.push(tempdata);
        });
        return list;
    }

    loadChartOptions() {
        this.summaryChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },

            chart: {
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Revenue Summary Forcast'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                formatter() {
                    let seriesSpan = '';

                    if (this.point.y > 1000000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000000, 2) +
                            'M' +
                            '<br />';
                    } else if (this.point.y > 1000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000, 2) +
                            'K' +
                            '<br />';
                    } else {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y, 2) +
                            '<br />';
                    }
                    return seriesSpan + '<table></table>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',

                    dataLabels: {
                        enabled: false
                    },

                    showInLegend: true
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    return `${this.name} (${this.percentage.toFixed(2)} %)`;
                }
            },
            series: [
                {
                    type: 'pie',
                    name: 'Summary',
                    data: []
                }
            ]
        };

        this.conversionChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Conversion'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: this.conversionCategoryList,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount ($)'
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            series: []
        };
        this.dependenciesChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Dependencies'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: this.dependenciesCategoryList,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount ($)'
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            series: []
        };
    }

    bindingGridData(data, categories) {
        var list = [];
        var totalarr = [];
        data.forEach(ele => {
            var tempdata = { name: ele.name, data: [] };
            var total = 0;
            for (var i in ele.value) {
                tempdata.data.push(ele.value[i].value);
                total += ele.value[i].value;
            }
            tempdata.data.push(total);
            list.push(tempdata);
        });

        for (var i in categories) {
            var temptotal = 0;
            data.forEach(ele => {
                for (var j in ele.value) {
                    if (categories[i] == ele.value[j].name) {
                        temptotal += ele.value[j].value;
                    }
                }
            });
            totalarr.push(temptotal);
        }
        list.push({ name: '', data: totalarr });
        return list;
    }

    showRevenuePiechartData(data) {
        let flag = false;
        data.some(ele => {
            if (ele[1] && ele[1] !== 0) {
                flag = true;
                return flag;
            }
        });
        return flag;
    }

    redirectRevenueForecast() {
        this.router.navigate(['dashboard-revenue-forecast-view']);
    }

    getTimestampDiffInHours(t2) {
        const date1 = +new Date();
        const date2 = +new Date(parseInt(t2, 10));

        const diff = Math.abs(date1 - date2) / 1000;

        // get total days between two dates
        const days = Math.floor(diff / 86400);

        // get hours
        const hours = Math.floor(diff / 3600) % 24;

        // get minutes
        const minutes = Math.floor(diff / 60) % 60;

        // get seconds
        const seconds = diff % 60;

        if (hours > 0) {
            return hours + ' hrs ' + minutes + ' minutes';
        } else if (minutes > 0) {
            return minutes + ' minutes';
        } else if (seconds > 0) {
            return seconds + ' seconds';
        }
    }
}
