import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class ToasterService {
    constructor(private toastrService: ToastrService) {}

    showSuccess(message, title?) {
        this.toastrService.success(message, title, {
            timeOut: 4000
        });
    }
    error(message, title?) {
        this.toastrService.error(message, title, {
            timeOut: 4000
        });
    }

    warning(message, title?) {
        this.toastrService.warning(message, title, {
            timeOut: 4000
        });
    }
}
