import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustDepComponent } from './cust-dep.component';

describe('CustDepComponent', () => {
    let component: CustDepComponent;
    let fixture: ComponentFixture<CustDepComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CustDepComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CustDepComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
