import { TestBed } from '@angular/core/testing';

import { RegionChangeService } from './region-change.service';

describe('RegionChangeService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: RegionChangeService = TestBed.get(RegionChangeService);
        expect(service).toBeTruthy();
    });
});
