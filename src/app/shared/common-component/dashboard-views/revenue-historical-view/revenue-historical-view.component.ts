import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Router } from '@angular/router';
import { YearChart } from '../../../models/dashboard.model';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../../services/dashboard.service';
import { ChartExportService, RegionChangeService } from '../../..';

@Component({
    selector: 'app-revenue-historical-view',
    templateUrl: './revenue-historical-view.component.html',
    styleUrls: ['./revenue-historical-view.component.css']
})
export class RevenueHistoricalViewComponent implements OnInit {
    constructor(
        private router: Router,
        private dashboardservice: DashboardService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private regionUpdateService: RegionChangeService,
        public chartExportService: ChartExportService
    ) {}

    Highcharts: typeof Highcharts = Highcharts; // Highcharts, it's Highcharts
    historyChartOptions = {};
    billingReportDashboardOneOptions = {};
    billingReportDashboardSecondOptions = {};
    reccuringChartOptions = {};
    yearList = [];
    selectedYear = '2019';
    historicalChartInstance;
    updateHistoricalViewChart: boolean = false;
    updatebillingReportDashboardOneViewChart: boolean = false;
    updatebillingReportDashboardSecondViewChart: boolean = false;
    reccuringoneToOneFlag: boolean = false;
    isDashboardChartOrTable = true;
    historyoneToOneFlag = false;
    oneDashboardoneToOneFlag = false;
    secondDashboardoneToOneFlag = false;

    updateReccuringViewChart = false;

    monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    //secondDashboardCategoryList = [];

    billingHistoryList = [];
    billingDashboardOneList = [];
    billingDashboardSecondList = [];
    billingReccuringList = [];

    showHistoryChartLoader = true;
    showRevenueReportDashboardOneLoader = true;
    showRevenueReportDashboardTwoLoader = true;

    revenueLobTimeStamp;
    revenueHistoryViewTimestamp;
    revenueHistoryDashboardTimestamp;
    revenueRecuringTimestamp;

    ngOnInit() {
        this.pageLoadBinding(this.selectedYear);
        this.loadHistoricChartOptions();
        // Year Binding
        const lastYear = 2009;
        const today = new Date();
        const year = today.getFullYear();
        for (let i = year; i >= lastYear; i--) {
            const yearData = { id: i, value: i };
            this.yearList.push(yearData);
        }

        this.regionUpdateService.region.subscribe(value => {
            this.refreshPage();
        });
    }

    refreshPage() {
        this.pageLoadBinding(this.selectedYear);
    }

    pageLoadBinding(year) {
        //  this.ngxService.start();
        var yeardata: YearChart = {
            year: year
        };
        //setTimeout(() => {
        this.dashboardservice.getRevenueHistoryView(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.billingHistoryList = this.bindingGridData(result.data.reportData);
                    this.historyChartOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.historyChartOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.revenueHistoryViewTimestamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.historyoneToOneFlag = true;
                    this.updateHistoricalViewChart = true;
                    this.showHistoryChartLoader = false;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //  this.ngxService.stop();
            }
        );
        //}, 500);
        //setTimeout(() => {
        this.dashboardservice.getRevenueHistoryViewDashbord(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.billingDashboardOneList = this.bindingGridData(result.data.reportData);
                    this.billingReportDashboardOneOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.billingReportDashboardOneOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.revenueHistoryDashboardTimestamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.updatebillingReportDashboardOneViewChart = true;
                    this.oneDashboardoneToOneFlag = true;
                    this.showRevenueReportDashboardOneLoader = false;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
        //}, 1000);
        //setTimeout(() => {
        this.dashboardservice.getRevenueHistoryViewLOB(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    //this.bindingsecondChartData(result.data);
                    this.billingDashboardSecondList = this.bindingGridData(result.data.reportData);
                    this.billingReportDashboardSecondOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.billingReportDashboardSecondOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.revenueLobTimeStamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.updatebillingReportDashboardSecondViewChart = true;
                    this.secondDashboardoneToOneFlag = true;
                    this.showRevenueReportDashboardTwoLoader = false;
                }
                // this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
        //}, 1500);

        this.dashboardservice.getRevenueRecuring(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    //this.bindingsecondChartData(result.data);
                    this.billingReccuringList = this.bindingGridData(result.data.reportData);
                    this.reccuringChartOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.reccuringChartOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.revenueRecuringTimestamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.updateReccuringViewChart = true;
                    this.reccuringoneToOneFlag = true;
                }
                // this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
    }

    bindingChartData(data) {
        var list = [];
        data.forEach(ele => {
            // var tempdata = { name: ele.name, data: [0, 0, 0, 0] };
            var tempdata = {
                name: ele.name,
                data: [
                    { name: 'Q1', y: 0, drilldown: 'Q1 ' + ele.name },
                    { name: 'Q2', y: 0, drilldown: 'Q2 ' + ele.name },
                    { name: 'Q3', y: 0, drilldown: 'Q3 ' + ele.name },
                    { name: 'Q4', y: 0, drilldown: 'Q4 ' + ele.name }
                ]
            };
            for (var i = 0; i < 12; i++) {
                if (i == 0 || i == 1 || i == 2) {
                    tempdata.data[0].y += ele.value[i].value;
                } else if (i == 3 || i == 4 || i == 5) {
                    tempdata.data[1].y += ele.value[i].value;
                } else if (i == 6 || i == 7 || i == 8) {
                    tempdata.data[2].y += ele.value[i].value;
                } else if (i == 9 || i == 10 || i == 11) {
                    tempdata.data[3].y += ele.value[i].value;
                }
            }
            list.push(tempdata);
        });
        return list;
    }

    bindingDrildownChartData(data) {
        var list = [];
        data.forEach(ele => {
            for (var i = 1; i < 5; i++) {
                list.push({ name: 'Q' + i + ' ' + ele.name, id: 'Q' + i + ' ' + ele.name, data: [], count: i, tempname: ele.name });
            }
        });
        data.forEach(ele => {
            var tempdata = { name: '', id: '', data: [] };
            for (var i = 0; i < 12; i++) {
                if (i == 0 || i == 1 || i == 2) {
                    list.forEach(subele => {
                        if (subele.count == 1 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                } else if (i == 3 || i == 4 || i == 5) {
                    list.forEach(subele => {
                        if (subele.count == 2 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                } else if (i == 6 || i == 7 || i == 8) {
                    list.forEach(subele => {
                        if (subele.count == 3 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                } else if (i == 9 || i == 10 || i == 11) {
                    list.forEach(subele => {
                        if (subele.count == 4 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                }
            }
        });
        return list;
    }

    makeDrilDownData(month, value) {
        let arr = [];
        arr.push(month);
        arr.push(value);
        return arr;
    }

    bindingGridData(data) {
        var list = [];
        var totalarr = [];
        data.forEach(ele => {
            var tempdata = { name: ele.name, data: [] };
            var total = 0;
            for (var i in ele.value) {
                tempdata.data.push(ele.value[i].value);
                total += ele.value[i].value;
            }
            tempdata.data.push(total);
            list.push(tempdata);
        });

        for (var i = 0; i < 13; i++) {
            var temptotal = 0;
            list.forEach(ele => {
                temptotal += ele.data[i];
            });
            totalarr.push(temptotal);
        }
        list.push({ name: '', data: totalarr });
        return list;
    }

    selectedYearOnChange(year) {
        this.pageLoadBinding(year);
    }

    loadHistoricChartOptions() {
        this.historyChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Historical'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.billingReportDashboardOneOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Report Dashboard'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.billingReportDashboardSecondOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Report Dashboard'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.reccuringChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Entity Wise Split'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };
    }

    chartLegendLineRemove(chart) {
        const series = chart.series;
        $(series).each((i, serie) => {
            if (serie['legendLine']) {
                serie['legendLine'].destroy();
            }
        });
    }

    RedirectRevenueForecast() {
        this.router.navigate(['dashboard-revenue-forecast-view']);
    }

    RedirectRevenueBacklog() {
        this.router.navigate(['dashboard-revenue-backlog-view']);
    }

    getTimestampDiffInHours(t2) {
        const date1 = +new Date();
        const date2 = +new Date(parseInt(t2, 10));

        const diff = Math.abs(date1 - date2) / 1000;

        // get total days between two dates
        const days = Math.floor(diff / 86400);

        // get hours
        const hours = Math.floor(diff / 3600) % 24;

        // get minutes
        const minutes = Math.floor(diff / 60) % 60;

        // get seconds
        const seconds = diff % 60;

        if (hours > 0) {
            return hours + ' hrs ' + minutes + ' minutes';
        } else if (minutes > 0) {
            return minutes + ' minutes';
        } else if (seconds > 0) {
            return seconds + ' seconds';
        }
    }
}
