import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
declare const $: any;
@Injectable({
    providedIn: 'root'
})
export class ChartExportService {
    constructor() {}

    export(id, fileName) {
        // This commented code is for export table using jquery table2excel library
        /*$('#' + id).table2excel({
            exclude: '.noExl',
            name: 'Worksheet Name',
            filename,
            fileext: '.xls',
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });*/

        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(document.getElementById(id));
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, fileName + '.xlsx');
    }
}
