import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class RegionChangeService {
    region = new EventEmitter<any>();

    constructor() {}

    updateRegion(regions) {
        this.region.emit(regions);
    }
}
