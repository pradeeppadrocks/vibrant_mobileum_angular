import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CONSTANTS } from '../../../shared/constant';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../../shared/services/billing.service';
import { PasswordService, ToasterService } from '../../../shared';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
    resetForm: FormGroup;
    password: FormControl;
    currPassword: FormControl;
    userId: FormControl;
    resetPassword: FormControl;
    isFormSubmitted = false;

    constructor(
        public activeModal: NgbActiveModal,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private passwordService: PasswordService,
        private customToasterService: ToasterService
    ) {}

    ngOnInit() {
        this.createFormControls();
        this.createLoginForm();
    }

    createFormControls() {
        this.currPassword = new FormControl('', Validators.required);
        this.password = new FormControl('', [
            Validators.minLength(CONSTANTS.regex.passwordMinLength),
            Validators.maxLength(CONSTANTS.regex.passwordMaxLength),
            Validators.required,
            Validators.pattern(CONSTANTS.regex.confirmpassword)
        ]);
        this.resetPassword = new FormControl('', [
            Validators.minLength(CONSTANTS.regex.passwordMinLength),
            Validators.maxLength(CONSTANTS.regex.passwordMaxLength),
            Validators.required,
            Validators.pattern(CONSTANTS.regex.confirmpassword)
        ]);
    }

    createLoginForm() {
        this.resetForm = new FormGroup({
            password: this.password,
            currPassword: this.currPassword,
            resetPassword: this.resetPassword
        });
    }

    reset() {
        if (this.resetForm.controls.currPassword.value !== this.resetForm.controls.password.value && this.resetForm.valid) {
            this.isFormSubmitted = false;
            // Make backend call to reset password.After success close modal
            this.passwordService.resetPassword(this.resetForm.value).subscribe((res: any) => {
                if (res.success) {
                    this.customToasterService.showSuccess('Updated password successfully');
                    this.activeModal.close(res);
                } else {
                    this.customToasterService.error(res.message);
                }
            });
        } else {
            this.isFormSubmitted = true;
        }
    }
}
