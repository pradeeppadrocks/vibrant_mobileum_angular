import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThirdPartyLibModule } from './third-Party-lib/thirdparty-lib.module';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';
import { PieChartComponent } from './charts/pie-chart/pie-chart.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [PieChartComponent],
    imports: [CommonModule, ThirdPartyLibModule, DirectivesModule, PipesModule, FormsModule],
    exports: [ThirdPartyLibModule, DirectivesModule, PipesModule, PieChartComponent]
})
export class SharedModule {}
