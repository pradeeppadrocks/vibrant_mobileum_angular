import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidatedBmModalComponent } from './consolidated-bm-modal.component';

describe('ConsolidatedBmModalComponent', () => {
    let component: ConsolidatedBmModalComponent;
    let fixture: ComponentFixture<ConsolidatedBmModalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConsolidatedBmModalComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConsolidatedBmModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
