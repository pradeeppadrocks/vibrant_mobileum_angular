import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToasterService } from '../../../shared';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DeleteModalComponent } from '../billing/modals/delete-modal/delete-modal.component';
import { EditProjectModalComponent } from '../modals/edit-project-modal/edit-project-modal.component';
import { ProjectDetails } from '../project.model';
import { ProjectService } from '../project.service';

@Component({
    selector: 'app-project-summary',
    templateUrl: './project-summary.component.html',
    styleUrls: ['./project-summary.component.css']
})
export class ProjectSummaryComponent implements OnInit {
    @Input() projectDetails;
    @Output() projectUpdated = new EventEmitter<any>();
    projectId;

    constructor(
        private toasterservice: ToasterService,
        private modalService: NgbModal,
        config: NgbModalConfig,
        private projectService: ProjectService
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {
        this.projectId = this.projectDetails.projId;
    }

    openEditProjectModal() {
        const modalRef1 = this.modalService.open(EditProjectModalComponent, { size: 'xl' as any, centered: true });
        // Need to pass data to modal

        modalRef1.componentInstance.projectDetails = this.projectDetails;
        modalRef1.result.then(
            result => {
                if (result.success) {
                    this.getProjectDetailsById(this.projectId);
                    this.projectUpdated.emit(true);
                } else {
                }
            },
            reason => {}
        );
    }

    getProjectDetailsById(projId) {
        // Get Project Details
        const prodata: ProjectDetails = {
            projId: this.projectId
        };
        this.projectService.getProjectDetails(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.projectDetails = result.data;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
    }
}
