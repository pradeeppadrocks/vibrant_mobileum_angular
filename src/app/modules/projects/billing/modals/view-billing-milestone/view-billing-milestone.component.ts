import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { BillingService } from '../../../../../shared/services/billing.service';
import { ToasterService } from '../../../../../shared';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { CreateMilestoneModalComponent } from '../create-milestone-modal/create-milestone-modal.component';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-view-billing-milestone',
    templateUrl: './view-billing-milestone.component.html',
    styleUrls: ['./view-billing-milestone.component.css']
})
export class ViewBillingMilestoneComponent implements OnInit {
    @Input() milestoneData;
    @Input() projectDetails;
    @Input() projectId;
    msData;
    deleteMilestoneResult;
    editMilestoneData;
    showTriggerBtn;

    constructor(
        public activeModal: NgbActiveModal,
        private modalService: NgbModal,
        private billingService: BillingService,
        private customToasterservice: ToasterService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService
    ) {}

    ngOnInit() {
        this.getMilestoneData();
    }

    getMilestoneData() {
        const milestoneData = {
            pageNo: 1,
            pageSize: 5,
            sort: '',
            order: 1,
            searchKey: '',
            bmProjID: this.milestoneData.projectId,
            bmForcasted: this.milestoneData.forcastedId,
            bmID: this.milestoneData.milestoneId
        };
        this.ngxService.start();
        this.billingService.getForecastedMilestone(milestoneData).subscribe((res: any) => {
            if (res.success) {
                this.msData = res.data;
                this.initializeTriggerBtn(this.msData.bmMsEndDate);
            } else {
                this.customToasterservice.error('someting went wrong...please try again');
            }
            this.ngxService.stop();
        });
    }

    initializeTriggerBtn(msEndDate) {
        const date1 = new Date(msEndDate);
        const date2 = new Date();
        const diffTime = Math.abs(date2.getTime() - date1.getTime());
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        this.showTriggerBtn = diffDays <= 10;
    }
    openEditBillingModal(btnName, msType?: any, msId?: any) {
        const modalRef1 = this.modalService.open(CreateMilestoneModalComponent, { size: 'xl' as any, centered: true });
        modalRef1.componentInstance.projectDetails = this.projectDetails;
        modalRef1.componentInstance.fiterValue = btnName;
        modalRef1.componentInstance.msType = msType;
        modalRef1.componentInstance.msId = msId;
        modalRef1.componentInstance.projectId = this.projectDetails.projId;

        modalRef1.result.then(
            result => {
                if (result.success) {
                    this.editMilestoneData = result;
                    this.getMilestoneData();
                    this.toasterservice.showSuccess(result.message);
                } else {
                    this.toasterservice.error(result.message);
                }
            },
            reason => {}
        );
    }

    openDeleteModal(milestoneId, fms, index?: any, numberOfElements?: any) {
        const modalRef1 = this.modalService.open(DeleteModalComponent, { size: 'md' as any, centered: true });
        // Need to pass data to modal
        const deleteMsData = {
            milestoneId,
            status: 0
        };
        modalRef1.componentInstance.deleteMsData = deleteMsData;
        modalRef1.result.then(
            result => {
                if (result.success) {
                    // this.toasterservice.showSuccess('Milestone deleted successfully');
                    this.deleteMilestoneResult = result;
                    this.activeModal.close(result);
                }
            },
            reason => {}
        );
    }

    closeViewModal(reason) {
        this.activeModal.dismiss(this.editMilestoneData ? this.editMilestoneData : reason);
    }

    triggerMs(msId) {
        this.billingService.triggerMs(msId).subscribe((res: any) => {
            if (res.success) {
                this.toasterservice.showSuccess(res.message);
            } else {
                this.toasterservice.error(res.message);
            }
        });
    }
}
