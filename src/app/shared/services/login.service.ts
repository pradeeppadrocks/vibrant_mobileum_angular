import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    constructor(private endpointService: EndpointService, private http: HttpClient) {}

    login(body) {
        return this.http.post(this.endpointService.getLoginUrl(), body);
    }
}
