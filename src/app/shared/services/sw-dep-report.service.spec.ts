import { TestBed } from '@angular/core/testing';

import { SwDepReportService } from './sw-dep-report.service';

describe('SwDepReportService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: SwDepReportService = TestBed.get(SwDepReportService);
        expect(service).toBeTruthy();
    });
});
