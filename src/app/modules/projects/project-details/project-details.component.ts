import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BillingService } from '../../../shared/services/billing.service';
import { ToasterService } from '../../../shared';
import { ProjectService } from '../project.service';
import { ProjectDetails } from '../project.model';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-project-details',
    templateUrl: './project-details.component.html',
    styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
    projectId;
    projectDetails;
    TotalBill = '';
    TotalRevenue = '';
    TotalUnbilled = '';
    RevenueBacklog = '';

    constructor(
        private route: ActivatedRoute,
        private billingService: BillingService,
        private toasterservice: ToasterService,
        private projectservice: ProjectService,
        private ngxService: NgxUiLoaderService
    ) {}

    ngOnInit() {
        this.projectId = this.route.snapshot.paramMap.get('id');
        if (this.projectId) {
            this.getProjectDetailsById(this.projectId);
        }
    }

    onChanged(event) {
        if (event) {
            this.getProjectDetailsById(this.projectId);
        }
    }

    getProjectDetailsById(projId) {
        // Get Project Details
        const prodata: ProjectDetails = {
            projId: this.projectId
        };
        this.ngxService.start();
        this.projectservice.getProjectDetails(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    // this.regionList = result.data.projects;
                    this.projectDetails = result.data;
                    this.TotalBill = result.data.totalBill;
                    this.TotalRevenue = result.data.totalRevenue;
                    this.TotalUnbilled = result.data.totalUnbilled;
                    this.RevenueBacklog = result.data.revenueBackLog;
                } else {
                    this.toasterservice.error(result.message);
                }
                this.ngxService.stop();
            },
            error => {
                this.ngxService.stop();
                this.toasterservice.error('Something went wrong');
            }
        );
    }
}
