import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, QueryList, ViewChildren, Input } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import 'datatables.net';
import 'datatables.net-bs4';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DealDistributionService } from '../deal-distribution.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import {
    ForcastedDealDistribution,
    LatestForcastedDealDistribution,
    DeleteMilestone,
    ProjectDetails,
    ImportDeal,
    FinalDeal,
    ApproveDeal,
    RejectDeal,
    ProjectType
} from '../deal-distribution.model';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-deal-distribution',
    templateUrl: './deal-distribution.component.html',
    styleUrls: ['./deal-distribution.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class DealDistributionComponent implements OnInit, AfterViewInit {
    @ViewChild('confirmdialog') public confirmdialog: ModalDirective;
    @ViewChild('addeditdealdestributionmodel') public addeditdealdestributionmodel: ModalDirective;
    @ViewChild('viewdealDistributionmodel') public viewdealDistributionmodel: ModalDirective;

    @ViewChild('db') forcastedtable: ElementRef;
    @ViewChildren(DataTableDirective)
    dtElements: QueryList<any>;
    dtOptions: any[] = [];
    @ViewChild('db1') pmotable: ElementRef;

    dtTrigger = new Subject<any>();
    dtTrigger1 = new Subject<any>();

    @Input() projectDetails;

    dealDistributionMilestone: any[];
    dealDistributionPMO: any[];
    ProjectTypeList = [];
    pageIndex = 0;
    pageLength = 10;
    pageIndex2 = 0;
    pageLength2 = 10;
    projectId = '';
    projectAmount = 0;

    dealDistributionForm: FormGroup;
    ddSWItem: FormControl;
    ddAmount: FormControl;
    ddEndDate: FormControl;
    ddUSDValue: FormControl;
    ddPercentage: FormControl;
    ddProjType: FormControl;
    ddStartDate: FormControl;
    ddPOCurrencyValue: FormControl;
    ddId: FormControl;
    ddProjectId: FormControl;

    STDM = '';
    OppID = '';
    DealType = '';
    ValueUSD = '';
    ActualStartDate = '';
    ActualEndDate = '';
    TotalBill = '';
    TotalRevenue = '';
    TotalUnbilled = '';
    RevenueBacklog = '';
    bmName = '';
    forcated = 1;
    reallocationAmount = 0;
    ProjectCode = '';

    isFormSubmitted = false;
    isViewDealdistribution = false;

    viewDealDistibutionDetails = {
        ddSWItem: '',
        ddAmount: '',
        recurringTimeBound: '',
        ddEndDate: '',
        ddUSDValue: '',
        ddPercentage: '',
        ddProjType: '',
        ddProjTypeId: '',
        ddStartDate: '',
        ddPOCurrencyValue: '',
        ddId: '',
        ddFeeType: '',
        ddFeeName: '',
        ddFeeDescription: '',
        ddQuantity: ''
    };

    csvOptions = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: 'Deal Distrubution List :',
        useBom: true,
        noDownload: false,
        headers: ['SW', 'Percentage', 'Amount', 'Project Type', 'Start Date', 'End Date', 'PO Currency Value', 'USD Value']
    };

    pmsSearchData;
    isPmsSearched;
    fmsSearchData;
    isFmsSearched;

    constructor(
        private http: HttpClient,
        private route: ActivatedRoute,
        private dealdistributionservice: DealDistributionService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.dtTrigger1.next();

        // This is to get table instance
        // Get event when page number is changed
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                dtInstance.on('page.dt', () => {
                    const pageInfo = dtInstance.page.info();
                    dtInstance.page.len();
                    if (index === 0) {
                        this.pageIndex = pageInfo.page;
                        this.pageLength = pageInfo.length;
                    } else if (index === 1) {
                        this.pageIndex2 = pageInfo.page;
                        this.pageLength2 = pageInfo.length;
                    }
                });
            });

            dtElement.dtInstance.then((dtInstance: any) => {
                dtInstance.on('length.dt', (e, settings, len) => {
                    const pageInfo = dtInstance.page.info();
                    if (index === 0) {
                        this.pageLength = pageInfo.length;
                        this.pageIndex = pageInfo.page;
                    } else if (index === 1) {
                        this.pageLength2 = pageInfo.length;
                        this.pageIndex2 = pageInfo.page;
                    }
                });
            });
        });
    }

    ngOnInit() {
        this.projectAmount = this.projectDetails.amountUSD;
        this.reallocationAmount = this.projectDetails.realocationAmount ? this.projectDetails.realocationAmount : 0;
        this.projectId = this.route.snapshot.paramMap.get('id');
        this.gridBinding();
        this.PMOGridBinding();
        this.createFormControls();
        this.createFormDetails();
        this.pageLoadBinding();
    }

    gridBinding() {
        const that = this;
        this.dtOptions[0] = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            columnDefs: [
                {
                    searchable: false,
                    orderable: false,
                    targets: 0
                }
            ],
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata: ForcastedDealDistribution = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    ddForcasted: '1',
                    ddId: '',
                    ddProjId: this.projectId
                };
                that.dealdistributionservice.getForcastedDealDistributionList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.dealDistributionMilestone = pagedData.data.dealDistributions;
                        that.dealDistributionMilestone.forEach(element => {
                            element.showlevel1 = false;
                            element.showlevel2 = false;
                            element.ddFinalized = 1;
                            if (element.ddApprovalStatus == 0 && element.ddFinalized == 1 && !element.ddLevel1Approve) {
                                element.showlevel1 = true;
                                element.showlevel2 = false;
                            }
                            if (!element.showlevel1) {
                                if (element.ddApprovalStatus == 0 && element.ddFinalized == 1 && !element.ddLevel2Approver) {
                                    element.showlevel1 = false;
                                    element.showlevel2 = true;
                                }
                            }
                        });
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.dealDistributionMilestone = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { name: 'Sl No.', orderable: false },
                { data: 'ddSWItem' },
                { data: 'ddPercentage' },
                { data: 'ddAmount' },
                { data: 'ddProjType' },
                { data: 'ddStartDate' },
                { data: 'ddEndDate' },
                { data: 'ddPOCurrencyValue' },
                { data: 'ddAmount' },
                // { data: 'statusApproval' },
                { data: 'action', orderable: false }
            ]
        };
    }

    PMOGridBinding() {
        const that = this;
        this.dtOptions[1] = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            columnDefs: [
                {
                    searchable: false,
                    orderable: false,
                    targets: 0
                }
            ],
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex2 + 1;
                dataTablesParameters.length = this.pageLength2;
                const temdata: ForcastedDealDistribution = {
                    pageNo: this.pageIndex2 + 1,
                    pageSize: this.pageLength2,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    ddForcasted: '0',
                    ddId: '',
                    ddProjId: this.projectId
                };
                that.dealdistributionservice.getForcastedDealDistributionList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.dealDistributionPMO = pagedData.data.dealDistributions;
                        that.dealDistributionPMO.forEach(element => {
                            element.showlevel1 = false;
                            element.showlevel2 = false;
                            element.ddFinalized = 1;
                            if (element.ddApprovalStatus == 0 && element.ddFinalized == 1 && !element.ddLevel1Approve) {
                                element.showlevel1 = true;
                                element.showlevel2 = false;
                            }
                            if (!element.showlevel1) {
                                if (element.ddApprovalStatus == 0 && element.ddFinalized == 1 && !element.ddLevel2Approver) {
                                    element.showlevel1 = false;
                                    element.showlevel2 = true;
                                }
                            }
                        });
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.dealDistributionPMO = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { name: 'Sl No.', orderable: false },
                { data: 'ddSWItem' },
                { data: 'ddPercentage' },
                { data: 'ddAmount' },
                { data: 'ddProjType' },
                { data: 'ddStartDate' },
                { data: 'ddEndDate' },
                { data: 'ddPOCurrencyValue' },
                { data: 'ddAmount' },
                { data: 'action', orderable: false }
            ]
        };
    }

    search(tableIndex) {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                if (index === 0 && tableIndex === index) {
                    this.isFmsSearched = true;
                    dtInstance.search(this.fmsSearchData).draw();
                } else if (index === 1 && tableIndex === index) {
                    this.isPmsSearched = true;
                    dtInstance.search(this.pmsSearchData).draw();
                }
            });
        });
    }

    reset(tableIndex) {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                if (index === 0 && tableIndex === index) {
                    this.isFmsSearched = false;
                    this.fmsSearchData = '';
                    dtInstance.search(this.fmsSearchData).draw();
                } else if (index === 1 && tableIndex === index) {
                    this.isPmsSearched = false;
                    this.pmsSearchData = '';
                    dtInstance.search(this.pmsSearchData).draw();
                }
            });
        });
    }

    pageLoadBinding() {
        // Get Project Details
        const prodata: ProjectDetails = {
            projId: this.projectId
        };
        //this.ngxService.start();
        this.dealdistributionservice.getProjectDetails(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    //this.regionList = result.data.projects;
                    this.STDM = '';
                    this.OppID = result.data.oppId;
                    this.DealType = result.data.dealType;
                    this.ValueUSD = result.data.amountUSD;
                    this.ActualStartDate = result.data.startDate;
                    this.ActualEndDate = result.data.closeDate;
                    this.TotalBill = result.data.totalBill;
                    this.TotalRevenue = result.data.totalRevenue;
                    this.TotalUnbilled = result.data.totalUnbilled;
                    this.RevenueBacklog = result.data.revenueBackLog;
                    this.bmName = result.data.bmName;
                    this.reallocationAmount = result.data.realocationAmount ? result.data.realocationAmount : 0;
                    this.ProjectCode = result.data.projectCode;
                }
                //this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
        //// Get Project Details
        //const prodata: ProjectDetails = {
        //  projId: this.projectId
        //};
        ////this.ngxService.start();
        //setTimeout(() => {
        //  this.dealdistributionservice.getProjectDetails(prodata).subscribe(
        //    result => {
        //      if (result.success && result.data) {
        //        //this.regionList = result.data.projects;
        //        this.STDM = '';
        //        this.OppID = result.data.oppId;
        //        this.DealType = result.data.dealType;
        //        this.ValueUSD = result.data.amountUSD;
        //        this.ActualStartDate = result.data.startDate;
        //        this.ActualEndDate = result.data.closeDate;
        //        this.TotalBill = result.data.totalBill;
        //        this.TotalRevenue = result.data.totalRevenue;
        //        this.TotalUnbilled = result.data.totalUnbilled;
        //        this.RevenueBacklog = result.data.revenueBackLog;
        //        this.bmName = result.data.bmName;
        //        this.reallocationAmount = result.data.realocationAmount ? result.data.realocationAmount : 0;
        //        this.ProjectCode = result.data.projectCode;
        //      }
        //      //this.ngxService.stop();
        //    },
        //    error => {
        //      this.toasterservice.error('Something went wrong');
        //      // this.ngxService.stop();
        //    }
        //  );
        //}, 3000);

        // Get Project Type
        const protypedata: ProjectType = {
            searchKey: ''
        };
        this.dealdistributionservice.getAllProjectsTypesList(protypedata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.ProjectTypeList = result.data.projecttypes;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
    }

    createFormControls() {
        this.ddSWItem = new FormControl('', [Validators.required, Validators.pattern(/^[ A-Za-z0-9_()-\/]*$/)]);
        this.ddAmount = new FormControl('', [Validators.required, Validators.pattern(/^[0-9]*$/)]);
        this.ddEndDate = new FormControl('', [Validators.required]);
        this.ddUSDValue = new FormControl('', [Validators.pattern(/^[0-9]*$/)]);
        this.ddPercentage = new FormControl('', [Validators.required]);
        this.ddProjType = new FormControl('', [Validators.required]);
        this.ddStartDate = new FormControl('', [Validators.required]);
        this.ddPOCurrencyValue = new FormControl('', [Validators.pattern(/^[0-9]*$/)]);
        this.ddId = new FormControl('');
        this.ddProjectId = new FormControl(this.projectId);
    }

    createFormDetails() {
        this.dealDistributionForm = new FormGroup({
            ddSWItem: this.ddSWItem,
            ddAmount: this.ddAmount,
            ddEndDate: this.ddEndDate,
            ddUSDValue: this.ddUSDValue,
            ddPercentage: this.ddPercentage,
            ddProjType: this.ddProjType,
            ddStartDate: this.ddStartDate,
            ddPOCurrencyValue: this.ddPOCurrencyValue,
            ddId: this.ddId,
            ddProjectId: this.ddProjectId
        });
    }

    addDealDistribution() {
        // Get latest Deal Distribution
        const lstdata: LatestForcastedDealDistribution = {
            ddForcasted: 0,
            ddProjectId: this.projectId
        };
        this.ngxService.start();
        this.dealdistributionservice.getLatestForcastedDealDistribution(lstdata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.forcated = 1;
                    if (result.data) {
                        this.ddSWItem.setValue(result.data.ddSWItem);
                        this.ddAmount.setValue(result.data.ddAmount);
                        this.ddEndDate.setValue(result.data.ddEndDate ? new Date(result.data.ddEndDate) : '');
                        this.ddUSDValue.setValue(result.data.ddUSDValue);
                        this.ddPercentage.setValue(result.data.ddPercentage);
                        this.ddProjType.setValue(result.data.ddProjTypeId);
                        this.ddStartDate.setValue(result.data.ddStartDate);
                        this.ddPOCurrencyValue.setValue(result.data.ddPOCurrencyValue);
                        this.ddProjectId.setValue(this.projectId);
                        this.ddId.setValue('');
                    } else {
                        this.clearDealDistribution();
                    }
                    this.ngxService.stop();
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
        this.addeditdealdestributionmodel.show();
    }

    editDealDistribution(data, forcasted) {
        this.forcated = forcasted;
        this.ddSWItem.setValue(data.ddSWItem);
        this.ddAmount.setValue(data.ddAmount);
        //this.recurringTimeBound.setValue(data.);
        this.ddEndDate.setValue(data.ddEndDate ? new Date(data.ddEndDate) : '');
        this.ddUSDValue.setValue(data.ddUSDValue);
        this.ddPercentage.setValue(data.ddPercentage);
        this.ddProjType.setValue(data.ddProjTypeId);
        this.ddStartDate.setValue(data.ddStartDate);
        this.ddPOCurrencyValue.setValue(data.ddPOCurrencyValue);
        this.ddId.setValue(data.ddId);
        this.ddProjectId.setValue(this.projectId);
        this.addeditdealdestributionmodel.show();
    }

    bindDetailDealDistribution(data) {
        this.viewDealDistibutionDetails = {
            ddSWItem: data.ddSWItem,
            ddAmount: data.ddAmount,
            recurringTimeBound: '',
            ddEndDate: data.ddEndDate,
            ddUSDValue: data.ddUSDValue,
            ddPercentage: data.ddPercentage,
            ddProjType: data.ddProjType,
            ddProjTypeId: data.ddProjTypeId,
            ddStartDate: data.ddStartDate,
            ddPOCurrencyValue: data.ddPOCurrencyValue,
            ddFeeType: data.ddFeeType,
            ddFeeName: data.ddFeeName,
            ddFeeDescription: data.ddFeeDescription,
            ddQuantity: data.ddQuantity,
            ddId: data.ddId
        };
    }

    viewDealDistribution(data, forcasted) {
        this.forcated = forcasted;
        this.bindDetailDealDistribution(data);
        this.viewdealDistributionmodel.show();
    }

    openViewEditPopup(data) {
        this.editDealDistribution(data, this.forcated);
        this.isViewDealdistribution = true;
    }

    openViewDeletePopup(data) {
        this.openDeletePopup(data);
    }

    clearDealDistribution() {
        this.ddSWItem.setValue('');
        this.ddAmount.setValue('');
        this.ddEndDate.setValue('');
        this.ddUSDValue.setValue('');
        this.ddPercentage.setValue('');
        this.ddProjType.setValue('');
        this.ddStartDate.setValue('');
        this.ddPOCurrencyValue.setValue('');
        this.ddId.setValue('');
    }

    cancelDealDistriution() {
        this.clearDealDistribution();
        this.dealDistributionForm.reset();
        this.addeditdealdestributionmodel.hide();
    }

    cancelViewDealDistriution() {
        this.clearDealDistribution();
        this.dealDistributionForm.reset();
        this.viewdealDistributionmodel.hide();
        this.isViewDealdistribution = false;
    }

    dateValidation() {
        if (this.ddStartDate.value && this.ddEndDate.value) {
            if (new Date(this.ddStartDate.value) > new Date(this.ddEndDate.value)) {
                this.toasterservice.error('Enter End date is greater than Start date');
                return false;
            }
            return true;
        }
    }

    blurAmount() {
        if (this.reallocationAmount != 0) {
            this.ddPercentage.setValue(((this.ddAmount.value * 100) / this.reallocationAmount).toFixed(2));
        } else {
            this.ddPercentage.setValue('');
        }
    }

    addUpdateDealDistribution() {
        if (this.dealDistributionForm.valid) {
            if (this.dateValidation()) {
                this.isFormSubmitted = false;
                this.ngxService.start();
                this.dealdistributionservice.addUpdateDealDistributionMilestone(this.dealDistributionForm.value).subscribe(
                    result => {
                        if (result && result.success) {
                            this.pageIndex = 0;
                            this.pageLength = 10;
                            this.rerenderGrid(this.forcated);
                            this.toasterservice.showSuccess(result.message);
                            if (this.isViewDealdistribution) {
                                this.bindDetailDealDistribution(this.dealDistributionForm.value);
                            }
                            this.cancelDealDistriution();
                        } else if (result && !result.success) {
                            this.toasterservice.error(result.message);
                        } else {
                            this.toasterservice.error('Something went wrong');
                        }
                        this.ngxService.stop();
                    },
                    error => {
                        this.toasterservice.error('Something went wrong');
                        this.ngxService.stop();
                    }
                );
            }
        } else {
            this.isFormSubmitted = true;
        }
    }

    openDeletePopup(data) {
        this.ddId.setValue(data.ddId);
        this.confirmdialog.show();
        this.confirmdialog.show();
    }

    cancelDelete() {
        this.ddId.setValue('');
        this.confirmdialog.hide();
        // this.viewdealDistributionmodel.hide();
    }

    deleteMilestone() {
        const data: DeleteMilestone = {
            deleteArray: [this.ddId.value],
            status: 0
        };
        this.ngxService.start();
        this.dealdistributionservice.deleteForcastedDealDistribution(data).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid(1);
                    this.toasterservice.showSuccess(result.message);
                    this.cancelDelete();
                    this.isViewDealdistribution = false;
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    rerenderGrid(forcasted): void {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            if (forcasted == 1 && index === 0) {
                dtElement.dtInstance.then((dtInstance: any) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger.next();
                });
            } else if (forcasted == 0 && index === 1) {
                dtElement.dtInstance.then((dtInstance: any) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger1.next();
                });
            }
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        this.dtTrigger1.unsubscribe();
    }

    importFile($event, forcasted, id): void {
        this.readThis($event.target, forcasted, id);
    }

    readThis(inputValue: any, forcasted, id): void {
        const file: File = inputValue.files[0];
        if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            this.toasterservice.error('Only xlsx files are supported.');
            return;
        }
        const myReader: FileReader = new FileReader();

        myReader.onloadend = e => {
            const strresult = myReader.result.toString();
            const base64 = strresult.split('base64,');

            const prodata: ImportDeal = {
                forcasted: forcasted,
                fileValue: base64[1],
                projId: this.projectId
            };
            this.ngxService.start();
            this.dealdistributionservice.importDealDistribution(prodata).subscribe(
                result => {
                    if (result.success && result.data) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid(forcasted);
                        this.toasterservice.showSuccess(result.message);
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    document.getElementById(id)['value'] = '';
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        };
        myReader.readAsDataURL(file);
    }

    FinalizeDealDistribution(forcasted) {
        const prodata: FinalDeal = {
            ddProjId: this.projectId,
            ddForcasted: forcasted
        };
        this.ngxService.start();
        this.dealdistributionservice.finalizedDealDistribution(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid(forcasted);
                    this.toasterservice.showSuccess(result.message);
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    ApproveStatus(ddid, level, sentlevel, forcasted) {
        const prodata: ApproveDeal = {
            ddID: ddid,
            level: level,
            sentlevel: sentlevel
        };
        this.ngxService.start();
        this.dealdistributionservice.approvedDealDistribution(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid(forcasted);
                    this.toasterservice.showSuccess(result.message);
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    DeniedStatus(ddid, level, forcasted) {
        const prodata: RejectDeal = {
            ddID: ddid,
            level: level
        };
        this.ngxService.start();
        this.dealdistributionservice.rejectDealDistribution(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid(forcasted);
                    this.toasterservice.showSuccess(result.message);
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }
}
