import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../..';
import { ToasterService } from '../../../shared';
import { ResetPasswordComponent } from '../../../modules/auth/reset-password/reset-password.component';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
    providedIn: 'root'
})
export class ResetPasswordGuard implements CanActivate {
    constructor(
        private sessionService: SessionService,
        private router: Router,
        private customToasterService: ToasterService,
        private modalService: NgbModal,
        config: NgbModalConfig
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.sessionService.getSession()) {
            const userDetails = this.sessionService.getSession();
            if (userDetails.data.forceResetPassword === 1) {
                this.customToasterService.warning('Please reset your password');
                this.openResetPasswordModal();
            }
        }
        return true;
    }

    openResetPasswordModal() {
        let isPasswordReset;
        const modalRef = this.modalService.open(ResetPasswordComponent, {
            windowClass: 'dark-modal',
            size: 'xl' as any,
            centered: true
        });

        modalRef.result.then(
            result => {
                if (result.success) {
                    isPasswordReset = true;
                    this.router.navigate(['', 'auth', 'login']);
                }
            },
            reason => {
                if (reason.success) {
                }
            }
        );
    }
}
