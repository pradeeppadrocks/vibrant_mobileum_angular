import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';

@Injectable({
    providedIn: 'root'
})
export class SwDepReportService {
    constructor(private baseService: BaseService, private endpointService: EndpointService) {}

    getSwDepData(body) {
        return this.baseService.post(this.endpointService.getSfDependencyUrl(), body);
    }
}
