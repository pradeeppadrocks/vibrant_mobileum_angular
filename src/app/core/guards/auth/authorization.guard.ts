import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { SessionService } from '../../services/session.service';
import { AuthenticationService } from '../../../shared';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {
    constructor(private auth: AuthenticationService, private router: Router, private sessionService: SessionService) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        if (this.auth.isTokenExpired()) {
            this.router.navigate(['', 'auth', 'login']);
            return false;
        }
        // this will be passed from the route config
        // on the data property
        const expectedRole = route.data.expectedRole;
        if (!expectedRole) {
            return true;
        }
        this.auth.checkAuthorization(expectedRole).subscribe(authorized => {
            if (!this.sessionService.getSession()) {
                this.router.navigate(['', 'auth', 'login']);
                return false;
            }

            if (!authorized) {
                this.router.navigate(['', 'auth', 'unauthenticated']);
                return false;
            }
        });
        return true;
    }
}
