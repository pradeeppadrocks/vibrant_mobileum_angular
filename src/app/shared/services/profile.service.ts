import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    constructor(private baseService: BaseService, private endpointService: EndpointService) {}

    updateProfile(body) {
        return this.baseService.post(this.endpointService.getUpdateProfileUrl(), body);
    }
}
