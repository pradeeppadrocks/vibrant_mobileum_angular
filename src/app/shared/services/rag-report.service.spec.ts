import { TestBed } from '@angular/core/testing';

import { RagReportService } from './rag-report.service';

describe('RagReportService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: RagReportService = TestBed.get(RagReportService);
        expect(service).toBeTruthy();
    });
});
