import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of as observableOf } from 'rxjs';
import { EncDecrService } from './enc-decr.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as CryptoJS from 'crypto-js';
import { element } from 'protractor';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService implements OnInit {
    private userRoles: Set<string>;
    private roles: any;
    public keyName = 'mobUserDetails';
    private userData = 'data';
    public redirectUrl: string;

    constructor(private http: HttpClient, private encrDecrService: EncDecrService) {
        this.roles = this.getUserData().user_access;
        this.constructPermission(this.roles);
    }

    ngOnInit() {}

    // returns a boolean observable
    public checkAuthorization(path: any): Observable<boolean> {
        return observableOf(this.doCheckAuthorization(path));
    }

    public isTokenExpired() {
        /* TODO: Revisit the code to enable check if token is expired or not once back-end changes to add header is done */
        const helper = new JwtHelperService();
        const jwtRawToken = this.getJwtToken();
        if (jwtRawToken) {
            const decodeToken = helper.decodeToken(jwtRawToken);
            const expTime = decodeToken.expDateTime;
            const currentTime = new Date().getTime();
            return currentTime > expTime;
        }
        return true;
    }

    private getUserData(key: string = this.keyName, userData: string = this.userData) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item && item.hasOwnProperty(userData)) {
            return item[userData];
        }
        return {};
    }

    getDecryptUserData() {
        return this.encrDecrService.decryptData(this.getUserData());
    }

    private doCheckAuthorization(path: string): boolean {
        this.roles = this.getUserData().user_access;
        this.constructPermission(this.roles);
        if (path.length) {
            if (path) {
                return this.userRoles.has(path);
            }
        }
        return false;

        // if (path.length) {
        //   if (path
        //     && this.userRoles.size) {
        //     return path.some(permittedRole => this.userRoles.has(permittedRole));
        //   }
        //   return false;
        // }
        // return false;
    }

    private constructPermission(role: any) {
        this.userRoles = new Set<string>();
        if (!role) {
            return;
        }

        Object.entries(role).forEach(([key, value]) => {
            const mod = key.toString().toUpperCase();
            const access = value.toString().split(',');
            let roleFound = false;
            if (access[0] === '1') {
                this.userRoles.add(`${mod}-CREATE`);
                roleFound = true;
            }

            if (access[1] === '1') {
                this.userRoles.add(`${mod}-READ`);
                roleFound = true;
            }

            if (access[2] === '1') {
                this.userRoles.add(`${mod}-UPDATE`);
                roleFound = true;
            }

            if (access[3] === '1') {
                this.userRoles.add(`${mod}-DELETE`);
                roleFound = true;
            }

            if (roleFound) {
                this.userRoles.add(mod);
            }
        });
    }

    private getJwtToken() {
        const item = localStorage.getItem('access_token');
        if (item) {
            return item;
        }
        return null;
    }
}
