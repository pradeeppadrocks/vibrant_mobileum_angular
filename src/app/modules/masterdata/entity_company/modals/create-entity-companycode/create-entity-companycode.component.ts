import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EntityService, ToasterService } from '../../../../../shared';

@Component({
    selector: 'app-create-entity-companycode',
    templateUrl: './create-entity-companycode.component.html',
    styleUrls: ['./create-entity-companycode.component.css']
})
export class CreateEntityCompanycodeComponent implements OnInit {
    entityCompanyForm: FormGroup;
    entityCode: FormControl;
    isFormSubmitted: boolean;

    constructor(public activeModal: NgbActiveModal, private entityService: EntityService, private toasterService: ToasterService) {}

    ngOnInit() {
        this.createFormControls();
        this.createFormDetails();
    }

    createFormControls() {
        this.entityCode = new FormControl('', [Validators.required]);
    }

    createFormDetails() {
        this.entityCompanyForm = new FormGroup({
            entityCode: this.entityCode
        });
    }

    addEntity() {
        if (this.entityCompanyForm.valid) {
            this.isFormSubmitted = false;
            // Make backend call to save data
            this.entityService.addEntity(this.entityCompanyForm.value).subscribe((res: any) => {
                if (res.success) {
                    this.toasterService.showSuccess(res.message);
                    this.activeModal.close(res);
                } else {
                    this.toasterService.error(res.message);
                }
            });
        } else {
            this.isFormSubmitted = true;
        }
    }
}
