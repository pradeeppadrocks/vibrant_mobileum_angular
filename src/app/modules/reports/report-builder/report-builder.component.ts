import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from '../../../core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { RegionChangeService, ReportBuilderService } from '../../../shared';
import { AddEditReportComponent } from './modals/add-edit-report/add-edit-report.component';
import { DeleteModalComponent } from './modals/delete-modal/delete-modal.component';
import { ViewReportComponent } from './modals/view-report/view-report.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-report-builder',
    templateUrl: './report-builder.component.html',
    styleUrls: ['./report-builder.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class ReportBuilderComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger = new Subject<any>();

    dtOptions: any = {};
    pageIndex = 0;
    pageLength = 10;
    regions = '';
    subscriptions;

    existingReports: any[];

    constructor(
        private modalService: NgbModal,
        config: NgbModalConfig,
        private ngxService: NgxUiLoaderService,
        private regionUpdateService: RegionChangeService,
        private reportBuilderService: ReportBuilderService,
        private router: Router
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {
        this.regions = this.reportBuilderService.getUpdatedRegion();
        this.renderReportListing();
        this.subscriptions = this.regionUpdateService.region.subscribe(value => {
            this.regions = value;
            this.reRenderReportListingTable();
        });
    }

    ngAfterViewInit() {
        this.dtTrigger.next();
        // This is to get table instance
        // Get event when page number is changed

        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    renderReportListing() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                this.initializeTableIndex(dataTablesParameters);
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : -1,
                    searchKey: dataTablesParameters.search.value,
                    regions: this.regions
                };

                this.reportBuilderService.getReportList(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.existingReports = pagedData.data.reportMeta;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.existingReports = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [{ data: 'report_meta_name', orderable: true }]
        };
    }

    reRenderReportListingTable() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    initializeTableIndex(params) {
        this.pageIndex = params.start / params.length;
        this.pageLength = params.length;
    }
    openAddOrEditReportModal(btnContent, id?: number, name?: any) {
        const modRef = this.modalService.open(AddEditReportComponent, { size: 'xl' as any, centered: true });
        modRef.componentInstance.reportId = id;
        modRef.componentInstance.btnContent = btnContent;
        modRef.componentInstance.reportName = name;

        modRef.result.then(
            result => {
                if (result.success) {
                    // this.refreshDataTable(fms, numberOfElements);
                    this.reRenderReportListingTable();
                }
            },
            reason => {}
        );
    }

    openDeleteModal(reportId) {
        const modRef = this.modalService.open(DeleteModalComponent, { size: 'md' as any, centered: true });
        modRef.componentInstance.reportId = reportId;

        modRef.result.then(
            result => {
                if (result.success) {
                    // this.refreshDataTable(fms, numberOfElements);
                    this.reRenderReportListingTable();
                }
            },
            reason => {}
        );
    }

    openViewReportModal(reportId, reportName) {
        /* const modRef = this.modalService.open(ViewReportComponent, { size: 'xl' as any, centered: true });
        modRef.componentInstance.reportId = reportId;
        modRef.componentInstance.reportName = reportName;

        modRef.result.then(
            result => {
                if (result.success) {
                    // this.refreshDataTable(fms, numberOfElements);
                }
            },
            reason => {}
        );*/
        this.router.navigate(['', 'reports', 'builder', reportId]);
    }

    backToReportBuilder() {
        this.router.navigate(['', 'reports', 'builder']);
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        if (!!this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }
}
