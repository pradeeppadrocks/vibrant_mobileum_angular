import { Component, Input, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RevenueService } from '../revenue.service';
import { CONSTANTS } from 'src/app/shared/constant';
import { ToasterService } from '../../../../shared';

@Component({
    selector: 'app-lob-revenue-modal',
    templateUrl: './lob-revenue-modal.component.html',
    styleUrls: ['./lob-revenue-modal.component.css']
})
export class LOBRevenueModalComponent implements OnInit {
    @Input() LOBList;
    @Input() ProjectCode;

    LineOfBusinessList = [];
    LOBProjectCode = '';

    constructor(public activeModal: NgbActiveModal, private customToasterService: ToasterService) {}

    ngOnInit() {
        this.LineOfBusinessList = this.LOBList;
        this.LOBProjectCode = this.ProjectCode;
    }
}
