import { Directive, Input } from '@angular/core';
import { AbstractControl, ValidatorFn, Validator, NG_VALIDATORS } from '@angular/forms';
import { from } from 'rxjs';
import { Key } from 'protractor';

@Directive({
    selector: '[appPasswordMatching]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: PasswordMatchingDirective,
            multi: true
        }
    ]
})
export class PasswordMatchingDirective implements Validator {
    @Input() appPasswordMatching: string;

    constructor() {}

    validate(control: AbstractControl): { [Key: string]: any } | null {
        const controlToCompare = control.parent.get(this.appPasswordMatching);
        if (controlToCompare && controlToCompare.value !== control.value) {
            return { notEqual: true };
        }
        return null;
    }
}
