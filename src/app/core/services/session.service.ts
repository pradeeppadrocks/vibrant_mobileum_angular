import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { EncDecrService } from 'src/app/shared';

@Injectable()
export class SessionService {
    constructor(private encrDecrService: EncDecrService) {}
    public keyName = 'mobUserDetails';
    private tokenName = 'user_id';
    private userId = 'user_id';
    private userName = 'user_name';
    private lastName = 'last_name';
    private jwtToken = 'access_token';
    private userData = 'data';

    getJwtToken() {
        const item = localStorage.getItem('access_token');
        if (item) {
            return item;
        }
        return null;
    }

    public setJwtToken(token) {
        if (token) {
            localStorage.setItem('access_token', token);
        }
    }

    /**
     * Set session item
     * @param key, item
     * @returns boolean
     */
    public setSession(value: any, key: string = this.keyName) {
        localStorage.setItem(key, JSON.stringify(value));
        const insertItem = this.getSession(key);
        if (insertItem) {
            return true;
        }
    }

    /**
     * Get session item
     * @param key
     * @returns Object
     */
    public getSession(key: string = this.keyName) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item) {
            return item;
        } else {
            return null;
        }
    }

    /**
     * Remove session item
     * @param key
     * @returns boolean
     */
    removeSession(key: string = this.keyName) {
        localStorage.removeItem(key);
        const insertItem = this.getSession(key);
        if (!insertItem) {
            return true;
        }
    }

    /**
     * Clear session
     * @returns boolean
     */
    public clearSession() {
        localStorage.clear();
        return true;
    }

    /**
     * Get token
     * @param key, tokenName
     * @returns string
     */
    public getToken(key: string = this.keyName, tokenName: string = this.tokenName) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item && item.hasOwnProperty(tokenName)) {
            return item[tokenName];
        }
        return null;
    }

    public getUserData(key: string = this.keyName, userData: string = this.userData) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item && item.hasOwnProperty(userData)) {
            return item[userData];
        }
        return null;
    }

    public getDecryptUserData() {
        return this.encrDecrService.decryptData(this.getUserData());
    }

    /**
     * Check login status
     * @param key, tokenName
     * @returns boolean
     */
    public checkLogin(key: string = this.keyName, tokenName: string = this.tokenName) {
        if (this.getToken(key, tokenName) != null) {
            return true;
        }
        return false;
    }

    public getRole() {
        if (localStorage.getItem(this.keyName)) {
            return JSON.parse(localStorage.getItem(this.keyName)).is_admin;
        }
    }

    public getUserId(key: string = this.keyName, itemName = this.userId) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item && item.hasOwnProperty(itemName)) {
            return item[itemName];
        }
        return null;
    }

    public getUsername(key: string = this.keyName, itemName = this.userName) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item && item.hasOwnProperty(itemName)) {
            return item[itemName];
        }
        return null;
    }

    public getLastName(key: string = this.keyName, itemName = this.lastName) {
        const item = JSON.parse(localStorage.getItem(key));
        if (item && item.hasOwnProperty(itemName)) {
            return item[itemName];
        }
        return null;
    }
}
