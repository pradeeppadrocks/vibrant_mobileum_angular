import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';
import { RegionChangeService } from './region-change.service';

@Injectable({
    providedIn: 'root'
})
export class DependencyReportService {
    updatedRegion: any;
    constructor(
        private endpointService: EndpointService,
        private baseService: BaseService,
        private regionUpdateService: RegionChangeService
    ) {
        this.regionUpdateService.region.subscribe(value => {
            this.updatedRegion = value;
        });
    }

    getUpdatedRegion() {
        return this.updatedRegion;
    }
}
