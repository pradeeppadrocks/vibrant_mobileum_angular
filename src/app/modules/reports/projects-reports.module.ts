import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared-module';
import { ProjectsReportsComponent } from './projects-reports-billing/projects-reports.component';
import { ProjectsReportsRoutingModule } from './projects-reports-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RevenueReportComponent } from './revenue-report/revenue-report.component';
import { ReportBuilderComponent } from './report-builder/report-builder.component';
import { AddEditReportComponent } from './report-builder/modals/add-edit-report/add-edit-report.component';
import { DeleteModalComponent } from './report-builder/modals/delete-modal/delete-modal.component';
import { ViewReportComponent } from './report-builder/modals/view-report/view-report.component';
import { SwDepComponent } from './dependency/sw-dep/sw-dep.component';
import { HwDepComponent } from './dependency/hw-dep/hw-dep.component';
import { CustDepComponent } from './dependency/cust-dep/cust-dep.component';
import { PaperWorkComponent } from './dependency/paper-work/paper-work.component';
import { RagBillingRedComponent } from './rag/billing/rag-billing-red/rag-billing-red.component';
import { RagBillingGreenComponent } from './rag/billing/rag-billing-green/rag-billing-green.component';
import { RagBillingAmberComponent } from './rag/billing/rag-billing-amber/rag-billing-amber.component';
import { RagRevenueRedComponent } from './rag/revenue/rag-revenue-red/rag-revenue-red.component';
import { RagRevenueGreenComponent } from './rag/revenue/rag-revenue-green/rag-revenue-green.component';
import { RagRevenueAmberComponent } from './rag/revenue/rag-revenue-amber/rag-revenue-amber.component';

@NgModule({
    declarations: [
        ProjectsReportsComponent,
        RevenueReportComponent,
        ReportBuilderComponent,
        AddEditReportComponent,
        DeleteModalComponent,
        ViewReportComponent,
        SwDepComponent,
        HwDepComponent,
        CustDepComponent,
        PaperWorkComponent,
        RagBillingRedComponent,
        RagBillingGreenComponent,
        RagBillingAmberComponent,
        RagRevenueRedComponent,
        RagRevenueGreenComponent,
        RagRevenueAmberComponent
    ],
    imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule],
    exports: [ProjectsReportsComponent, ProjectsReportsRoutingModule, ReportBuilderComponent],
    entryComponents: [AddEditReportComponent, DeleteModalComponent]
})
export class ProjectsReportsModule {}
