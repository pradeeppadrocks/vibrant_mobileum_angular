import { Injectable } from '@angular/core';
import { BaseService } from './../../../shared/services/base.service';
import { Revenue, ProjectDetails, ImportRev } from './revenue.model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class RevenueService {
    //DevURL = 'http://192.168.1.142:9051/';
    DevURL = environment.host;

    //Revenue URL
    getRevenueListURL = `${this.DevURL}getRevenue`;
    getProjectDetailURL = `${this.DevURL}getProjectDetail`;
    importRevenueURL = `${this.DevURL}importRevenue`;
    constructor(private baseService: BaseService) {}

    //Revenue API
    getRevenueList(data: Revenue) {
        return this.baseService.post(this.getRevenueListURL, data);
    }

    getProjectDetails(data: ProjectDetails) {
        return this.baseService.post(this.getProjectDetailURL, data);
    }

    importRevenue(data: ImportRev) {
        return this.baseService.post(this.importRevenueURL, data);
    }
}
