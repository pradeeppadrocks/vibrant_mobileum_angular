import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RevenueReportComponent } from './revenue-report/revenue-report.component';
import { ReportBuilderComponent } from './report-builder/report-builder.component';
import { ViewReportComponent } from './report-builder/modals/view-report/view-report.component';
import { SwDepComponent } from './dependency/sw-dep/sw-dep.component';
import { HwDepComponent } from './dependency/hw-dep/hw-dep.component';
import { CustDepComponent } from './dependency/cust-dep/cust-dep.component';
import { PaperWorkComponent } from './dependency/paper-work/paper-work.component';
import { RagBillingRedComponent } from './rag/billing/rag-billing-red/rag-billing-red.component';
import { RagBillingGreenComponent } from './rag/billing/rag-billing-green/rag-billing-green.component';
import { RagBillingAmberComponent } from './rag/billing/rag-billing-amber/rag-billing-amber.component';
import { ProjectsReportsComponent } from './projects-reports-billing/projects-reports.component';

const routes: Routes = [
    { path: '', redirectTo: 'billing' },
    { path: 'billing', component: ProjectsReportsComponent },
    { path: 'revenue', component: RevenueReportComponent },
    { path: 'builder', component: ReportBuilderComponent },
    { path: 'builder/:id', component: ViewReportComponent },
    { path: 'sw-dep', component: SwDepComponent },
    { path: 'hw-dep', component: HwDepComponent },
    { path: 'cust-dep', component: CustDepComponent },
    { path: 'paperwork', component: PaperWorkComponent },
    { path: 'rag-billing-red', component: RagBillingRedComponent },
    { path: 'rag-billing-green', component: RagBillingGreenComponent },
    { path: 'rag-billing-amber', component: RagBillingAmberComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProjectsReportsRoutingModule {}
