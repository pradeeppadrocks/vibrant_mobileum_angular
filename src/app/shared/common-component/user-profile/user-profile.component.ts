import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CONSTANTS } from '../../constant';
import { SessionService } from '../../../core';
import { ProfileService, ToasterService } from '../..';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
    profileForm: FormGroup;
    userId: FormControl;
    username: FormControl;
    password: FormControl;
    retypePassword: FormControl;
    currentPassword: FormControl;
    name: FormControl;
    regId: FormControl;
    email: FormControl;
    mobile: FormControl;
    userDefaultAccess: FormControl;
    profileImageStatus: FormControl;
    gender: FormControl;
    profileImage: FormControl;
    isFormSubmitted = false;

    public imagePath;
    imgURL: any;
    public message: string;
    imgSizeErrMsg;
    imgBase64Data;

    userDetails;

    constructor(
        private sessionService: SessionService,
        private profileService: ProfileService,
        private customToasterService: ToasterService,
        private router: Router
    ) {
        this.getUserDetailsFromStorage();
        this.createFormControls();
        this.createForm();
    }

    ngOnInit() {}

    getUserDetailsFromStorage() {
        this.userDetails = this.sessionService.getSession().data;
    }

    createFormControls() {
        this.username = new FormControl(this.userDetails.username, [Validators.required, Validators.pattern(CONSTANTS.regex.username)]);
        this.password = new FormControl('', [
            Validators.minLength(CONSTANTS.regex.passwordMinLength),
            Validators.maxLength(CONSTANTS.regex.passwordMaxLength),
            // Validators.required,
            Validators.pattern(CONSTANTS.regex.confirmpassword)
        ]);
        this.retypePassword = new FormControl('', [
            Validators.minLength(CONSTANTS.regex.passwordMinLength),
            Validators.maxLength(CONSTANTS.regex.passwordMaxLength),
            // Validators.required,
            Validators.pattern(CONSTANTS.regex.confirmpassword)
        ]);
        this.currentPassword = new FormControl('', Validators.required);
        this.name = new FormControl(this.userDetails.name, [Validators.required]);
        this.email = new FormControl(this.userDetails.email, [Validators.required, Validators.pattern(CONSTANTS.regex.email)]);
        this.mobile = new FormControl(this.userDetails.mobile, [Validators.required, Validators.pattern(CONSTANTS.regex.phone)]);
        this.userId = new FormControl(this.userDetails.userId);
        this.profileImageStatus = new FormControl(0);
        this.gender = new FormControl(this.userDetails.gender, Validators.required);
        this.profileImage = new FormControl('');
        this.imgURL = this.userDetails.profileImageUrl + this.userDetails.userPhoto;
    }

    createForm() {
        this.profileForm = new FormGroup({
            username: this.username,
            currentPassword: this.currentPassword,
            password: this.password,
            retypePassword: this.retypePassword,
            name: this.name,
            email: this.email,
            mobile: this.mobile,
            userId: this.userId,
            profileImageStatus: this.profileImageStatus,
            gender: this.gender,
            profileImage: this.profileImage
        });
    }

    preview(files) {
        if (files.length === 0) {
            return;
        }

        if (files[0].size > 512000) {
            window.alert('Image size should not exceed 500kb');
            this.imgSizeErrMsg = true;
            return;
        }

        const mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = 'Only images are supported.';
            return;
        } else {
            this.message = '';
        }

        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = event => {
            this.imgURL = reader.result;
            this.imgBase64Data = this.imgURL.split('base64,')[1];
            this.profileForm.controls.profileImageStatus.setValue(1);
            this.profileForm.controls.profileImage.setValue(this.imgBase64Data);
        };
    }

    updateProfile() {
        if (this.profileForm.valid && !this.message && !this.imgSizeErrMsg) {
            // Make backend call to save user after success response redirect to login
            this.profileService.updateProfile(this.profileForm.value).subscribe((res: any) => {
                if (res.success) {
                    this.customToasterService.showSuccess('Your profile updated successfully');
                    this.isFormSubmitted = false;
                    this.profileForm.reset();
                    this.imgURL = '';
                    document.getElementById('image').setAttribute('value', '');
                    this.router.navigate(['', 'auth', 'login']);
                } else {
                    this.customToasterService.error(res.message);
                }
            });
        } else {
            this.isFormSubmitted = true;
        }
    }

    cancel() {
        this.profileForm.reset();
        this.imgURL = '';
        document.getElementById('image').setAttribute('value', '');
    }

    removeImage() {
        this.imgURL = '';
        document.getElementById('image').setAttribute('value', '');
        this.profileForm.controls.profileImage.setValue('');
    }

    loadDefaultImg(event) {
        this.imgURL = '../../../../assets/images/dummy-img.jpg';
    }
}
