import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class EndpointService {
    constructor() {}

    // Login
    loginUrl = this.getBaseUrl() + 'login';
    ReportExportUrl = this.getBaseUrl() + '';

    // Milestone url
    forecastedMilestoneUrl = this.getBaseUrl() + 'getForcastedMilestones';
    deleteMilestoneUrl = this.getBaseUrl() + 'deleteForcastedMilestones';
    addForcastedMilestonesUrl = this.getBaseUrl() + 'addForcastedMilestones';
    updateForecastedUrl = this.getBaseUrl() + 'updateForcastedMilestones';
    updatedPmsUrl = this.getBaseUrl() + 'updateActualMilestones';
    milestoneUrl = this.getBaseUrl() + 'getMilestones';
    finalizeMilestoneUrl = this.getBaseUrl() + 'finalizedMilestone';
    approvedMsUrl = this.getBaseUrl() + 'approvedMilestones';
    rejectMilestoneUrl = this.getBaseUrl() + 'rejectMilestones';
    latestAddedForecastUrl = this.getBaseUrl() + 'getLatestForcastedMilestones';
    mappPmsUrl = this.getBaseUrl() + 'milestoneMaped';
    triggerMsUrl = this.getBaseUrl() + 'triggeredMilestones';
    msTypeUrl = this.getBaseUrl() + 'getMilestonesTypes';

    // Progects url
    projectUrl = this.getBaseUrl() + 'getProjects';
    importMilestoneUrl = this.getBaseUrl() + 'importMilestone';
    projectDetailByIdUrl = this.getBaseUrl() + 'getProjectDetail';

    // Timelive url
    timeliveUrl = this.getBaseUrl() + 'getProjectLoe';
    additionaAmountDataUrl = this.getBaseUrl() + 'updateAdditionalLoeData';
    timeliveEffortUrl = this.getBaseUrl() + 'updateEffortsLoeData';
    importTimeliveUrl = this.getBaseUrl() + 'importLoeData';

    // Approval url
    approvalProjectListUrl = this.getBaseUrl() + 'getApproveMilestones';

    // Billing dependency
    allPojectsDependency = this.getBaseUrl() + 'getAllPojectsDependency';
    dependencyOwnerListUrl = this.getBaseUrl() + 'getUsersList';

    // Reports url
    billingBacklogWaterfallReportUrl = this.getBaseUrl() + 'getBillingWaterfall';
    billingWaterfallExportUrl = this.getBaseUrl() + 'getBillingWaterfallExport';
    revenueWaterfallReportUrl = this.getBaseUrl() + 'getRevenueWaterfall';
    revenueWaterfallExportUrl = this.getBaseUrl() + 'getRevenueWaterfallExport';

    // Report Builder Url
    reportFieldsUrl = this.getBaseUrl() + 'reportFields';
    addReportUrl = this.getBaseUrl() + 'addReportsMeta';
    editReportUrl = this.getBaseUrl() + 'updateReportsMeta';
    deleteReportUrl = this.getBaseUrl() + 'deleteReportsMeta';
    reportListUrl = this.getBaseUrl() + 'getReportsMeta';
    reportMetaDataUrl = this.getBaseUrl() + 'getReportsMetaDetails';
    reportDetailsViewUrl = this.getBaseUrl() + 'getCustomReport';

    // Sfdep url
    sfdepUrl = this.getBaseUrl() + 'getSWDependencyReport';

    // Reset Password
    resetPasswordUrl = this.getBaseUrl() + 'resetPassword';

    // Profile Url
    updateProfileUrl = this.getBaseUrl() + 'updateUserProfile';

    // Entity Url
    addEntityUrl = this.getBaseUrl() + 'addEntityCode';
    editEntityUrl = this.getBaseUrl() + 'updateEntityCode';
    entityListUrl = this.getBaseUrl() + 'getEntityCode';
    deleteEntityUrl = this.getBaseUrl() + 'deleteEntityCode';
    projectEntityListUrl = this.getBaseUrl() + 'getEntity';

    // Projects url
    importAllMsUrl = this.getBaseUrl() + 'importAllMilestone ';

    // Rag Url
    billingRagReportUrl = this.getBaseUrl() + 'billingRagReport';

    getBaseUrl() {
        return environment.host;
    }

    getLoginUrl() {
        return this.loginUrl;
    }

    getReportExportUrl() {
        return this.ReportExportUrl;
    }

    getForecastedMilestone() {
        return this.forecastedMilestoneUrl;
    }

    getDeleteMilestoneUrl() {
        return this.deleteMilestoneUrl;
    }

    getProjectsUrl() {
        return this.projectUrl;
    }

    getAddForecastedMilestoneUrl() {
        return this.addForcastedMilestonesUrl;
    }

    getUpdateForecastedUrl() {
        return this.updateForecastedUrl;
    }

    getUpdatedPmsUrl() {
        return this.updatedPmsUrl;
    }

    getMilestoneUrl() {
        return this.milestoneUrl;
    }

    getFinalizeMilestoneUrl() {
        return this.finalizeMilestoneUrl;
    }

    getApproveMilestoneUrl() {
        return this.approvedMsUrl;
    }

    getRejectMilestoneUrl() {
        return this.rejectMilestoneUrl;
    }

    getImportMilestoneUrl() {
        return this.importMilestoneUrl;
    }

    getTimeliveUrl() {
        return this.timeliveUrl;
    }

    getApproveProjectListUrl() {
        return this.approvalProjectListUrl;
    }

    getProjectDetailsUrl() {
        return this.projectDetailByIdUrl;
    }

    getLatesAddedMilestoneUrl() {
        return this.latestAddedForecastUrl;
    }

    getAllPojectDependency() {
        return this.allPojectsDependency;
    }

    getMapPmsUrl() {
        return this.mappPmsUrl;
    }

    getDependencyOwnerListUrl() {
        return this.dependencyOwnerListUrl;
    }

    getTriggerMsUrl() {
        return this.triggerMsUrl;
    }

    getBillingBacklogWaterfallReportUrl() {
        return this.billingBacklogWaterfallReportUrl;
    }

    getAdditionalAmountDataUrl() {
        return this.additionaAmountDataUrl;
    }

    getBillingwaterfallExportUrl() {
        return this.billingWaterfallExportUrl;
    }

    getResetPasswordUrl() {
        return this.resetPasswordUrl;
    }

    getSfDependencyUrl() {
        return this.sfdepUrl;
    }

    getRevenueWaterfallReportUrl() {
        return this.revenueWaterfallReportUrl;
    }

    getRevenueWaterfallExportUrl() {
        return this.revenueWaterfallExportUrl;
    }

    getUpdateProfileUrl() {
        return this.updateProfileUrl;
    }

    getMsTypeUrl() {
        return this.msTypeUrl;
    }

    getAddEntityUrl() {
        return this.addEntityUrl;
    }

    getEditEntityUrl() {
        return this.editEntityUrl;
    }

    getEntityListUrl() {
        return this.entityListUrl;
    }

    getEntityDeleteUrl() {
        return this.deleteEntityUrl;
    }

    getProjectEntityurl() {
        return this.projectEntityListUrl;
    }

    getTimeliveEffortUrl() {
        return this.timeliveEffortUrl;
    }

    getImportTimeliveUrl() {
        return this.importTimeliveUrl;
    }

    getReportFieldsUrl() {
        return this.reportFieldsUrl;
    }

    getAddReportUrl() {
        return this.addReportUrl;
    }

    getEditReportUrl() {
        return this.editReportUrl;
    }

    getDeleteReportUrl() {
        return this.deleteReportUrl;
    }

    getReportListUrl() {
        return this.reportListUrl;
    }

    getReportDetailsBasedOnIdUrl() {
        return this.reportMetaDataUrl;
    }

    getReportDetailsViewUrl() {
        return this.reportDetailsViewUrl;
    }

    getImportAllMsUrl() {
        return this.importAllMsUrl;
    }

    getBillinRagReportUrl() {
        return this.billingRagReportUrl;
    }
}
