import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { PMO, EditPMO, DeleteMasterdata } from './../master.model';
import { MasterService } from '../master.service';
import { ToasterService } from '../../../shared';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-master-pmo-dealtype',
    templateUrl: './pmo_dealtype.component.html',
    styleUrls: ['./pmo_dealtype.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class PMODealTypeComponent implements OnInit, AfterViewInit {
    @ViewChild('editpmo') editPMOpopup: ModalDirective;
    @ViewChild('confirmdialog') confirmDialog: ModalDirective;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

    dtOptions: any = {};
    PMOList: any[];
    pageIndex = 0;
    pageLength = 10;
    deleteId = '';

    pmoForm: FormGroup;
    pdtName: FormControl;
    pdtId: FormControl;
    status: FormControl;
    isFormSubmitted = false;

    isSearched;
    searchData;

    constructor(
        private http: HttpClient,
        private masterservice: MasterService,
        private ngxService: NgxUiLoaderService,
        private toasterservice: ToasterService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.gridBinding();
        this.createFormControls();
        this.createFormDetails();
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata: PMO = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    pdtId: ''
                };
                that.masterservice.getPmoDealTypeList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.PMOList = pagedData.data.pmodeal;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.PMOList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'pdt_id', orderable: false },
                { data: 'pdt_name' },
                { data: 'pdt_created_date' },
                { data: 'pdt_updated_date' },
                { data: 'moredetails', orderable: false }
            ]
        };
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    createFormControls() {
        this.pdtName = new FormControl('', [Validators.required]);
        this.pdtId = new FormControl('');
        this.status = new FormControl('');
    }

    createFormDetails() {
        this.pmoForm = new FormGroup({
            pdtName: this.pdtName,
            pdtId: this.pdtId,
            status: this.status
        });
    }

    addPMO() {
        this.pdtId.setValue('');
        this.pdtName.setValue('');
        this.status.setValue(1);
        this.editPMOpopup.show();
    }

    editPMO(data) {
        this.pdtId.setValue(data.pdt_id);
        this.pdtName.setValue(data.pdt_name);
        this.status.setValue(1);
        this.editPMOpopup.show();
    }

    addUpdatePMO() {
        if (this.pmoForm.valid) {
            this.isFormSubmitted = false;
            this.ngxService.start();
            this.masterservice.addUpdatePmoDealType(this.pmoForm.value).subscribe(
                result => {
                    if (result && result.success) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                        this.cancelPMO();
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }

    cancelPMO() {
        this.pdtId.setValue('');
        this.pdtName.setValue('');
        this.status.setValue('');
        this.pmoForm.reset();
        this.editPMOpopup.hide();
    }

    openDeletePopup(id) {
        this.status.setValue(0);
        this.deleteId = id;
        this.confirmDialog.show();
    }

    cancelDelete() {
        this.status.setValue('');
        this.deleteId = '';
        this.confirmDialog.hide();
    }

    deletePMO() {
        const data: DeleteMasterdata = {
            deleteArray: [this.deleteId],
            status: this.status.value
        };
        this.ngxService.start();
        this.masterservice.deletePmoDealType(data).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    this.cancelDelete();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
