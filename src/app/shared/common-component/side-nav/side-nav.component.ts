import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../core';
import { AuthenticationService } from '../..';

@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
    userDetails;
    userType;
    keyName = 'M';
    imgURL: any;

    constructor(private sessionService: SessionService) {}

    ngOnInit() {
        this.getLoggedInUser();
        this.getUserType();
    }

    getLoggedInUser() {
        const userDetails = this.sessionService.getSession();
        this.userDetails = userDetails.data;
        this.imgURL = this.userDetails.profileImageUrl + this.userDetails.userPhoto;
    }

    showApproveMenu() {
        const access = this.userDetails.user_access;
        if (access && (access.Level1 || access.Level2)) {
            const constructLevel1Permsn = access.Level1.split(',');
            const constructLevele2Permsn = access.Level2.split(',');
            if (this.userType === 'User Level 1') {
                for (let i = 0; i < constructLevel1Permsn.length; i++) {
                    if (constructLevel1Permsn[i] === '0') {
                        return false;
                    }
                }
                return true;
            } else if (this.userType === 'User Level 2') {
                for (let i = 0; i < constructLevele2Permsn.length; i++) {
                    if (constructLevele2Permsn[i] === '0') {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    getUserType() {
        // this.userType = this.getUserData().type;
        this.userType = this.userDetails.name;
        return this.userType;
    }
}
