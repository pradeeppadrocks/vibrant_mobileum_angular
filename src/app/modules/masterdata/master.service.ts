import { Injectable } from '@angular/core';
import { BaseService } from './../../shared/services/base.service';
import {
    ConversionType,
    EditConversionType,
    DeleteMasterdata,
    LineOfBusiness,
    EditLineOfBusiness,
    Milestone,
    EditMilestone,
    Opportunity,
    EditOpportunity,
    PMO,
    EditPMO,
    Module,
    EditModule,
    Region,
    EditRegion,
    UserType,
    EditUserType,
    User,
    EditUser,
    SFDCUsers,
    DealType,
    EditDealType,
    ProjType,
    EditProjType,
    ProjDependency,
    EditProjDependency
} from './master.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class MasterService {
    //DevURL = 'http://192.168.1.142:9051/';
    DevURL = environment.host;

    //Conversion Type URL
    getConversionTypeURL = `${this.DevURL}getConversionType`;
    addConversionTypeURL = `${this.DevURL}addConversionType`;
    updateConversionTypeURL = `${this.DevURL}updateConversionType`;
    deleteConversionTypeURL = `${this.DevURL}deleteConversionType`;

    //Line of business URL
    getLineOfBusinessURL = `${this.DevURL}getLineOfBusiness`;
    addLineOfBusinessURL = `${this.DevURL}addLineOfBusiness`;
    updateLineOfBusinessURL = `${this.DevURL}updateLineOfBusiness`;
    deleteLineOfBusinessURL = `${this.DevURL}deleteLineOfBusiness`;

    //Milestone URL
    getMilestonesTypeURL = `${this.DevURL}getMilestonesType`;
    updateMilestonesTypeURL = `${this.DevURL}updateMilestonesType`;
    addMilestonesTypeURL = `${this.DevURL}addMilestonesType`;
    deleteMilestonesTypeURL = `${this.DevURL}deleteMilestonesType`;

    //Opportunity URL
    getOpportunityTypeURL = `${this.DevURL}getOpportunityType`;
    addOpportunityTypeURL = `${this.DevURL}addOpportunityType`;
    updateOpportunityTypeURL = `${this.DevURL}updateOpportunityType`;
    deleteOpportunityTypeURL = `${this.DevURL}deleteOpportunityType`;

    //PMO URL
    getPmoDealTypeURL = `${this.DevURL}getPmoDealType`;
    addPmoDealTypeURL = `${this.DevURL}addPmoDealType`;
    updatePmoDealTypeURL = `${this.DevURL}updatePmoDealType`;
    deletePmoDealTypeURL = `${this.DevURL}deletePmoDealType`;

    //Module URL
    getModulesURL = `${this.DevURL}getModules`;
    addModuleURL = `${this.DevURL}addModule`;
    updateModuleURL = `${this.DevURL}updateModule`;
    deleteModuleURL = `${this.DevURL}deleteModule`;

    //Region URL
    getRegionsURL = `${this.DevURL}getRegions`;
    addRegionURL = `${this.DevURL}addRegion`;
    updateRegionURL = `${this.DevURL}updateRegion`;
    deleteRegionURL = `${this.DevURL}deleteRegion`;

    //User Type URL
    getUserTypesURL = `${this.DevURL}getUserTypes`;
    addUserTypeURL = `${this.DevURL}addUserType`;
    updateUserTypeURL = `${this.DevURL}updateUserType`;
    deleteUserTypeURL = `${this.DevURL}deleteUserType`;

    //User URL
    getUsersURL = `${this.DevURL}getUsers`;
    addUserURL = `${this.DevURL}addUser`;
    updateUserURL = `${this.DevURL}updateUser`;
    deleteUserURL = `${this.DevURL}deleteUser`;
    getSfdcUserURL = `${this.DevURL}getSfdcUser`;

    //Deal Type URL
    getDealTypeURL = `${this.DevURL}getDealType`;
    addDealTypeURL = `${this.DevURL}addDealType`;
    updateDealTypeURL = `${this.DevURL}updateDealType`;
    deleteDealTypeURL = `${this.DevURL}deleteDealType`;

    //Project Type URL
    getProjectTypeURL = `${this.DevURL}getProjectType`;
    addProjectTypeURL = `${this.DevURL}addProjectType`;
    updateProjectTypeURL = `${this.DevURL}updateProjectType`;
    deleteProjectTypeURL = `${this.DevURL}deleteProjectType`;

    //Project Dependency URL
    getprojDependencyURL = `${this.DevURL}getprojDependency`;
    addprojDependencyURL = `${this.DevURL}addprojDependency`;
    updateprojDependencyURL = `${this.DevURL}updateprojDependency`;
    deleteprojDependencyURL = `${this.DevURL}deleteprojDependency`;

    constructor(private baseService: BaseService) {}

    //Conversion Type API
    getConversionTypeList(data: ConversionType) {
        return this.baseService.post(this.getConversionTypeURL, data);
    }

    addUpdateConversionType(data: EditConversionType) {
        if (data.conversionTypeId) {
            return this.baseService.post(this.updateConversionTypeURL, data);
        } else {
            return this.baseService.post(this.addConversionTypeURL, data);
        }
    }

    deleteConversionType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteConversionTypeURL, data);
    }

    //Line of business API
    getLineOfBusinessList(data: LineOfBusiness) {
        return this.baseService.post(this.getLineOfBusinessURL, data);
    }

    addUpdateLineOfBusiness(data: EditLineOfBusiness) {
        if (data.lineOfBusinessId) {
            return this.baseService.post(this.updateLineOfBusinessURL, data);
        } else {
            return this.baseService.post(this.addLineOfBusinessURL, data);
        }
    }

    deleteLineOfBusiness(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteLineOfBusinessURL, data);
    }

    //Milestone API
    getMilestonesTypeList(data: Milestone) {
        return this.baseService.post(this.getMilestonesTypeURL, data);
    }

    addUpdateMilestonesType(data: EditMilestone) {
        if (data.milestoneId) {
            return this.baseService.post(this.updateMilestonesTypeURL, data);
        } else {
            return this.baseService.post(this.addMilestonesTypeURL, data);
        }
    }

    deleteMilestonesType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteMilestonesTypeURL, data);
    }

    //Opportunity API
    getOpportunityTypeList(data: Opportunity) {
        return this.baseService.post(this.getOpportunityTypeURL, data);
    }

    addUpdateOpportunityType(data: EditOpportunity) {
        if (data.opportunityId) {
            return this.baseService.post(this.updateOpportunityTypeURL, data);
        } else {
            return this.baseService.post(this.addOpportunityTypeURL, data);
        }
    }

    deleteOpportunityType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteOpportunityTypeURL, data);
    }

    //PMO API
    getPmoDealTypeList(data: PMO) {
        return this.baseService.post(this.getPmoDealTypeURL, data);
    }

    addUpdatePmoDealType(data: EditPMO) {
        if (data.pdtId) {
            return this.baseService.post(this.updatePmoDealTypeURL, data);
        } else {
            return this.baseService.post(this.addPmoDealTypeURL, data);
        }
    }

    deletePmoDealType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deletePmoDealTypeURL, data);
    }

    //Module API
    getModulesList(data: Module) {
        return this.baseService.post(this.getModulesURL, data);
    }

    addUpdateModule(data: EditModule) {
        if (data.moduleId) {
            return this.baseService.post(this.updateModuleURL, data);
        } else {
            return this.baseService.post(this.addModuleURL, data);
        }
    }

    deleteModule(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteModuleURL, data);
    }

    //Region API
    getRegionList(data: Region) {
        return this.baseService.post(this.getRegionsURL, data);
    }

    addUpdateRegion(data: EditRegion) {
        if (data.regId) {
            return this.baseService.post(this.updateRegionURL, data);
        } else {
            return this.baseService.post(this.addRegionURL, data);
        }
    }

    deleteRegion(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteRegionURL, data);
    }

    //User Type API
    getUserTypesList(data: UserType) {
        return this.baseService.post(this.getUserTypesURL, data);
    }

    addUpdateUserType(data: EditUserType) {
        if (data.userTypeId) {
            return this.baseService.post(this.updateUserTypeURL, data);
        } else {
            return this.baseService.post(this.addUserTypeURL, data);
        }
    }

    deleteUserType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteUserTypeURL, data);
    }

    //User API
    getUserList(data: User) {
        return this.baseService.post(this.getUsersURL, data);
    }
    addUpdateUser(data: EditUser) {
        if (data.userId) {
            return this.baseService.post(this.updateUserURL, data);
        } else {
            return this.baseService.post(this.addUserURL, data);
        }
    }

    deleteUser(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteUserURL, data);
    }

    getSfdcUserList(data: SFDCUsers) {
        return this.baseService.post(this.getSfdcUserURL, data);
    }

    //Deal Type API
    getDealTypeList(data: DealType) {
        return this.baseService.post(this.getDealTypeURL, data);
    }
    addUpdateDealType(data: EditDealType) {
        if (data.dealTypeId) {
            return this.baseService.post(this.updateDealTypeURL, data);
        } else {
            return this.baseService.post(this.addDealTypeURL, data);
        }
    }

    deleteDealType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteDealTypeURL, data);
    }

    //Project Type API
    getProjectTypeList(data: ProjType) {
        return this.baseService.post(this.getProjectTypeURL, data);
    }
    addUpdateProjectType(data: EditProjType) {
        if (data.projTypeId) {
            return this.baseService.post(this.updateProjectTypeURL, data);
        } else {
            return this.baseService.post(this.addProjectTypeURL, data);
        }
    }

    deleteProjectType(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteProjectTypeURL, data);
    }

    //Project Dependency API
    getProjectDependencyList(data: ProjDependency) {
        return this.baseService.post(this.getprojDependencyURL, data);
    }
    addUpdateProjectDependency(data: EditProjDependency) {
        if (data.dependencyId) {
            return this.baseService.post(this.updateprojDependencyURL, data);
        } else {
            return this.baseService.post(this.addprojDependencyURL, data);
        }
    }

    deleteProjectDependency(data: DeleteMasterdata) {
        return this.baseService.delete(this.deleteprojDependencyURL, data);
    }
}
