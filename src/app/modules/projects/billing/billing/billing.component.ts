import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, QueryList, ViewChildren, OnDestroy, Input } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import 'datatables.net';
import 'datatables.net-bs4';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BillingService } from 'src/app/shared/services/billing.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { ForecastedMilestone } from '../../../../shared/models/billing.model';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ViewBillingMilestoneComponent } from '../modals/view-billing-milestone/view-billing-milestone.component';
import { DeleteModalComponent } from '../modals/delete-modal/delete-modal.component';
import { Subject } from 'rxjs';
import { CreateMilestoneModalComponent } from '../modals/create-milestone-modal/create-milestone-modal.component';
import { ConsolidatedBmModalComponent } from '../modals/consolidated-bm-modal/consolidated-bm-modal.component';
import { SessionService } from '../../../../core';
import { ImportDeal } from '../../deal-distribution/deal-distribution.model';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-billing',
    templateUrl: './billing.component.html',
    styleUrls: ['./billing.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class BillingComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() projectDetails;

    @ViewChild('ConsolidatedBMModal') public ConsolidatedBMModal: ModalDirective;
    @ViewChild('db') example1: ElementRef;
    @ViewChildren(DataTableDirective)
    dtElements: QueryList<any>;
    dtOptions: any[] = [];
    @ViewChild('db1') table: ElementRef;

    dtTrigger = new Subject<any>();
    dtTrigger1 = new Subject<any>();

    getForcastedMilestone: any[];
    pmsMilestones: any[];
    pageIndex = 0;
    pageLength = 10;
    pageIndex2 = 0;
    pageLength2 = 10;
    projectId = '';
    userType;
    showAcceptLevel1: number;
    showSendToLevel2;
    showRejectLevel;
    private forecastedMilestoneAcceptDisabledValue = [];
    private forecastedMilestoneRejectDisabledValue = [];
    private forecastedMilestoneSendToDisabledValue = [];
    private pmsMilestoneSendToDisabledValue = [];
    private pmsMilestoneAcceptDisabledValue = [];
    private pmsMilestoneRejectDisabledValue = [];
    showFinalizeBtn;
    showCheckboxBtn = true;

    defaultSqlDate = '1900-01-01T00:00:00.000Z';

    pmsMilestonesList: any[];

    selectedPms;
    selectedPmsOptions: any = [];

    pmsSearchData;
    isPmsSearched;
    fmsSearchData;
    isFmsSearched;

    constructor(
        private http: HttpClient,
        private route: ActivatedRoute,
        private billingService: BillingService,
        private toasterservice: ToasterService,
        private modalService: NgbModal,
        config: NgbModalConfig,
        private sessionService: SessionService,
        private ngxService: NgxUiLoaderService
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
        this.getUserType();
        this.showFinalize();
    }

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.dtTrigger1.next();

        // This is to get table instance
        // Get event when page number is changed

        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                dtInstance.on('page.dt', () => {
                    const pageInfo = dtInstance.page.info();
                    dtInstance.page.len();
                    if (index === 0) {
                        this.pageIndex = pageInfo.page;
                        this.pageLength = pageInfo.length;
                    } else if (index === 1) {
                        this.pageIndex2 = pageInfo.page;
                        this.pageLength2 = pageInfo.length;
                    }
                });
            });

            dtElement.dtInstance.then((dtInstance: any) => {
                dtInstance.on('length.dt', (e, settings, len) => {
                    const pageInfo = dtInstance.page.info();
                    if (index === 0) {
                        this.pageLength = pageInfo.length;
                        this.pageIndex = pageInfo.page;
                    } else if (index === 1) {
                        this.pageLength2 = pageInfo.length;
                        this.pageIndex2 = pageInfo.page;
                    }
                });
            });
        });
    }

    ngOnInit() {
        this.projectId = this.route.snapshot.paramMap.get('id');
        //  this.getProjectDetailsById(this.projectId);
        this.gridBinding();
        this.renderPmsDataTable();
    }

    search(tableIndex) {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                if (index === 0 && tableIndex === index) {
                    this.isFmsSearched = true;
                    dtInstance.search(this.fmsSearchData).draw();
                } else if (index === 1 && tableIndex === index) {
                    this.isPmsSearched = true;
                    dtInstance.search(this.pmsSearchData).draw();
                }
            });
        });
    }

    reset(tableIndex) {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                if (index === 0 && tableIndex === index) {
                    this.isFmsSearched = false;
                    this.fmsSearchData = '';
                    dtInstance.search(this.fmsSearchData).draw();
                } else if (index === 1 && tableIndex === index) {
                    this.isPmsSearched = false;
                    this.pmsSearchData = '';
                    dtInstance.search(this.pmsSearchData).draw();
                }
            });
        });
    }

    getProjectDetailsById(projId) {
        this.billingService.getProjectDetailsByProjectId(projId).subscribe((res: any) => {
            if (res.success) {
                this.projectDetails = res.data.projects[0];
            } else {
                this.toasterservice.error('Unable to fetch details...please try again');
            }
        });
    }

    gridBinding() {
        this.dtOptions[0] = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [[1, 'asc']],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                this.initializeTableIndex(dataTablesParameters);
                const temdata: ForecastedMilestone = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    bmID: '',
                    bmProjID: this.projectId,
                    bmForcasted: 1
                };

                this.billingService.getForecastedMilestone(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.getForcastedMilestone = pagedData.data.forcastedmilestones;
                        this.pmsMilestonesList = pagedData.data.actualKeyId;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.getForcastedMilestone = [];
                        this.pmsMilestonesList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'Sl No.', orderable: false },
                { data: 'bmMilestoneName', orderable: true },
                { data: 'bmMsStartDate', orderable: true },
                { data: 'bmMsEndDate', orderable: true },
                { data: 'Ageing/Delays', orderable: false },
                { data: 'bmAmount', orderable: true },
                { data: 'action', orderable: false }
            ]
        };
    }

    initializeTableIndex(params) {
        this.pageIndex = params.start / params.length;
        this.pageLength = params.length;
    }

    initializePmsTableIndex(params) {
        this.pageIndex2 = params.start / params.length;
        this.pageLength2 = params.length;
    }

    openViewMsModal(milestoneId, forcastedId, index, elementsTable) {
        const modalRef = this.modalService.open(ViewBillingMilestoneComponent, {
            windowClass: 'dark-modal',
            size: 'xl' as any,
            centered: true
        });
        // Need to pass data to modal
        const milestoneData = {
            milestoneId,
            projectId: this.projectId,
            forcastedId
        };
        modalRef.componentInstance.milestoneData = milestoneData;
        modalRef.componentInstance.projectDetails = this.projectDetails;
        modalRef.result.then(
            result => {
                if (result.success) {
                    if (forcastedId === 0) {
                        this.pageIndex2 = 0;
                        this.pageLength2 = 1;
                    } else if (forcastedId === 1) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                    }
                    this.rerenderGrid(forcastedId);
                }
            },
            reason => {
                if (reason.success) {
                    if (forcastedId === 0) {
                        this.pageIndex2 = 0;
                        this.pageLength2 = 1;
                    } else if (forcastedId === 1) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                    }
                    this.rerenderGrid(forcastedId);
                }
            }
        );
    }

    openDeleteModal(milestoneId, fms, index, numberOfElements) {
        const modalRef1 = this.modalService.open(DeleteModalComponent, { size: 'md' as any, centered: true });
        // Need to pass data to modal
        const deleteMsData = {
            milestoneId,
            status: 0
        };
        modalRef1.componentInstance.deleteMsData = deleteMsData;
        modalRef1.result.then(
            result => {
                if (result.success) {
                    // this.refreshDataTable(fms, numberOfElements);
                    this.rerenderGrid(fms);
                } else {
                    console.log('Milestone is not deleted successfully');
                }
            },
            reason => {}
        );
    }

    openCreateBillingModal(value, msType?: any, msId?: any) {
        const modalRef1 = this.modalService.open(CreateMilestoneModalComponent, { size: 'xl' as any, centered: true });
        modalRef1.componentInstance.projectDetails = this.projectDetails;
        modalRef1.componentInstance.fiterValue = value;
        modalRef1.componentInstance.msType = msType;
        modalRef1.componentInstance.msId = msId;
        modalRef1.componentInstance.projectId = this.projectId;

        modalRef1.result.then(
            result => {
                if (result.success) {
                    this.toasterservice.showSuccess(result.message);
                    this.showCheckboxBtn = false;
                    this.selectedPmsOptions = [];
                    this.rerenderGrid(msType);
                } else {
                    this.toasterservice.error(result.message);
                }
            },
            reason => {}
        );
    }

    consolidatedBillingModal() {
        const modalRef1 = this.modalService.open(ConsolidatedBmModalComponent, { size: 'xl' as any, centered: true });
        modalRef1.componentInstance.projectDetails = this.projectDetails;
        const milestoneData = {
            milestoneId: '',
            projectId: this.projectId,
            forcastedId: 1
        };
        modalRef1.componentInstance.milestoneData = milestoneData;

        modalRef1.result.then(
            result => {},
            reason => {
                if (reason && reason.success) {
                    //  this.toasterservice.showSuccess(result.message);
                    this.rerenderGrid(undefined);
                }
            }
        );
    }

    refreshDataTable(fms, numberOfElements) {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            if (index === 0 && fms === 1) {
                dtElement.dtInstance.then((dtInstance: any) => {
                    let currentIndex = fms === 1 ? this.pageIndex : this.pageIndex2;
                    if (numberOfElements === 1 && currentIndex > 0) {
                        currentIndex = currentIndex - 1;
                    }
                    dtInstance.page(currentIndex).draw('page');
                });
            }
        });
    }

    renderPmsDataTable() {
        this.dtOptions[1] = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [[1, 'asc']],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;

                this.initializePmsTableIndex(dataTablesParameters);
                dataTablesParameters.draw = this.pageIndex2 + 1;
                dataTablesParameters.length = this.pageLength2;
                const temdata: ForecastedMilestone = {
                    pageNo: this.pageIndex2 + 1,
                    pageSize: this.pageLength2,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    bmID: '',
                    bmProjID: this.projectId,
                    bmForcasted: 0
                };
                this.billingService.getForecastedMilestone(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.pmsMilestones = pagedData.data.forcastedmilestones;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.pmsMilestones = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'Sl No.', orderable: false },
                { data: 'bmMilestoneName', orderable: true },
                { data: 'bmMsStartDate', orderable: true },
                { data: 'bmMsEndDate', orderable: true },
                { data: 'Ageing/Delays', orderable: false },
                { data: 'bmAmount', orderable: true },
                { data: 'action', orderable: false }
            ]
        };
    }

    getDelayInWeeks(startDate, actualStartDate) {
        const date1 = new Date(startDate);
        const date2 = new Date(actualStartDate);
        const timeDiff = Math.abs(date2.getTime() - date1.getTime());
        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const diffInWeeks = Math.ceil(diffDays / 7);
        return diffInWeeks;
    }

    rerenderGrid(fms): void {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            if (fms === 0 && index === 1) {
                dtElement.dtInstance.then((dtInstance: any) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger1.next();
                });
            } else if (fms === 1 && index === 0) {
                dtElement.dtInstance.then((dtInstance: any) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger.next();
                });
            } else if (index === 0 && !fms) {
                dtElement.dtInstance.then((dtInstance: any) => {
                    // Destroy the table first
                    dtInstance.destroy();
                    // Call the dtTrigger to rerender again
                    this.dtTrigger.next();
                });
            }
        });
    }

    reRenderDataTableAfterCreationMilestone() {
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
                // Call the dtTrigger to rerender again
                if (index === 0) {
                    // Destroy the table first
                    dtInstance.destroy();
                    this.dtTrigger.next();
                } else if (index === 1) {
                    // Destroy the table first
                    dtInstance.destroy();
                    this.dtTrigger1.next();
                }
            });
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        this.dtTrigger1.unsubscribe();
    }

    finalize(milestoneType) {
        // call api to finalize
        this.ngxService.start();
        this.billingService.finalizeMilestoneBasedOnProject(this.projectId, milestoneType).subscribe((res: any) => {
            if (res.success) {
                this.toasterservice.showSuccess('Milestone Finalized successfully');
            } else {
                this.toasterservice.error(res.message);
            }
            this.ngxService.stop();
        });
    }

    getUserType() {
        const userDetails = this.sessionService.getSession();
        this.userType = userDetails.data.username;
        return this.userType;
    }

    checkUserType() {
        if (this.userType.indexOf('1') > -1) {
            return 1;
        } else if (this.userType.indexOf('2') > -1) {
            return 2;
        } else {
            return null;
        }
    }

    accepetLevel(msId, userTypeLevel, index, msType) {
        this.ngxService.start();
        this.billingService.acceptMilestone(msId, userTypeLevel).subscribe((res: any) => {
            if (res.success) {
                this.toasterservice.showSuccess(res.message);
                if (msType === 1) {
                    this.forecastedMilestoneAcceptDisabledValue.push(index);
                } else if (msType === 0) {
                    this.pmsMilestoneAcceptDisabledValue.push(index);
                }
                this.ngxService.stop();
            } else {
                this.toasterservice.error(res.message);
                this.ngxService.stop();
            }
        });
    }

    sendToLevel2(msId, userTypeLevel, index, msType) {
        this.ngxService.start();
        this.billingService.sendToLevel2(msId, userTypeLevel).subscribe((res: any) => {
            if (res.success) {
                this.showSendToLevel2 = true;
                this.toasterservice.showSuccess(res.message);
                if (msType === 1) {
                    this.forecastedMilestoneSendToDisabledValue.push(index);
                } else if (msType === 0) {
                    this.pmsMilestoneSendToDisabledValue.push(index);
                }
                this.ngxService.stop();
            } else {
                this.toasterservice.error(res.message);
                this.ngxService.stop();
            }
        });
    }

    rejectMilestone(msId, userTypeLevel, index, msType) {
        this.ngxService.start();
        this.billingService.rejectMilestoneLevel(msId, userTypeLevel).subscribe((res: any) => {
            if (res.success) {
                this.showRejectLevel = true;
                this.toasterservice.showSuccess(res.message);

                if (msType === 1) {
                    this.forecastedMilestoneRejectDisabledValue.push(index);
                } else if (msType === 0) {
                    this.pmsMilestoneRejectDisabledValue.push(index);
                }
                this.ngxService.stop();
            } else {
                this.toasterservice.error(res.message);
                this.ngxService.stop();
            }
        });
    }

    shouldDisableForecastedMilestoneAcceptButton(index): boolean {
        return this.forecastedMilestoneAcceptDisabledValue.includes(index);
    }

    shouldDisableForecastedMilestoneRejectButton(index): boolean {
        return this.forecastedMilestoneRejectDisabledValue.includes(index);
    }

    shouldDisableForecastedMilestoneSendToButton(index): boolean {
        return this.forecastedMilestoneSendToDisabledValue.includes(index);
    }

    shouldDisablePmsMilestoneAcceptButton(index): boolean {
        return this.pmsMilestoneAcceptDisabledValue.includes(index);
    }

    shouldDisablePmsMilestoneRejectButton(index): boolean {
        return this.pmsMilestoneRejectDisabledValue.includes(index);
    }

    shouldDisablePmsMilestoneSendToButton(index): boolean {
        return this.pmsMilestoneSendToDisabledValue.includes(index);
    }

    importFile($event, msType, id): void {
        this.readThis($event.target, msType, id);
    }

    readThis(inputValue: any, msType, id): void {
        const file: File = inputValue.files[0];
        if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            this.toasterservice.error('Only xlsx files are supported.');
            return;
        }
        const myReader: FileReader = new FileReader();

        myReader.onloadend = e => {
            const strresult = myReader.result.toString();
            const base64 = strresult.split('base64,');
            const prodata = {
                forcasted: msType,
                fileValue: base64[1],
                projId: this.projectId,
                entity: this.projectDetails.entityCompanyCode
            };
            this.ngxService.start();
            this.billingService.importBmMilestone(prodata).subscribe(
                result => {
                    if (result.success && result.data) {
                        this.rerenderGrid(msType);
                        this.toasterservice.showSuccess(result.message);
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    // Clear the input file for uploading same file again
                    document.getElementById(id)['value'] = '';
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
            // this.image = myReader.result;
        };
        myReader.readAsDataURL(file);
    }

    shouldDisplayApprovalButton(data: any): boolean {
        if (data.bmApprovalStatus === 0 && data.bmFinalized === 1) {
            if (this.checkUserType() === 1 && !data.bmLevel1Approver) {
                return true;
            } else if (this.checkUserType() === 2 && !data.bmLevel2Approver) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    showFinalize() {
        const userDetails = this.sessionService.getSession();
        if (userDetails && userDetails.data && userDetails.data.user_access) {
            if (userDetails.data.user_access.Finalize) {
                const finalize = userDetails.data.user_access.Finalize;
                const splitFinalize = finalize.split(',');
                for (let i = 0; i < splitFinalize.length; i++) {
                    if (splitFinalize[i] === '0') {
                        this.showFinalizeBtn = false;
                        return;
                    }
                }
                this.showFinalizeBtn = true;
            }
        }
    }

    selectedPmsOption(event, index) {
        const value = event;
        const obj = { index, value };
        this.pushToArray(this.selectedPmsOptions, obj);
    }

    pushToArray(arr, obj) {
        const i = arr.findIndex(e => e.index === obj.index);
        if (i === -1) {
            arr.push(obj);
        } else {
            arr[i] = obj;
        }
    }

    showCheckbox(index) {
        const obj = this.selectedPmsOptions.find(o => o.index === index);
        if (obj && obj.value !== 'Map Milestone') {
            this.showCheckboxBtn = true;
            return true;
        }
        this.showCheckboxBtn = false;
        return false;
    }

    // getSelectedPMS(index) {
    //   return this.selectedPmsOptions.find(o => o.index === index);
    // }

    mappPms(event, fmsId, index) {
        if (event.target.checked && this.selectedPmsOptions.length > 0) {
            const obj = this.selectedPmsOptions.find(o => o.index === index);
            this.billingService.mappPms(fmsId, obj.value).subscribe((res: any) => {
                if (res.success) {
                    this.toasterservice.showSuccess(res.message);
                    this.reRenderDataTableAfterCreationMilestone();
                } else {
                    this.toasterservice.error(res.message);
                }
            });
        } else {
            //  this.toasterservice.error('Please select any milestone to map');
        }
    }
}
