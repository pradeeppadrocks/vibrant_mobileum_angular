import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
    selector: 'app-pie-chart',
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit, OnChanges {
    Highcharts = Highcharts;
    pieChartOptions = {};
    // Name of the PIE
    @Input() name: string;
    // Colours to paint the PIE chart. If no colour given, PIE will be painted with it's default settings.
    @Input() colors: string[];
    // Data to draw
    @Input() data: any;
    // Drop-down options
    @Input() dropDownOptions: string[];
    // Option to select on load of the PIE
    @Input() selectedOption: string;
    // Emit a event to the parent component to do some operation
    @Output() changed = new EventEmitter<string>();
    updatePie: boolean;

    constructor() {}

    ngOnInit() {
        if (!this.selectedOption) {
            this.selectedOption = this.dropDownOptions && this.dropDownOptions.length > 0 ? this.dropDownOptions[0] : '';
        }
    }

    onSelectionChange(newValue: string) {
        this.changed.emit(newValue);
    }

    loadPieChartOptions() {
        this.pieChartOptions = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                padding: 0
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                symbolRadius: 0
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    colors: this.colors,
                    dataLabels: {
                        enabled: true,
                        format: '{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        },
                        color: '#ffffff',
                        radialGradient: null
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
            series: this.data
        };
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.loadPieChartOptions();
        this.updatePie = true;
    }
}
