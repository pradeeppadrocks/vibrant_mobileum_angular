import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {PmoRoutingModule} from './pmo-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PmoRoutingModule]
})
export class PmoModule {
}
