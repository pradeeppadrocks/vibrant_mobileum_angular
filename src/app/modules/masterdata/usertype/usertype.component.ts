import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserType, EditUserType, DeleteMasterdata } from './../master.model';
import { MasterService } from '../master.service';
import { ToasterService } from '../../../shared';
import { DataTableDirective } from 'angular-datatables';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SessionService } from '../../../core';

@Component({
    selector: 'app-master-usertype',
    templateUrl: './usertype.component.html',
    styleUrls: ['./usertype.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class UserTypeComponent implements OnInit, AfterViewInit {
    @ViewChild('editmoduleaccess') editModuleAccesspopup: ModalDirective;
    @ViewChild('editusertype') editUserTypepopup: ModalDirective;
    @ViewChild('confirmdialog') confirmDialog: ModalDirective;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

    dtOptions: any = {};
    userTypeList: any[];
    pageIndex = 0;
    pageLength = 10;
    deleteId = '';

    userTypeForm: FormGroup;
    userTypeId: FormControl;
    userTypeName: FormControl;
    userDefaultAccess: FormControl;
    status: FormControl;
    isFormSubmitted = false;
    ModuleAccessData = [];
    accessKey = [];
    accessValue = [];
    editModuleAccess = {};

    isSearched;
    searchData;

    constructor(
        private http: HttpClient,
        private masterservice: MasterService,
        private ngxService: NgxUiLoaderService,
        private toasterservice: ToasterService,
        private sessionService: SessionService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.gridBinding();
        this.createFormControls();
        this.createFormDetails();
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata: UserType = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    userTypeId: ''
                };
                that.masterservice.getUserTypesList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.userTypeList = pagedData.data.userTypes;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.userTypeList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'urol_id', orderable: false },
                { data: 'urol_name' },
                { data: 'urol_default_access' },
                { data: 'moredetails', orderable: false }
            ]
        };
    }

    createFormControls() {
        this.userTypeName = new FormControl('', [Validators.required]);
        this.userTypeId = new FormControl('');
        this.status = new FormControl('');
        this.userDefaultAccess = new FormControl({});
    }

    createFormDetails() {
        this.userTypeForm = new FormGroup({
            userTypeName: this.userTypeName,
            userTypeId: this.userTypeId,
            status: this.status,
            userDefaultAccess: this.userDefaultAccess
        });
    }

    addUserType() {
        this.userTypeId.setValue('');
        this.userTypeName.setValue('');
        this.userDefaultAccess.setValue({});
        this.status.setValue(1);
        this.editUserTypepopup.show();
    }

    editUserType(data) {
        this.userTypeId.setValue(data.urol_id);
        this.userTypeName.setValue(data.urol_name);
        this.userDefaultAccess.setValue(data.urol_default_access);
        this.status.setValue(1);
        this.editUserTypepopup.show();
    }

    addUpdateUserType() {
        if (this.userTypeForm.valid) {
            this.isFormSubmitted = false;
            this.ngxService.start();
            this.masterservice.addUpdateUserType(this.userTypeForm.value).subscribe(
                result => {
                    if (result && result.success) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                        this.cancelUserType();
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }

    cancelUserType() {
        this.userTypeForm.reset();
        this.editUserTypepopup.hide();
        this.userTypeName.setValue('');
        this.userTypeId.setValue('');
        this.status.setValue('');
        this.userDefaultAccess.setValue({});
    }

    editUserAccess(data) {
        if (data && data.urol_default_access) {
            this.userTypeId.setValue(data.urol_id);
            this.userTypeName.setValue(data.urol_name);
            this.userDefaultAccess.setValue(data.urol_default_access);
            this.status.setValue(1);

            this.ModuleAccessData = [];
            this.accessKey = Object.keys(data.urol_default_access);
            let valuelist = Object.values(data.urol_default_access);
            let valstring = '';
            for (var i in valuelist) {
                valstring += parseInt(i) != valuelist.length - 1 ? valuelist[i] + ',' : valuelist[i];
            }
            let stringsplit = valstring.split(',');

            let temobj = { view: 0, add: 0, edit: 0, delete: 0 };
            this.accessValue = [];
            for (var i in stringsplit) {
                if (parseInt(i) % 4 == 0) {
                    temobj = { view: 0, add: 0, edit: 0, delete: 0 };
                    temobj.add = parseInt(stringsplit[i]);
                } else if (parseInt(i) % 4 == 1) {
                    temobj.view = parseInt(stringsplit[i]);
                } else if (parseInt(i) % 4 == 2) {
                    temobj.edit = parseInt(stringsplit[i]);
                } else if (parseInt(i) % 4 == 3) {
                    temobj.delete = parseInt(stringsplit[i]);
                    this.accessValue.push(temobj);
                }
            }
            for (var i in this.accessKey) {
                var pushdata = { moduleName: this.accessKey[i], list: this.accessValue[i] };
                this.ModuleAccessData.push(pushdata);
            }
            //for sorting
            this.ModuleAccessData = this.ModuleAccessData.sort(function(a, b) {
                if (a.moduleName < b.moduleName) {
                    return -1;
                }
                if (a.moduleName < b.moduleName) {
                    return 1;
                }
                // names must be equal
                return 0;
            });
        } else {
            this.ModuleAccessData = [];
        }
        this.editModuleAccesspopup.show();
    }

    updateModuleAccess() {
        var objdata = {};
        this.ModuleAccessData.forEach(data => {
            data.list.view = data.list.view == true ? 1 : data.list.view == false ? 0 : data.list.view;
            data.list.add = data.list.add == true ? 1 : data.list.add == false ? 0 : data.list.add;
            data.list.edit = data.list.edit == true ? 1 : data.list.edit == false ? 0 : data.list.edit;
            data.list.delete = data.list.delete == true ? 1 : data.list.delete == false ? 0 : data.list.delete;
            objdata[data.moduleName] = data.list.add + ',' + data.list.view + ',' + data.list.edit + ',' + data.list.delete;
        });
        this.userDefaultAccess.setValue(objdata);
        this.ngxService.start();
        this.masterservice.addUpdateUserType(this.userTypeForm.value).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    if (this.sessionService.getSession().data.userId == this.userTypeForm.value.userId) {
                        this.toasterservice.warning('Plese login again for smooth use');
                    }
                    this.cancelModuleAccess();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    cancelModuleAccess() {
        this.editModuleAccesspopup.hide();
        this.userTypeName.setValue('');
        this.userTypeId.setValue('');
        this.status.setValue('');
        this.userDefaultAccess.setValue({});
    }

    openDeletePopup(id) {
        this.deleteId = id;
        this.status.setValue(0);
        this.confirmDialog.show();
    }

    cancelDelete() {
        this.deleteId = '';
        this.status.setValue('');
        this.confirmDialog.hide();
    }

    deleteUserType() {
        const data: DeleteMasterdata = {
            deleteArray: [this.deleteId],
            status: 0
        };
        this.masterservice.deleteUserType(data).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    this.cancelDelete();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
