export class ForecastedMilestone {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    bmProjID: any;
    bmForcasted: any;
    bmID: any;
}
