import { TestBed } from '@angular/core/testing';

import { DependencyReportService } from './dependency-report.service';

describe('DependencyReportService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: DependencyReportService = TestBed.get(DependencyReportService);
        expect(service).toBeTruthy();
    });
});
