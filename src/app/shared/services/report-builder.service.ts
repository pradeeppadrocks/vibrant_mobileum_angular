import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';
import { RegionChangeService } from './region-change.service';

@Injectable({
    providedIn: 'root'
})
export class ReportBuilderService {
    updatedRegion: any;

    constructor(
        private baseService: BaseService,
        private endpointService: EndpointService,
        private regionUpdateService: RegionChangeService
    ) {
        this.regionUpdateService.region.subscribe(value => {
            this.updatedRegion = value;
        });
    }

    getUpdatedRegion() {
        return this.updatedRegion;
    }

    getReportMenu() {
        const body = {};
        return this.baseService.post(this.endpointService.getReportFieldsUrl(), body);
    }

    getReportList(body) {
        return this.baseService.post(this.endpointService.getReportListUrl(), body);
    }

    addReport(data, duration, reportName) {
        const body = {
            fields: data.ParentChildchecklist,
            duration,
            reportName
        };
        return this.baseService.post(this.endpointService.addReportUrl, body);
    }

    editReport(data, duration, reportName, reportId) {
        const body = {
            fields: data.ParentChildchecklist,
            duration,
            reportName,
            reportId
        };
        return this.baseService.post(this.endpointService.getEditReportUrl(), body);
    }

    deleteReport(reportId) {
        const body = {
            deleteArray: [reportId],
            status: 0
        };

        return this.baseService.delete(this.endpointService.getDeleteReportUrl(), body);
    }

    getReportDetailsBasedOnId(body) {
        return this.baseService.post(this.endpointService.getReportDetailsBasedOnIdUrl(), body);
    }

    getReportDetailsOnView(body) {
        return this.baseService.post(this.endpointService.getReportDetailsViewUrl(), body);
    }
}
