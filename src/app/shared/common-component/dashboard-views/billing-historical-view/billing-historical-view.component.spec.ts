import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingHistoricalViewComponent } from './billing-historical-view.component';

describe('BillingHistoricalViewComponent', () => {
    let component: BillingHistoricalViewComponent;
    let fixture: ComponentFixture<BillingHistoricalViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BillingHistoricalViewComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BillingHistoricalViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
