import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEntitycompanyComponent } from './edit-entitycompany.component';

describe('EditEntitycompanyComponent', () => {
    let component: EditEntitycompanyComponent;
    let fixture: ComponentFixture<EditEntitycompanyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EditEntitycompanyComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditEntitycompanyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
