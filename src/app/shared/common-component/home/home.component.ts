import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    constructor() {}

    ngOnInit() {
        // Toggle the side navigation
        $('#sidebarToggle').on('click', function(e) {
            e.preventDefault();
            $('body').toggleClass('sidebar-toggled');
            $('.sidebar').toggleClass('toggled');
            // $('.sidebar').slideToggle('slow');
            // $('.sidebar').animate({
            //     width: 'toggle'
            // }, "slow");
            sidebarHover();
            if ($('body').hasClass('sidebar-toggled')) {
                $('.nav-item .nav-link')
                    .next('.sub-menu')
                    .css('display', 'none');
            } else {
                $('.open-submenu')
                    .next('.sub-menu')
                    .css('display', 'block');
            }
        });

        /*Navbar Submenu item toggle*/
        // this is for dashboard and submenu
        $('.top-nav-link').click(function() {
            if (!$('.sidebar').hasClass('toggled')) {
                $('.nav-item .nav-link').removeClass('open-submenu');
                $('.nav-item .nav-link')
                    .next('.sub-menu')
                    .slideToggle('.sub-menu')
                    .css('display', 'none');
                $('#nav-link').removeClass('open-submenu');
                $('#nav-link')
                    .next('#sub-menu')
                    .slideUp('#sub-menu')
                    .css('display', 'none');

                $(this)
                    .next('.sub-menu')
                    .css('display', 'block');
                $(this).addClass('open-submenu');
            }
        });
        // master and submenu
        $('#masterData').click(function() {
            $('#masterData').removeClass('open-submenu');
            $('#masterData')
                .next('#masterMenu')
                .slideToggle('#masterMenu')
                .css('display', 'block');
            $('.nav-link').removeClass('open-submenu');
            $('.nav-link')
                .next('.sub-menu')
                .css('display', 'none');

            $(this)
                .next('#masterMenu')
                .css('display', 'block');
            $(this).addClass('open-submenu');
        });
        // reports and submenu

        $(document).ready(function() {
            $('#reports').click(function() {
                $('#reportmenu').slideToggle();
                $('#dashboardMenu').slideUp('.sub-menu');
                $('#masterMenu').slideUp();
            });
        });

        var currentPage = window.location.pathname;
        // dashboards submenu paths
        var dashboard = '/dashboard';
        var billingHistoricalView = '/dashboard-billing-historical-view';
        var revenueHistoricalView = '/dashboard-revenue-historical-view';

        // masters submenu paths
        var region = '/master/region';
        var conversiontype = '/master/conversiontype';
        var billingmilestone = '/master/billingmilestone';
        var lineofbusiness = '/master/lineofbusiness';
        var pmodealtype = '/master/pmodealtype';
        var opportunitydealtype = '/master/opportunitydealtype';
        var modulemaster = '/master/modulemaster';
        var usertype = '/master/usertype';
        var dealtype = '/master/dealtype';
        var projecttype = '/master/projecttype';
        var projectdependency = '/master/projectdependency';
        var entitycode = '/master/entitycompanycode';

        //reports submenu paths
        var billing = '/reports/billing';
        var revenue = '/reports/revenue';
        var builder = '/reports/builder';
        var swdep = '/reports/sw-dep';
        var hwdep = '/reports/hw-dep';
        var custdep = '/reports/cust-dep';
        var paperworkdep = '/reports/paperwork';
        var ragBillingRed = '/reports/rag-billing-red';
        var ragBillingGreen = '/reports/rag-billing-green';
        var ragBillingAmber = '/reports/rag-billing-amber';

        if (currentPage == dashboard || currentPage == billingHistoricalView || currentPage == revenueHistoricalView) {
            $('.nav-link').removeClass('open-submenu');
            $('.nav-link')
                .next('.sub-menu')
                .css('display', 'inherit');
            $('masterData').removeClass('open-submenu');
            $('#masterData')
                .next('#masterMenu')
                .css('display', 'none');
            $('#reports').removeClass('open-submenu');
            $('#reports')
                .next('#reportmenu')
                .css('display', 'none');
        } else if (
            currentPage == region ||
            currentPage == conversiontype ||
            currentPage == billingmilestone ||
            currentPage == lineofbusiness ||
            currentPage == pmodealtype ||
            currentPage == opportunitydealtype ||
            currentPage == modulemaster ||
            currentPage == usertype ||
            currentPage == dealtype ||
            currentPage == projecttype ||
            currentPage == projectdependency ||
            currentPage == entitycode
        ) {
            $(function() {
                $('.nav-link').removeClass('open-submenu');
                $('.nav-link')
                    .next('.sub-menu')
                    .css('display', 'none');
                $('#masterData').removeClass('open-submenu');
                $('#masterData')
                    .next('#masterMenu')
                    .css('display', 'inherit');
            });
        } else {
            $('#masterData').removeClass('open-submenu');
            $('#masterData')
                .next('#masterMenu')
                .css('display', 'none');
        }

        if (
            billing == currentPage ||
            revenue == currentPage ||
            builder == currentPage ||
            swdep == currentPage ||
            hwdep == currentPage ||
            custdep == currentPage ||
            paperworkdep == currentPage ||
            ragBillingRed == currentPage ||
            ragBillingGreen == currentPage ||
            ragBillingAmber == currentPage
        ) {
            $('#reports').removeClass('open-submenu');
            $('#reports')
                .next('#reportmenu')
                .css('display', 'inherit');
        }

        function sidebarHover() {
            $('.sidebar-toggled .nav-item').hover(
                function() {
                    if ($('body').hasClass('sidebar-toggled')) {
                        $(this)
                            .find('.sub-menu')
                            .css('display', 'block');
                    }
                },
                function() {
                    if ($('body').hasClass('sidebar-toggled')) {
                        $(this)
                            .find('.sub-menu')
                            .css('display', 'none');
                    }
                }
            );
        }
        sidebarHover();
        /*// Prevent the content wrapper from scrolling when the fixed side navigation hovered over
$('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
if ($(window).width() > 768) {
var e0 = e.originalEvent,
  delta = e0.wheelDelta || -e0.detail;
this.scrollTop += (delta < 0 ? 1 : -1) * 30;
e.preventDefault();
}
});*/

        // Scroll to top button appear
        $(document).on('scroll', function() {
            const scrollDistance = $(this).scrollTop();
            if (scrollDistance > 100) {
                $('.scroll-to-top').fadeIn();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });

        // Smooth scrolling using jQuery easing
        $(document).on('click', 'a.scroll-to-top', function(event) {
            const $anchor = $(this);
            $('html, body')
                .stop()
                .animate(
                    {
                        scrollTop: $($anchor.attr('href')).offset().top
                    },
                    1000,
                    'easeInOutExpo'
                );
            event.preventDefault();
        });
    }
}
