import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenueHistoricalViewComponent } from './revenue-historical-view.component';

describe('RevenueHistoricalViewComponent', () => {
    let component: RevenueHistoricalViewComponent;
    let fixture: ComponentFixture<RevenueHistoricalViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RevenueHistoricalViewComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RevenueHistoricalViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
