import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RagBillingGreenComponent } from './rag-billing-green.component';

describe('RagBillingGreenComponent', () => {
    let component: RagBillingGreenComponent;
    let fixture: ComponentFixture<RagBillingGreenComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RagBillingGreenComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RagBillingGreenComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
