import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RagRevenueGreenComponent } from './rag-revenue-green.component';

describe('RagRevenueGreenComponent', () => {
    let component: RagRevenueGreenComponent;
    let fixture: ComponentFixture<RagRevenueGreenComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RagRevenueGreenComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RagRevenueGreenComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
