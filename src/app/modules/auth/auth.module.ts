import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UnauthenticatedComponent } from './unauthenticated/unauthenticated.component';
import { SharedModule } from '../../shared/shared-module';

@NgModule({
    declarations: [LoginComponent, ForgotPasswordComponent, UnauthenticatedComponent, ResetPasswordComponent],
    imports: [CommonModule, AuthRoutingModule, ReactiveFormsModule, FormsModule, SharedModule],
    entryComponents: [ResetPasswordComponent],
    exports: [ResetPasswordComponent, AuthRoutingModule]
})
export class AuthModule {}
