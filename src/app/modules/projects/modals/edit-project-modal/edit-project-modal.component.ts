import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProjectService } from '../../project.service';
import { EntityService, ToasterService } from '../../../../shared';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConversionType, ProjectDetails } from '../../project.model';
import { CONSTANTS } from '../../../../shared/constant';

@Component({
    selector: 'app-edit-project-modal',
    templateUrl: './edit-project-modal.component.html',
    styleUrls: ['./edit-project-modal.component.css']
})
export class EditProjectModalComponent implements OnInit {
    @Input() projectDetails;
    projectId;

    editProjectForm: FormGroup;
    projId: FormControl;
    dealTypeId: FormControl;
    conversionTypeId: FormControl;
    dealWarranty: FormControl;
    dealWarrantyStartDate: FormControl;
    uatDate: FormControl;
    faDate: FormControl;
    entityCompanyCode: FormControl;
    revenueProfile: FormControl;
    revenueProfileCash: FormControl;
    isFormSubmitted = false;

    contentTypeList = [];
    revenueProfileList = [{ name: 'VSOE', value: 1 }, { name: 'POC', value: 2 }, { name: 'TIMEBOUND', value: 3 }];
    entityList: any[];

    constructor(
        private projectService: ProjectService,
        private toasterservice: ToasterService,
        public activeModal: NgbActiveModal,
        private entityService: EntityService
    ) {}

    ngOnInit() {
        this.projectId = this.projectDetails.projId;
        this.getConversionType();
        this.createFormControls();
        this.getEntityList();
        this.createForm();
        this.editProjectModelOpen(this.projectDetails);
    }

    editProjectModelOpen(data) {
        this.projId.setValue(data.projId);
        this.dealTypeId.setValue(data.dealTypeId);
        this.conversionTypeId.setValue(data.conversionTypeId ? data.conversionTypeId : '');
        this.dealWarranty.setValue(data.dealWarranty);
        this.dealWarrantyStartDate.setValue(data.dealWarrantyStartDate);
        this.uatDate.setValue(data.uatDate);
        this.faDate.setValue(data.faDate);
        this.entityCompanyCode.setValue(data.entityCodeId ? Number(data.entityCodeId) : null);
        this.revenueProfile.setValue(data.revenueProfile ? parseInt(data.revenueProfile, 10) : null);
        this.revenueProfileCash.setValue(data.revenueProfileCash.toString());
    }

    createFormControls() {
        this.projId = new FormControl('');
        this.dealTypeId = new FormControl('');
        this.conversionTypeId = new FormControl('');
        this.dealWarranty = new FormControl('');
        this.dealWarrantyStartDate = new FormControl('');
        this.uatDate = new FormControl('');
        this.faDate = new FormControl('');
        this.entityCompanyCode = new FormControl(null);
        this.revenueProfile = new FormControl('');
        this.revenueProfileCash = new FormControl(0);
    }

    createForm() {
        this.editProjectForm = new FormGroup({
            projId: this.projId,
            dealTypeId: this.dealTypeId,
            conversionTypeId: this.conversionTypeId,
            dealWarranty: this.dealWarranty,
            dealWarrantyStartDate: this.dealWarrantyStartDate,
            uatDate: this.uatDate,
            faDate: this.faDate,
            entityCompanyCode: this.entityCompanyCode,
            revenueProfile: this.revenueProfile,
            revenueProfileCash: this.revenueProfileCash
        });
    }

    UpdateProject() {
        if (this.editProjectForm.value.projId && this.editProjectForm.valid) {
            this.isFormSubmitted = false;
            this.editProjectForm.value.dealWarranty = this.editProjectForm.value.dealWarranty ? 1 : 0;
            this.editProjectForm.value.revenueProfileCash = parseInt(this.editProjectForm.value.revenueProfileCash, 10);
            this.projectService.updateProjectDetail(this.editProjectForm.value).subscribe(
                result => {
                    if (result && result.success) {
                        this.toasterservice.showSuccess(result.message);
                        this.activeModal.close(result);
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    // this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }

    getConversionType() {
        const conversiondata: ConversionType = {
            pageNo: 1,
            pageSize: 1000,
            sort: '',
            order: '',
            searchKey: '',
            conversionTypeId: ''
        };
        this.projectService.getConversionType(conversiondata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.contentTypeList = result.data.conversions;
                    // result.data.company.forEach(element => {
                    //  this.contentTypeList = [...this.companyList, element];
                    // });
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
    }

    getEntityList() {
        this.entityService.getProjectEntity().subscribe((res: any) => {
            if (res.success) {
                this.entityList = res.data.entity;
            } else {
                this.toasterservice.error(res.message);
            }
        });
    }
}
