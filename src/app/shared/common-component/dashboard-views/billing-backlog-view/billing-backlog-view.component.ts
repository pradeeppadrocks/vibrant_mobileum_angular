import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../../services/dashboard.service';
import { Router } from '@angular/router';
import { ChartExportService, RegionChangeService } from '../../..';

@Component({
    selector: 'app-billing-backlog-view',
    templateUrl: './billing-backlog-view.component.html',
    styleUrls: ['./billing-backlog-view.component.css']
})
export class BillingBacklogViewComponent implements OnInit {
    Highcharts: typeof Highcharts = Highcharts; // Highcharts, it's Highcharts

    summaryChartOptions = {};
    updateBillingBacklogChart = false;
    backlogSummaryData = [];
    billinBacklogSummaryoneToOneFlag = false;
    showBillingBacklogSummaryChart;

    conversionChartOptions = {};
    conversiononeToOneFlag = false;
    conversionCategoryList = [];
    conversionList = [];
    updateConversionChart = false;

    dependenciesChartOptions = {};
    dependenciesoneToOneFlag = false;
    dependenciesCategoryList = [];
    dependenciesList = [];
    updateDependenciesChart = false;

    showSummaryChartLoader = true;
    showConversionLoader = true;
    showDependencyLoader = true;

    constructor(
        private router: Router,
        private dashboardservice: DashboardService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private regionUpdateService: RegionChangeService,
        public chartExportService: ChartExportService
    ) {}

    ngOnInit() {
        this.loadChartOptions();
        this.pageLoadBinding();

        this.regionUpdateService.region.subscribe(value => {
            this.refreshPage();
        });
    }

    refreshPage() {
        this.pageLoadBinding();
    }
    pageLoadBinding() {
        // this.ngxService.start();

        this.dashboardservice.getBillingBacklogSummaryPieUrl().subscribe(
            result => {
                if (result.success && result.data) {
                    const piechartarray = [];
                    result.data.forEach(ele => {
                        let temparray = [];
                        temparray.push(ele.name);
                        temparray.push(ele.value);
                        piechartarray.push(temparray);
                    });
                    this.showBillingBacklogSummaryChart = this.showBillingPiechartData(piechartarray);
                    this.summaryChartOptions['series'][0].data = piechartarray;
                    this.updateBillingBacklogChart = true;
                    this.billinBacklogSummaryoneToOneFlag = true;
                    this.showSummaryChartLoader = false;
                    //  this.ngxService.stop();
                }
                //  this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //  this.ngxService.stop();
            }
        );

        this.dashboardservice.getBillingBacklogConverstion().subscribe(
            result => {
                if (result.success && result.data) {
                    result.data.forEach(ele => {
                        this.conversionCategoryList = [];
                        for (const i in ele.value) {
                            this.conversionCategoryList.push(ele.value[i].name);
                        }
                    });
                    this.conversionList = this.bindingGridData(result.data, this.conversionCategoryList);
                    this.conversionChartOptions['xAxis'].categories = this.conversionCategoryList;
                    this.conversionChartOptions['series'] = this.bindingChartData(result.data);
                    this.updateConversionChart = true;
                    this.conversiononeToOneFlag = true;
                    this.showConversionLoader = false;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //   this.ngxService.stop();
            }
        );

        this.dashboardservice.getBillingBacklogDependencies().subscribe(
            result => {
                if (result.success && result.data) {
                    result.data.forEach(ele => {
                        this.dependenciesCategoryList = [];
                        for (const i in ele.value) {
                            this.dependenciesCategoryList.push(ele.value[i].name);
                        }
                    });
                    this.dependenciesList = this.bindingGridData(result.data, this.dependenciesCategoryList);
                    this.dependenciesChartOptions['xAxis'].categories = this.dependenciesCategoryList;
                    this.dependenciesChartOptions['series'] = this.bindingChartData(result.data);
                    this.updateDependenciesChart = true;
                    this.dependenciesoneToOneFlag = true;
                    this.showDependencyLoader = false;
                }
                //  this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
    }

    bindingChartData(data) {
        const list = [];
        data.forEach(ele => {
            const tempdata = { name: ele.name, data: [] };
            for (const i in ele.value) {
                tempdata.data.push(ele.value[i].value);
            }
            list.push(tempdata);
        });
        return list;
    }

    loadChartOptions() {
        this.summaryChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },

            chart: {
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Backlog Summary'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                formatter() {
                    let seriesSpan = '';

                    if (this.point.y > 1000000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000000, 2) +
                            'M' +
                            '<br />';
                    } else if (this.point.y > 1000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000, 2) +
                            'K' +
                            '<br />';
                    } else {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y, 2) +
                            '<br />';
                    }
                    return seriesSpan + '<table></table>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',

                    dataLabels: {
                        enabled: false
                    },

                    showInLegend: true
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    return `${this.name} (${this.percentage.toFixed(2)} %)`;
                }
            },
            series: [
                {
                    type: 'pie',
                    name: '',
                    data: []
                }
            ]
        };

        this.conversionChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Conversion'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: this.conversionCategoryList,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount ($)'
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            series: []
        };
        this.dependenciesChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Dependencies'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: this.dependenciesCategoryList,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount ($)'
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            series: []
        };
    }

    bindingGridData(data, categories) {
        const list = [];
        const totalarr = [];
        data.forEach(ele => {
            const tempdata = { name: ele.name, data: [] };
            let total = 0;
            for (const i in ele.value) {
                tempdata.data.push(ele.value[i].value);
                total += ele.value[i].value;
            }
            tempdata.data.push(total);
            list.push(tempdata);
        });

        for (const i in categories) {
            let temptotal = 0;
            data.forEach(ele => {
                for (const j in ele.value) {
                    if (categories[i] == ele.value[j].name) {
                        temptotal += ele.value[j].value;
                    }
                }
            });
            totalarr.push(temptotal);
        }
        list.push({ name: '', data: totalarr });
        return list;
    }

    showBillingPiechartData(data) {
        let flag = false;
        data.some(ele => {
            if (ele[1] !== 0) {
                flag = true;
                return flag;
            }
        });
        return flag;
    }
    redirectBillingForecast() {
        this.router.navigate(['dashboard-billing-forecast-view']);
    }
}
