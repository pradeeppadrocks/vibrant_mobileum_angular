import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class TimeliveService {
    constructor(private endpointService: EndpointService, private baseService: BaseService) {}

    getTimeliveData(body) {
        return this.baseService.post(this.endpointService.getTimeliveUrl(), body);
    }

    getProjectDetailsByProjectId(projId) {
        const body = {
            pageNo: 1,
            pageSize: 10,
            sort: '',
            order: '1',
            searchKey: '',
            projDealYear: '',
            projDealQtr: '',
            projId: projId,
            companyId: '',
            regId: ''
        };

        return this.baseService.post(this.endpointService.getProjectsUrl(), body);
    }

    saveAdditionalData(body) {
        return this.baseService.post(this.endpointService.getAdditionalAmountDataUrl(), body);
    }

    saveEfforts(body) {
        return this.baseService.post(this.endpointService.getTimeliveEffortUrl(), body);
    }

    importTimelive(body) {
        return this.baseService.post(this.endpointService.getImportTimeliveUrl(), body);
    }
}
