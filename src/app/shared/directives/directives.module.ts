import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HasRoleDirective } from './has-role.directive';
import { PasswordMatchingDirective } from './password-matching.directive';

@NgModule({
    declarations: [HasRoleDirective, PasswordMatchingDirective],
    imports: [CommonModule],
    exports: [HasRoleDirective, PasswordMatchingDirective]
})
export class DirectivesModule {}
