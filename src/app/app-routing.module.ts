import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './shared/common-component/dashboard/dashboard.component';
import { HomeComponent } from './shared/common-component/home/home.component';
import { AuthorizationGuard } from './core/guards/auth/authorization.guard';
import { AuthGuard } from './core/guards/auth/auth.guard';
import { DataTableComponent } from './shared/common-component/data-table/data-table.component';
import { BillingHistoricalViewComponent } from './shared/common-component/dashboard-views/billing-historical-view/billing-historical-view.component';
import { RevenueHistoricalViewComponent } from './shared/common-component/dashboard-views/revenue-historical-view/revenue-historical-view.component';
import { BillingForecastViewComponent } from './shared/common-component/dashboard-views/billing-forecast-view/billing-forecast-view.component';
import { ApprovalComponent } from './shared/common-component/approval/approval.component';
import { BillingBacklogViewComponent } from './shared/common-component/dashboard-views/billing-backlog-view/billing-backlog-view.component';
import { RevenueForecastViewComponent } from './shared/common-component/dashboard-views/revenue-forecast-view/revenue-forecast-view.component';
import { RevenueBacklogViewComponent } from './shared/common-component/dashboard-views/revenue-backlog-view/revenue-backlog-view.component';
import { UserProfileComponent } from './shared/common-component/user-profile/user-profile.component';
import { ResetPasswordGuard } from './core/guards/auth/reset-password.guard';

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: './modules/auth/auth.module#AuthModule'
    },
    { path: 'data-table', component: DataTableComponent },
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'settings',
                component: UserProfileComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'approval',
                component: ApprovalComponent,
                canActivate: [AuthGuard, AuthorizationGuard]
            },
            {
                path: 'dashboard-billing-historical-view',
                component: BillingHistoricalViewComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'dashboard-revenue-historical-view',
                component: RevenueHistoricalViewComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'dashboard-billing-forecast-view',
                component: BillingForecastViewComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'dashboard-billing-backlog-view',
                component: BillingBacklogViewComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'dashboard-revenue-forecast-view',
                component: RevenueForecastViewComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'dashboard-revenue-backlog-view',
                component: RevenueBacklogViewComponent,
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'pmo',
                loadChildren: './modules/pmo/pmo.module#PmoModule',
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'stdm',
                loadChildren: './modules/stdm/stdm.module#StdmModule',
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'projects',
                loadChildren: './modules/projects/projects.module#ProjectsModule',
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
                /*data: {
expectedRole: 'USERS'
}*/
            },
            {
                path: 'master',
                loadChildren: './modules/masterdata/masterdata.module#MasterDataModule',
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            },
            {
                path: 'reports',
                loadChildren: './modules/reports/projects-reports.module#ProjectsReportsModule',
                canActivate: [AuthGuard, AuthorizationGuard, ResetPasswordGuard]
            }
        ]
    },
    { path: '**', redirectTo: '/dashboard' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
