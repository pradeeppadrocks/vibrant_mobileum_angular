import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { ForecastedMilestone } from '../../../../shared/models/billing.model';
import { SwDepReportService } from '../../../../shared';

@Component({
    selector: 'app-sw-dep',
    templateUrl: './sw-dep.component.html',
    styleUrls: ['./sw-dep.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class SwDepComponent implements OnInit, AfterViewInit {
    @Input() projectDetails;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;

    dtOptions: any = {};
    pageIndex = 0;
    pageLength = 10;
    swDepData: any[];
    currentYear = new Date().getFullYear();
    totalBillingLastYear = 'totalBilling' + (this.currentYear - 1);
    billingBacklogLastYear = 'billingBacklog' + (this.currentYear - 1);
    billingCurrentYear = 'billingBacklog' + this.currentYear;
    billingNextYear = 'totalBilling' + (this.currentYear + 1);
    biliingAfter2yr = 'totalBilling' + (this.currentYear + 2);
    biliingAfter3yr = 'totalBilling' + (this.currentYear + 3);
    currentQtr = 'Q' + this.getCurrentQtr();
    totalBillingCrntQtr = 'totalBilling' + this.currentQtr;

    constructor(private http: HttpClient, private swDepService: SwDepReportService) {}

    ngAfterViewInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                this.pageLength = len;
            });
        });
    }

    ngOnInit() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            bFilter: false,
            searching: false,
            paging: false,
            info: false,
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    projId: this.projectDetails.projId
                };
                this.swDepService.getSwDepData(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.swDepData = pagedData.data.swDependencies;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.swDepData = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [{ data: 'ca_dep_owner', orderable: false }]
        };
    }

    getCurrentQtr() {
        const today = new Date();
        return Math.floor((today.getMonth() + 3) / 3);
    }

    getUniqOwner(ownerList) {
        const uniqueOwnerList = ownerList
            .split(',')
            .filter((items, i, a) => {
                return i === a.indexOf(items);
            })
            .join(', ');
        return uniqueOwnerList;
    }
}
