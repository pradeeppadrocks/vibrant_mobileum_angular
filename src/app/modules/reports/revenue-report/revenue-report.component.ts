import { AfterViewInit, Component, Input, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { EndpointService, RegionChangeService, ReportsService } from '../../../shared';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { Subject } from 'rxjs';
declare var $: any;

@Component({
    selector: 'app-revenue-report',
    templateUrl: './revenue-report.component.html',
    styleUrls: ['./revenue-report.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class RevenueReportComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() projectDetails;
    @ViewChild(DataTableDirective) datatableElement;
    dtTrigger = new Subject<any>();
    dtOptions: any = {};

    persons: any[] = [];
    billingBacklog: any[];
    pageIndex = 0;
    pageLength = 10;

    revenueBacklog: any[];

    currentYear = new Date().getFullYear();
    totalBillingLastYear = 'totalBilling' + (this.currentYear - 1);
    billingBacklogLastYear = 'billingBacklog' + (this.currentYear - 1);
    billingCurrentYear = 'billingBacklog' + this.currentYear;
    billingNextYear = 'totalBilling' + (this.currentYear + 1);
    biliingAfter2yr = 'totalBilling' + (this.currentYear + 2);
    currentQtr = 'Q' + this.getCurrentQtr();
    totalBillingCrntQtr = 'totalBilling' + this.currentQtr;
    totalBacklogLastYr = 'totalBacklog' + (this.currentYear - 1);
    totalBacklogCrntYr = 'totalBacklog' + this.currentYear;
    mntBacklogLastYr = 'mntBacklog' + (this.currentYear - 1);
    mntBacklogCrntYr = 'mntBacklog' + this.currentYear;
    selectedYear = new Date().getFullYear();
    yearList = [];

    isSearched;
    searchData;
    regions;
    subscriptions;

    constructor(
        private reportService: ReportsService,
        private baseService: HttpClient,
        private fileSaverService: FileSaverService,
        private endpointService: EndpointService,
        private regionUpdateService: RegionChangeService
    ) {
        this.regions = this.reportService.getUpdatedRegion();
        this.subscriptions = this.regionUpdateService.region.subscribe(value => {
            this.regions = value;
            this.reRenderRevenueBacklogTable();
        });

        this.constructYearList();
    }

    ngAfterViewInit() {
        this.dtTrigger.next();

        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit() {
        this.renderRevenueBacklog(this.selectedYear);
    }

    getCurrentQtr() {
        const today = new Date();
        return Math.floor((today.getMonth() + 3) / 3);
    }

    constructYearList() {
        const lastYear = 2000;
        const currentYear = new Date().getFullYear();
        for (let i = lastYear; i <= currentYear; i++) {
            const obj = { id: i, value: i };
            this.yearList.push(obj);
        }
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    renderRevenueBacklog(year) {
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    projId: '',
                    year: this.selectedYear,
                    regions: this.regions ? this.regions : ''
                };

                this.reportService.getRevenueBacklogWaterfallReportData(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.revenueBacklog =
                            pagedData.data.revenueWaterFall.projectData.length > 0 ? pagedData.data.revenueWaterFall.projectData : [];
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.revenueBacklog = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [{ data: 'oppId', orderable: false }]
        };
    }

    selectedYearOnChange(year) {
        this.reRenderRevenueBacklogTable();
    }

    reRenderRevenueBacklogTable() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        if (!!this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    export() {
        const body = {};
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        this.baseService
            .post(this.endpointService.getRevenueWaterfallExportUrl(), body, { responseType: 'arraybuffer', headers })
            .subscribe((res: any) => {
                this.fileSaverService.save(
                    new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
                    this.selectedYear + ' revenue.xlsx'
                );
            });
    }

    /* hide(e) {
      e.preventDefault();

      // Get the column API object
      this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
        const column = dtInstance.column( $(this).attr('data-column') );

        // Toggle the visibility
        column.visible( ! column.visible() );
      });

    }*/
}
