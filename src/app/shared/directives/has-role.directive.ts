import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '..';

@Directive({
    selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit, OnDestroy {
    @Input() appHasRole: string | string[];
    private permission$: Subscription;

    constructor(
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private authenticationService: AuthenticationService
    ) {}

    ngOnInit(): void {
        this.applyPermission();
    }

    private applyPermission(): void {
        this.permission$ = this.authenticationService.checkAuthorization(this.appHasRole).subscribe(authorized => {
            if (authorized) {
                this.viewContainer.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainer.clear();
            }
        });
    }

    ngOnDestroy(): void {
        this.permission$.unsubscribe();
    }
}
