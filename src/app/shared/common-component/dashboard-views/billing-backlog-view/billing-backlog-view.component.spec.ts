import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingBacklogViewComponent } from './billing-backlog-view.component';

describe('BillingBacklogViewComponent', () => {
    let component: BillingBacklogViewComponent;
    let fixture: ComponentFixture<BillingBacklogViewComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BillingBacklogViewComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BillingBacklogViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
