import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEntityCompanycodeComponent } from './create-entity-companycode.component';

describe('CreateEntityCompanycodeComponent', () => {
    let component: CreateEntityCompanycodeComponent;
    let fixture: ComponentFixture<CreateEntityCompanycodeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CreateEntityCompanycodeComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateEntityCompanycodeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
