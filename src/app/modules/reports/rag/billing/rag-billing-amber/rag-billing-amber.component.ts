import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';
import { EndpointService, RagReportService, RegionChangeService } from '../../../../../shared';

@Component({
    selector: 'app-rag-billing-amber',
    templateUrl: './rag-billing-amber.component.html',
    styleUrls: ['./rag-billing-amber.component.css', '../../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class RagBillingAmberComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() projectDetails;
    @ViewChild(DataTableDirective) datatableElement;
    dtTrigger = new Subject<any>();
    dtOptions: any = {};

    ragBillinRed: any[] = [];
    pageIndex = 0;
    pageLength = 10;

    revenueBacklog: any[];

    currentYear = new Date().getFullYear();
    selectedYear = new Date().getFullYear();
    yearList = [];

    regions;
    subscriptions;

    constructor(
        private baseService: HttpClient,
        private fileSaverService: FileSaverService,
        private endpointService: EndpointService,
        private regionUpdateService: RegionChangeService,
        private ragReportService: RagReportService
    ) {
        this.regions = this.ragReportService.getUpdatedRegion();
        this.subscriptions = this.regionUpdateService.region.subscribe(value => {
            this.regions = value;
            this.reRenderRevenueBacklogTable();
        });

        this.constructYearList();
    }

    ngAfterViewInit() {
        this.dtTrigger.next();

        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit() {
        this.renderRevenueBacklog(this.selectedYear);
    }

    constructYearList() {
        const lastYear = 2000;
        const currentYear = new Date().getFullYear();
        for (let i = currentYear; i >= lastYear; i--) {
            const obj = { id: i, value: i };
            this.yearList.push(obj);
        }
    }

    /*tableSearch() {
    this.datatableElement.dtInstance.then((dtInstance: any) => {
      this.isSearched = true;
      dtInstance.search(this.searchData).draw();
    });
  }

  tableReset() {
    this.datatableElement.dtInstance.then((dtInstance: any) => {
      this.isSearched = false;
      this.searchData = '';
      dtInstance.search(this.searchData).draw();
    });
  }*/

    renderRevenueBacklog(year?: any) {
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    ragTag: 'amber',
                    year: this.selectedYear,
                    regions: this.regions ? this.regions : ''
                };

                this.ragReportService.getBillingRagReport(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.ragBillinRed = pagedData.data.billingWaterFall.length > 0 ? pagedData.data.billingWaterFall : [];
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.ragBillinRed = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [{ data: 'Opportunity_Name', orderable: false }, { data: 'Project_Code', orderable: false }]
        };
    }

    selectedYearOnChange(year) {
        this.reRenderRevenueBacklogTable();
    }

    reRenderRevenueBacklogTable() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            dtInstance.destroy();
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        if (!!this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    export() {
        const body = {
            pageSize: 10,
            pageNo: 1,
            order: -1,
            sort: '',
            ragTag: 'amber',
            xlsExport: true,
            regions: this.regions ? this.regions : ''
        };
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        this.baseService
            .post(this.endpointService.getBillinRagReportUrl(), body, { responseType: 'arraybuffer', headers })
            .subscribe((res: any) => {
                this.fileSaverService.save(
                    new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
                    this.selectedYear + ' Rag Billing Amber.xlsx'
                );
            });
    }
}
