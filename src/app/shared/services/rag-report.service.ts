import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';
import { RegionChangeService } from './region-change.service';

@Injectable({
    providedIn: 'root'
})
export class RagReportService {
    updatedRegion: any;
    constructor(
        private endpointService: EndpointService,
        private baseService: BaseService,
        private regionUpdateService: RegionChangeService
    ) {
        this.regionUpdateService.region.subscribe(value => {
            this.updatedRegion = value;
        });
    }

    getUpdatedRegion() {
        return this.updatedRegion;
    }

    getBillingRagReport(body) {
        return this.baseService.post(this.endpointService.getBillinRagReportUrl(), body);
    }
}
