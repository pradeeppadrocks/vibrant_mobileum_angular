import { AfterViewInit, Component, ElementRef, QueryList, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { MasterService } from '../../master.service';
import { Milestone } from '../../master.model';
import { CreateEntityCompanycodeComponent } from '../modals/create-entity-companycode/create-entity-companycode.component';
import { DeleteEntityCodeComponent } from '../modals/delete-entity-code/delete-entity-code.component';
import { EditEntitycompanyComponent } from '../modals/edit-entitycompany/edit-entitycompany.component';
import { EntityService } from '../../../../shared';

@Component({
    selector: 'app-entity-company-code',
    templateUrl: './entity-company-code.component.html',
    styleUrls: ['./entity-company-code.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class EntityCompanyCodeComponent implements OnInit, AfterViewInit {
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtElements: QueryList<any>;
    dtTrigger = new Subject<any>();

    dtOptions: any = {};
    entityList: any[];
    pageIndex = 0;
    pageLength = 10;
    deleteId = '';

    isSearched;
    searchData;

    constructor(
        private http: HttpClient,
        private entityService: EntityService,
        private ngxService: NgxUiLoaderService,
        private toasterservice: ToasterService,
        private modalService: NgbModal
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.gridBinding();
    }

    gridBinding() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [[2, 'desc']],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    entityId: ''
                };
                this.entityService.getEntityList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        this.entityList = pagedData.data.entitycode;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.entityList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'Sl No.', orderable: false },
                { data: 'entityCode', orderable: true },
                { data: 'entityCreatedDate', orderable: true },
                { data: 'entityUpdatedDate', orderable: true }
            ]
        };
    }

    createEntityCompanyModal() {
        const createEntityModal = this.modalService.open(CreateEntityCompanycodeComponent, {
            windowClass: 'dark-modal',
            size: 'lg' as any,
            centered: true
        });

        createEntityModal.result.then(
            result => {
                if (result.success) {
                    this.rerenderGrid();
                }
            },
            reason => {}
        );
    }

    deleteEntityCompanyModal(entity) {
        const deleteModal = this.modalService.open(DeleteEntityCodeComponent, {
            windowClass: 'dark-modal',
            size: 'md' as any,
            centered: true
        });

        deleteModal.componentInstance.entity = entity;

        deleteModal.result.then(
            result => {
                if (result.success) {
                    this.rerenderGrid();
                }
            },
            reason => {}
        );
    }

    editEntityCompanyModal(entity) {
        const editModalRef = this.modalService.open(EditEntitycompanyComponent, {
            windowClass: 'dark-modal',
            size: 'lg' as any,
            centered: true
        });
        editModalRef.componentInstance.entity = entity;

        editModalRef.result.then(
            result => {
                if (result.success) {
                    this.rerenderGrid();
                }
            },
            reason => {}
        );
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
