import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
declare const $: any;
const IndicatorExport = require('highcharts/modules/exporting');
IndicatorExport(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';

Drilldown(Highcharts);
import { DashboardService } from '../../services/dashboard.service';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ChartExportService, RegionChangeService } from '../..';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    @ViewChild('revenueChartTable') revenueChartTable: ElementRef;

    crntQtrRagData;

    Highcharts = Highcharts;
    billingChartOptions = {};
    billingChartList = [];
    revenueChartOptions = {};
    revenueChartList = [];
    topBilllingProject = [];
    topRevenueProject = [];

    selectProjectName: any = '';
    selectOppId: any = '';
    selectGPProjectCode: any = '';
    selectOppName: any = '';
    selectBRID: any = '';
    topBillingMenu = [{ name: '5', value: 5 }, { name: '10', value: 10 }, { name: '15', value: 15 }];
    topRevenueMenu = [{ name: '5', value: 5 }, { name: '10', value: 10 }, { name: '15', value: 15 }];
    updateBillingChart = false;
    updateRevenueChart: boolean = false;
    billingoneToOneFlag: boolean = false;
    revenueoneToOneFlag: boolean = false;
    monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    projectCodeList = [];
    oppIdList = [];
    oppNameList = [];

    initialProjectCodeList = [];
    initialOppIdList = [];
    initialOppNameList = [];

    billingTopFilterOption = '';
    revenueTopFilterOption = '';

    defaultFilterOption = 5;
    showBillingChart;
    showRevenueChart;

    constructor(
        private dashboardservice: DashboardService,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private regionUpdateService: RegionChangeService,
        public chartExportService: ChartExportService
    ) {}

    ngOnInit() {
        this.getCrntQtrRagData();
        this.getDashboardChartFiterData();
        this.pageLoadChartBinding();
        this.pageLoadBinding();

        this.regionUpdateService.region.subscribe(value => {
            this.refreshPage();
        });
    }

    refreshPage() {
        this.getCrntQtrRagData();
        this.getDashboardChartFiterData();
        this.pageLoadBinding();
        this.getDashboardChartFiterData();
        this.selectOppId = '';
        this.selectGPProjectCode = '';
        this.selectOppName = '';
    }

    getDashboardChartFiterData() {
        this.dashboardservice.getDashboardChartFiterData().subscribe((res: any) => {
            if (res.success) {
                this.initialProjectCodeList = res.data.projectcode;
                this.initialOppIdList = res.data.oppId;
                this.initialOppNameList = res.data.oppName;
                this.initializeDropDownFilterData(res);
            } else {
                this.toasterservice.error(res.message);
            }
        });
    }

    initializeDropDownFilterData(res) {
        this.projectCodeList = res.data.projectcode;
        this.oppIdList = res.data.oppId;
        this.oppNameList = res.data.oppName;
    }

    onOppIdChange(oppId) {
        this.selectOppName = '';
        this.selectGPProjectCode = '';
        this.refreshProjectCodeList(oppId);
        this.refreshOppNameList(oppId);
    }

    onOppNameChange(oppId) {
        this.selectGPProjectCode = '';
        this.refreshProjectCodeList(oppId);
    }

    refreshProjectCodeList(oppId) {
        this.projectCodeList = this.initialProjectCodeList.filter(item => {
            return item.oppId === oppId;
        });
    }

    refreshOppNameList(oppId) {
        this.oppNameList = this.initialOppNameList.filter(item => {
            return item.oppId === oppId;
        });
    }

    pageLoadBinding() {
        // this.ngxService.start();

        const filterData = {
            projectCode: this.selectGPProjectCode,
            oppId: this.selectOppId,
            oppName: this.selectOppName,
            pageSize: ''
        };

        this.dashboardservice.getDashboardBillingReportGraph(filterData).subscribe(
            result => {
                if (result.success && result.data) {
                    this.topBilllingProject = result.data.topBillingCustomers;

                    this.billingChartList = this.bindingGridData(result.data);
                    this.billingChartOptions['series'] = this.bindingChartData(result.data, '#B5EFCE', '#F6D3AF');
                    this.billingChartOptions['drilldown'].series = this.bindingDrildownChartData(result.data);
                    this.updateBillingChart = true;
                    this.billingoneToOneFlag = true;
                    this.showBillingChart = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
        this.dashboardservice.getDashboardRevenueReportGraph(filterData).subscribe(
            result => {
                if (result.success && result.data) {
                    this.topRevenueProject = result.data.topRevenueProjects;

                    this.revenueChartList = this.bindingGridData(result.data);
                    this.revenueChartOptions['series'] = this.bindingChartData(result.data, '#B5EFCE', '#F6D3AF');
                    this.revenueChartOptions['drilldown'].series = this.bindingDrildownChartData(result.data);
                    this.updateRevenueChart = true;
                    this.revenueoneToOneFlag = true;
                    this.showRevenueChart = true;
                }
                //  this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    searchCurrentQTRRAGView(e?: any) {
        if (!this.selectOppId) {
            this.resetCurrentQTRRAGView();
        } else if (this.selectOppId || this.selectGPProjectCode || this.selectOppName) {
            this.showBillingChart = false;
            this.showRevenueChart = false;
            this.pageLoadBinding();
        } else {
            alert('Please select Atleast one dropdown!');
        }
    }

    getCrntQtrRagData() {
        this.dashboardservice.getCurrentQtrRagViewData().subscribe((res: any) => {
            if (res.success) {
                this.crntQtrRagData = res.data;
            }
        });
    }

    resetCurrentQTRRAGView() {
        this.selectProjectName = '';
        this.selectOppId = '';
        this.selectGPProjectCode = '';
        this.selectOppName = '';
        this.selectBRID = '';
        this.projectCodeList = this.initialProjectCodeList;
        this.oppNameList = this.initialOppNameList;
        this.oppIdList = this.oppIdList;
        this.showBillingChart = false;
        this.showRevenueChart = false;
        this.pageLoadBinding();
    }

    bindingChartData(data, color1, color2) {
        var plandata = {
            name: 'Plan',
            data: [
                { name: 'Q1', y: 0, drilldown: 'Q1 Plan' },
                { name: 'Q2', y: 0, drilldown: 'Q2 Plan' },
                { name: 'Q3', y: 0, drilldown: 'Q3 Plan' },
                { name: 'Q4', y: 0, drilldown: 'Q4 Plan' },
                { name: 'Total', y: 0, drilldown: 'Total Plan' }
            ],
            color1
        };
        var actualdata = {
            name: 'Actual',
            data: [
                { name: 'Q1', y: 0, drilldown: 'Q1 Actual' },
                { name: 'Q2', y: 0, drilldown: 'Q2 Actual' },
                { name: 'Q3', y: 0, drilldown: 'Q3 Actual' },
                { name: 'Q4', y: 0, drilldown: 'Q4 Actual' },
                { name: 'Total', y: 0, drilldown: 'Total Actual' }
            ],
            color2
        };
        var list = [];
        for (var i = 0; i < 12; i++) {
            if (i == 0 || i == 1 || i == 2) {
                plandata.data[0].y += data.planData[i].value;
                actualdata.data[0].y += data.actualData[i].value;
            } else if (i == 3 || i == 4 || i == 5) {
                plandata.data[1].y += data.planData[i].value;
                actualdata.data[1].y += data.actualData[i].value;
            } else if (i == 6 || i == 7 || i == 8) {
                plandata.data[2].y += data.planData[i].value;
                actualdata.data[2].y += data.actualData[i].value;
            } else if (i == 9 || i == 10 || i == 11) {
                plandata.data[3].y += data.planData[i].value;
                actualdata.data[3].y += data.actualData[i].value;
            }
            plandata.data[4].y += data.planData[i].value;
            actualdata.data[4].y += data.actualData[i].value;
        }
        list.push(plandata);
        list.push(actualdata);
        return list;
    }

    bindingDrildownChartData(data) {
        var plandata = [
            { name: 'Q1 Plan', id: 'Q1 Plan', data: [] },
            { name: 'Q2 Plan', id: 'Q2 Plan', data: [] },
            { name: 'Q3 Plan', id: 'Q3 Plan', data: [] },
            { name: 'Q4 Plan', id: 'Q4 Plan', data: [] },
            { name: 'Total Plan', id: 'Total Plan', data: [] }
        ];
        var actualdata = [
            { name: 'Q1 Actual', id: 'Q1 Actual', data: [] },
            { name: 'Q2 Actual', id: 'Q2 Actual', data: [] },
            { name: 'Q3 Actual', id: 'Q3 Actual', data: [] },
            { name: 'Q4 Actual', id: 'Q4 Actual', data: [] },
            { name: 'Total Actual', id: 'Total Actual', data: [] }
        ];

        var list = [];
        for (var i = 0; i < 12; i++) {
            let planarr = [];
            let actualarr = [];
            if (i == 0 || i == 1 || i == 2) {
                plandata[0].data.push(this.makeDrilDownData(this.monthList[i], data.planData[i].value));
                actualdata[0].data.push(this.makeDrilDownData(this.monthList[i], data.actualData[i].value));
            } else if (i == 3 || i == 4 || i == 5) {
                plandata[1].data.push(this.makeDrilDownData(this.monthList[i], data.planData[i].value));
                actualdata[1].data.push(this.makeDrilDownData(this.monthList[i], data.actualData[i].value));
            } else if (i == 6 || i == 7 || i == 8) {
                plandata[2].data.push(this.makeDrilDownData(this.monthList[i], data.planData[i].value));
                actualdata[2].data.push(this.makeDrilDownData(this.monthList[i], data.actualData[i].value));
            } else if (i == 9 || i == 10 || i == 11) {
                plandata[3].data.push(this.makeDrilDownData(this.monthList[i], data.planData[i].value));
                actualdata[3].data.push(this.makeDrilDownData(this.monthList[i], data.actualData[i].value));
            }
            plandata[4].data.push(this.makeDrilDownData(this.monthList[i], data.planData[i].value));
            actualdata[4].data.push(this.makeDrilDownData(this.monthList[i], data.actualData[i].value));
        }
        for (var la in plandata) {
            list.push(plandata[la]);
            list.push(actualdata[la]);
        }
        return list;
    }

    makeDrilDownData(month, value) {
        let arr = [];
        arr.push(month);
        arr.push(value);
        return arr;
    }

    bindingGridData(data) {
        var plandata = { name: 'Plan', data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] };
        var actualdata = { name: 'Actual', data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] };
        var totaldata = { name: '', data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] };
        var list = [];
        for (var i = 0; i < 12; i++) {
            plandata.data[i] = data.planData[i].value;
            actualdata.data[i] = data.actualData[i].value;
            plandata.data[12] += data.planData[i].value;
            actualdata.data[12] += data.actualData[i].value;
            totaldata.data[i] = data.planData[i].value + data.actualData[i].value;
        }
        // for last total
        totaldata.data[i] = plandata.data[i] + actualdata.data[i];

        list.push(plandata);
        list.push(actualdata);
        list.push(totaldata);
        return list;
    }

    BillingChartChange(charttype) {
        this.billingChartOptions['chart'].type = charttype;
        this.updateBillingChart = true;
    }

    RevenueChartChange(charttype) {
        this.revenueChartOptions['chart'].type = charttype;
        this.updateRevenueChart = true;
    }

    pageLoadChartBinding() {
        this.billingChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Billing'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount ($)'
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                /*pointFormat:
                    '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>$ {point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',*/
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        // format: '${point.y:.1f}',
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            }
        };
        this.revenueChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'column'
                // zoomType: 'xy'
            },
            title: {
                text: 'Revenue'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Amount ($)'
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                useHTML: true,
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        let pointValue = point.y;
                        if (pointValue < 0) {
                            pointValue = -pointValue;
                        }
                        if (pointValue > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                (point.y < 0 ? '-' : '') +
                                Highcharts.numberFormat(pointValue / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (pointValue > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                (point.y < 0 ? '-' : '') +
                                Highcharts.numberFormat(pointValue / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                (point.y < 0 ? '-' : '') +
                                Highcharts.numberFormat(pointValue, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        // format: '${point.y:.1f}',
                        formatter() {
                            let pointValue = this.point.y;
                            if (pointValue < 0) {
                                pointValue = -pointValue;
                            }
                            if (pointValue > 1000000) {
                                return '$' + (this.point.y < 0 ? '-' : '') + Highcharts.numberFormat(pointValue / 1000000, 2) + 'M';
                            } else if (pointValue > 1000) {
                                return '$' + (this.point.y < 0 ? '-' : '') + Highcharts.numberFormat(pointValue / 1000, 2) + 'K';
                            } else {
                                return '$' + (this.point.y < 0 ? '-' : '') + Highcharts.numberFormat(pointValue, 2);
                            }
                        }
                    }
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            }
        };
    }

    revenueFilterChange(value) {
        this.ngxService.start();

        const filterData = {
            projectCode: this.selectGPProjectCode,
            oppId: this.selectOppId,
            oppName: this.selectOppName,
            pageSize: Number(this.revenueTopFilterOption)
        };

        this.dashboardservice.getDashboardRevenueReportGraph(filterData).subscribe(
            result => {
                this.ngxService.stop();
                if (result.success && result.data) {
                    this.topRevenueProject = result.data.topRevenueProjects;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    billingFilterChange(value?: any) {
        this.ngxService.start();

        const filterData = {
            projectCode: this.selectGPProjectCode,
            oppId: this.selectOppId,
            oppName: this.selectOppName,
            pageSize: Number(this.billingTopFilterOption)
        };

        this.dashboardservice.getDashboardBillingReportGraph(filterData).subscribe(
            result => {
                this.ngxService.stop();
                if (result.success && result.data) {
                    this.topBilllingProject = result.data.topBillingCustomers;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
    }
}
