import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityRoutingModule } from './entity-routing.module';
import { EntityCompanyCodeComponent } from './entity-company-code/entity-company-code.component';
import { CreateEntityCompanycodeComponent } from './modals/create-entity-companycode/create-entity-companycode.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared-module';
import { DeleteEntityCodeComponent } from './modals/delete-entity-code/delete-entity-code.component';
import { EditEntitycompanyComponent } from './modals/edit-entitycompany/edit-entitycompany.component';

@NgModule({
    declarations: [EntityCompanyCodeComponent, CreateEntityCompanycodeComponent, DeleteEntityCodeComponent, EditEntitycompanyComponent],
    imports: [CommonModule, EntityRoutingModule, FormsModule, ReactiveFormsModule, SharedModule],
    entryComponents: [CreateEntityCompanycodeComponent, DeleteEntityCodeComponent, EditEntitycompanyComponent]
})
export class EntityCompanyCodeModule {}
