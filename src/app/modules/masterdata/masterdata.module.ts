import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared-module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MasterDataRoutingModule } from './masterdata-routing.module';
import { BillingMilestoneComponent } from './billingmilestones/billingmilestones.component';
import { ConversionTypeComponent } from './conversiontype/conversiontype.component';
import { LineOfBusinessComponent } from './lineofbusiness/lineofbusiness.component';
import { ModuleComponent } from './module/module.component';
import { ModuleMasterComponent } from './modulemaster/modulemaster.component';
import { OpportunityDealTypeComponent } from './opportunity_dealtype/opportunity_dealtype.component';
import { PMODealTypeComponent } from './pmo_dealtype/pmo_dealtype.component';
import { RegionComponent } from './region/region.component';
import { UserListComponent } from './userlist/userlist.component';
import { UserTypeComponent } from './usertype/usertype.component';
import { DealTypeComponent } from './dealtype/dealtype.component';
import { ProjectTypeComponent } from './projecttype/projecttype.component';
import { ProjectDependencyComponent } from './projectdependency/projectdependency.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { MasterService } from './master.service';
import { EntityCompanyCodeModule } from './entity_company/entity-company-code.module';

@NgModule({
    declarations: [
        BillingMilestoneComponent,
        ConversionTypeComponent,
        LineOfBusinessComponent,
        ModuleComponent,
        ModuleMasterComponent,
        OpportunityDealTypeComponent,
        PMODealTypeComponent,
        RegionComponent,
        UserListComponent,
        UserTypeComponent,
        DealTypeComponent,
        ProjectTypeComponent,
        ProjectDependencyComponent
    ],
    imports: [
        NgSelectModule,
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        ModalModule.forRoot(),
        MasterDataRoutingModule,
        SharedModule,
        EntityCompanyCodeModule
    ],
    entryComponents: [],
    providers: [MasterService]
})
export class MasterDataModule {}
