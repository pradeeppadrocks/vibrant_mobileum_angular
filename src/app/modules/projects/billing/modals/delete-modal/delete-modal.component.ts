import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../../../../shared/services/billing.service';
import { ToasterService } from '../../../../../shared';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CreateMilestoneModalComponent } from '../create-milestone-modal/create-milestone-modal.component';
import { SessionService } from '../../../../../core';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';

@Component({
    selector: 'app-delete-modal',
    templateUrl: './delete-modal.component.html',
    styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
    @Input() deleteMsData;
    msData;

    constructor(
        public activeModal: NgbActiveModal,
        private billingService: BillingService,
        private customToasterservice: ToasterService,
        private ngxService: NgxUiLoaderService,
        private modalService: NgbModal,
        config: NgbModalConfig
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {}

    deleteSelectedMsData() {
        const modalRef = this.modalService.open(RemarksModalComponent, { size: 'md' as any, centered: true });

        modalRef.result.then(
            result => {
                if (result) {
                    // later need to send remarks to api
                    this.ngxService.start();
                    this.deleteMsData['bmRemarks'] = result;
                    this.billingService.deleteMsData(this.deleteMsData).subscribe((res: any) => {
                        if (res.success) {
                            this.customToasterservice.showSuccess('Milestone deleted successfully');
                            this.activeModal.close(res);
                            this.ngxService.stop();
                        } else {
                            this.customToasterservice.error(res.message);
                            this.ngxService.stop();
                        }
                    });
                }
            },
            reason => {}
        );
    }
}
