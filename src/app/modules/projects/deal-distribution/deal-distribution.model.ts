export class ForcastedDealDistribution {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    ddForcasted: any;
    ddId: string;
    ddProjId: string;
}

export class LatestForcastedDealDistribution {
    ddForcasted: any;
    ddProjectId: any;
}

export class DeleteMilestone {
    deleteArray: any;
    status: any;
}

export class EditDealDistribution {
    ddId: string;
    ddFeeType: string;
    ddFeeName: string;
    ddFeeDescription: string;
    ddQuantity: any;
    ddProjectId: any;
    ddSWItem: string;
    ddPercentage: any;
    ddProjType: any;
    ddAmount: any;
    ddStartDate: Date;
    ddEndDate: Date;
    ddPOCurrencyValue: any;
    ddUSDValue: any;
}

export class ProjectDetails {
    projId: any;
}

export class ImportDeal {
    forcasted: any;
    fileValue: string;
    projId: string;
}

export class FinalDeal {
    ddProjId: any;
    ddForcasted: string;
}

export class ApproveDeal {
    ddID: any;
    level: any;
    sentlevel: any;
}

export class RejectDeal {
    ddID: any;
    level: any;
}

export class ProjectType {
    searchKey: string;
}
