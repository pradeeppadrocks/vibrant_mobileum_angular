import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';
import { HighchartsChartModule } from 'highcharts-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { FileSaverModule } from 'ngx-filesaver';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        CustomMaterialModule,
        ToastrModule.forRoot(),
        DataTablesModule,
        HighchartsChartModule,
        NgbModule,
        BsDatepickerModule.forRoot(),
        FileSaverModule,
        NgSelectModule
    ],
    exports: [
        CustomMaterialModule,
        ToastrModule,
        DataTablesModule,
        HighchartsChartModule,
        NgbModule,
        BsDatepickerModule,
        FileSaverModule,
        NgSelectModule
    ]
})
export class ThirdPartyLibModule {}
