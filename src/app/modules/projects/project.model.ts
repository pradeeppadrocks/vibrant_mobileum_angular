export class Project {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    projDealYear: any;
    projDealQtr: any;
    companyId: any;
    regId: any;
    projId: any;
}

export class Company {
    searchKey: string;
}

export class Region {
    searchKey: string;
}

export class ConversionType {
    pageNo: any;
    pageSize: any;
    sort: string;
    order: any;
    searchKey: string;
    conversionTypeId: any;
}

export class EditProject {
    projId: any;
    dealTypeId: string;
    conversionTypeId: any;
    dealWarranty: any;
    dealWarrantyStartDate: Date;
    uatDate: Date;
    faDate: Date;
    entityCompanyCode: string;
    revenueProfile: string;
}

export class ProjectDetails {
    projId: any;
}
