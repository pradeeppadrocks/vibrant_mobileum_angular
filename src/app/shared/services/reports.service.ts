import { Injectable } from '@angular/core';
import { EndpointService } from './endpoint.service';
import { BaseService } from './base.service';
import { RegionChangeService } from './region-change.service';

@Injectable({
    providedIn: 'root'
})
export class ReportsService {
    updatedRegion: any;
    constructor(
        private endpointService: EndpointService,
        private baseService: BaseService,
        private regionUpdateService: RegionChangeService
    ) {
        this.regionUpdateService.region.subscribe(value => {
            this.updatedRegion = value;
        });
    }

    getUpdatedRegion() {
        return this.updatedRegion;
    }

    getBillingBacklogWaterfallReportData(body) {
        return this.baseService.post(this.endpointService.getBillingBacklogWaterfallReportUrl(), body);
    }

    getRevenueBacklogWaterfallReportData(body) {
        return this.baseService.post(this.endpointService.getRevenueWaterfallReportUrl(), body);
    }
}
