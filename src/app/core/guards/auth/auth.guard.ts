import { Injectable, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { AuthenticationService, ToasterService } from '../../../shared';
import { SessionService } from '../..';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, OnInit {
    constructor(
        private auth: AuthenticationService,
        private router: Router,
        private sessionService: SessionService,
        private customToasterService: ToasterService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const url: string = state.url;
        if (!this.sessionService.getSession()) {
            this.auth.redirectUrl = url;
            this.router.navigate(['', 'auth', 'login']);
            return false;
        } else if (this.sessionService.getSession() && this.auth.isTokenExpired()) {
            if (this.auth.isTokenExpired()) {
                this.auth.redirectUrl = url;
                this.customToasterService.error('Token expired.Please login');
            }
            this.router.navigate(['', 'auth', 'login']);
            return false;
        }
        return true;
    }

    ngOnInit(): void {}
}
