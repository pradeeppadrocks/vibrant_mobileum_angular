import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DealDistributionComponent } from './deal-distribution/deal-distribution.component';

const routes: Routes = [{ path: 'deal-distribution', component: DealDistributionComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)]
})
export class DealDistributionRoutingModule {}
