import { Injectable, OnInit } from '@angular/core';
import { BaseService } from './../../shared/services/base.service';
import { Project, Company, Region, ConversionType, EditProject, ProjectDetails } from './project.model';
import { environment } from '../../../environments/environment';
import { EndpointService, RegionChangeService } from '../../shared';

@Injectable()
export class ProjectService implements OnInit {
    //DevURL = 'http://192.168.1.142:9051/';
    DevURL = environment.host;

    //Project List URL
    getProjectsURL = `${this.DevURL}getProjects`;
    getCompanyURL = `${this.DevURL}getCompany`;
    getRegionURL = `${this.DevURL}getRegion`;
    getConversionTypeURL = `${this.DevURL}getConversionType`;
    updateProjectDetailURL = `${this.DevURL}updateProjectDetail`;
    getProjectDetailURL = `${this.DevURL}getProjectDetail`;

    updatedRegion;
    constructor(
        private baseService: BaseService,
        private regionUpdateService: RegionChangeService,
        private endpointService: EndpointService
    ) {
        this.regionUpdateService.region.subscribe(value => {
            this.updatedRegion = value;
        });
    }

    ngOnInit() {}

    getUpdatedRegion() {
        return this.updatedRegion;
    }
    //Project List API
    getProjectList(data: Project) {
        return this.baseService.post(this.getProjectsURL, data);
    }

    getCompanyList(data: Company) {
        return this.baseService.post(this.getCompanyURL, data);
    }

    getRegionList(data: Region) {
        return this.baseService.post(this.getRegionURL, data);
    }

    getConversionType(data: ConversionType) {
        return this.baseService.post(this.getConversionTypeURL, data);
    }

    updateProjectDetail(data: EditProject) {
        return this.baseService.post(this.updateProjectDetailURL, data);
    }

    getProjectDetails(data: ProjectDetails) {
        return this.baseService.post(this.getProjectDetailURL, data);
    }

    importAllMilestone(body) {
        return this.baseService.post(this.endpointService.getImportAllMsUrl(), body);
    }
}
