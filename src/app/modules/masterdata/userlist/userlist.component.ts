import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { User, EditUser, UserType, DeleteMasterdata, Region, SFDCUsers } from './../master.model';
import { MasterService } from '../master.service';
import { ToasterService } from '../../../shared';
import { CONSTANTS } from 'src/app/shared/constant';
import { DataTableDirective } from 'angular-datatables';
import 'datatables.net';
import 'datatables.net-bs4';
import { Subject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SessionService } from '../../../core';

@Component({
    selector: 'app-master-userlist',
    templateUrl: './userlist.component.html',
    styleUrls: ['./userlist.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class UserListComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('editmoduleaccess') editModuleAccesspopup: ModalDirective;
    @ViewChild('edituser') editUserpopup: ModalDirective;
    @ViewChild('confirmdialog') confirmDialog: ModalDirective;
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
    deleteId = '';

    dtOptions: any = {};
    userList: any[];
    pageIndex = 0;
    pageLength = 10;
    regionList: any = [];
    userTypeList: any = [];
    SFDCUserList: any = [];
    isPasswordMatch = true;
    isDisable = true;
    isEdit = false;

    userForm: FormGroup;
    userId: FormControl;
    username: FormControl;
    password: FormControl;
    confirmpassword: FormControl;
    name: FormControl;
    regId: FormControl;
    email: FormControl;
    mobile: FormControl;
    userType: FormControl;
    userDefaultAccess: FormControl;
    sfdcuser: FormControl;
    status: FormControl;
    isFormSubmitted = false;
    ModuleAccessData = [];
    accessKey = [];
    accessValue = [];
    editModuleAccess = {};
    isSearched;
    searchData;

    constructor(
        private http: HttpClient,
        private masterservice: MasterService,
        private ngxService: NgxUiLoaderService,
        private toasterservice: ToasterService,
        private sessionService: SessionService
    ) {}

    ngAfterViewInit() {
        this.dtTrigger.next();
        this.afterInit();
    }

    afterInit() {
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                const pageInfo = dtInstance.page.info();
                this.pageLength = len;
                this.pageIndex = pageInfo.page;
            });
        });
    }

    ngOnInit(): void {
        this.gridBinding();
        this.createFormControls();
        this.createFormDetails();
        this.pageLoadBinding();
        this.enableDisableAllFields(this.isDisable);
    }

    gridBinding() {
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            /*lengthMenu: [2, 5, 10],
pageLength: 2,*/
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata: User = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length != 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : 'updatedDate',
                    order: dataTablesParameters.order.length != 0 ? (dataTablesParameters.order[0].dir == 'desc' ? -1 : 1) : -1,
                    searchKey: dataTablesParameters.search.value,
                    userId: ''
                };
                that.masterservice.getUserList(temdata).subscribe(pagedData => {
                    if (pagedData.success && pagedData.data) {
                        that.userList = pagedData.data.users;
                        this.afterInit();
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        that.userList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'userId', orderable: false },
                { data: 'username' },
                { data: 'name' },
                { data: 'regions', orderable: false },
                { data: 'email' },
                { data: 'type' },
                { data: 'userId', orderable: false },
                { data: 'moredetails', orderable: false }
            ]
        };
    }

    pageLoadBinding() {
        // Get RegionList
        const regdata: Region = {
            pageNo: 1,
            pageSize: 1000,
            sort: '',
            order: '',
            searchKey: '',
            regId: ''
        };
        this.masterservice.getRegionList(regdata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.regionList = result.data.regions;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );

        // Get User Type List
        const utydata: UserType = {
            pageNo: 1,
            pageSize: 1000,
            sort: '',
            order: '',
            searchKey: '',
            userTypeId: ''
        };
        this.masterservice.getUserTypesList(utydata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.userTypeList = result.data.userTypes;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
        // Get SFDC User List
        const sfdcusrdata: SFDCUsers = {
            searchKey: ''
        };
        this.masterservice.getSfdcUserList(sfdcusrdata).subscribe(
            result => {
                if (result.success && result.data) {
                    result.data.sfdcUsers.forEach(element => {
                        this.SFDCUserList = [...this.SFDCUserList, element];
                    });
                    const otherdata = {
                        usr_id: 'other',
                        usr_name: 'Others',
                        usr_email: '',
                        usr_mobile: '',
                        usr_sfdc_id: '',
                        usr_status: 0,
                        usr_username: ''
                    };
                    this.SFDCUserList = [...this.SFDCUserList, otherdata];
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
            }
        );
    }

    createFormControls() {
        this.username = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.username)]);
        this.password = new FormControl('', [
            this.conditionalValidator(() => this.isEdit === false, Validators.required),
            Validators.pattern(CONSTANTS.regex.confirmpassword),
            Validators.minLength(CONSTANTS.regex.passwordMinLength),
            Validators.maxLength(CONSTANTS.regex.passwordMaxLength)
        ]);
        this.confirmpassword = new FormControl('');
        this.name = new FormControl('', [Validators.required]);
        this.regId = new FormControl(null, [Validators.required]);
        this.email = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.email)]);
        this.mobile = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.phone)]);
        this.userType = new FormControl('', [Validators.required]);
        this.sfdcuser = new FormControl(null);
        this.userId = new FormControl('');
        this.status = new FormControl('');
        this.userDefaultAccess = new FormControl({});
    }

    conditionalValidator(condition: () => boolean, validator: ValidatorFn): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!condition()) {
                return null;
            }
            return validator(control);
        };
    }

    createFormDetails() {
        this.userForm = new FormGroup({
            username: this.username,
            password: this.password,
            confirmpassword: this.confirmpassword,
            name: this.name,
            regId: this.regId,
            email: this.email,
            mobile: this.mobile,
            userType: this.userType,
            sfdcuser: this.sfdcuser,
            userId: this.userId,
            status: this.status,
            userDefaultAccess: this.userDefaultAccess
        });
    }

    enableDisableAllFields(disable) {
        if (disable) {
            this.userForm.get('username').disable();
            this.userForm.get('name').disable();
            this.userForm.get('password').disable();
            this.userForm.get('confirmpassword').disable();
            this.userForm.get('regId').disable();
            this.userForm.get('email').disable();
            this.userForm.get('mobile').disable();
            this.userForm.get('userType').disable();
        } else {
            this.userForm.get('username').enable();
            this.userForm.get('name').enable();
            this.userForm.get('password').enable();
            this.userForm.get('confirmpassword').enable();
            this.userForm.get('regId').enable();
            this.userForm.get('email').enable();
            this.userForm.get('mobile').enable();
            this.userForm.get('userType').enable();
        }
    }

    matchPassword() {
        if (this.userForm.value.password) {
            if (this.userForm.value.password === this.userForm.value.confirmpassword) {
                this.isPasswordMatch = true;
            } else {
                this.isPasswordMatch = false;
            }
        } else {
            this.isPasswordMatch = true;
        }
    }

    tableSearch() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = true;
            dtInstance.search(this.searchData).draw();
        });
    }

    tableReset() {
        this.datatableElement.dtInstance.then((dtInstance: any) => {
            this.isSearched = false;
            this.searchData = '';
            dtInstance.search(this.searchData).draw();
        });
    }

    showConfirm(data) {}

    changeSFDCUser(data) {
        if (data) {
            this.isDisable = false;
            this.enableDisableAllFields(this.isDisable);
            if (data.usr_id == 'other') {
                this.userId.setValue('');
                this.email.setValue('');
                this.mobile.setValue('');
                this.name.setValue('');
                this.status.setValue(1);
                this.username.setValue('');
            } else {
                this.userId.setValue(data.usr_id);
                this.email.setValue(data.usr_email);
                this.mobile.setValue(data.usr_mobile);
                this.name.setValue(data.usr_name);
                // this.status.setValue(data.usr_status);
                this.status.setValue(1);
                this.username.setValue(data.usr_username);
            }
        } else {
            this.clearForm();
            this.isDisable = true;
            this.enableDisableAllFields(this.isDisable);
        }
    }

    addUser() {
        this.isDisable = true;
        this.enableDisableAllFields(this.isDisable);
        this.isEdit = false;
        this.userId.setValue('');
        this.username.setValue('');
        this.name.setValue('');
        this.password.setValue('');
        this.confirmpassword.setValue('');
        this.regId.setValue('');
        this.email.setValue('');
        this.mobile.setValue('');
        this.userType.setValue('');
        this.sfdcuser.setValue(null);
        this.userDefaultAccess.setValue({});
        this.status.setValue(1);
        this.editUserpopup.show();
    }

    editUser(data) {
        this.isDisable = false;
        this.enableDisableAllFields(this.isDisable);
        this.isEdit = true;
        this.userId.setValue(data.userId);
        this.username.setValue(data.username);
        this.name.setValue(data.name);
        this.password.setValue('');
        this.confirmpassword.setValue('');
        this.regId.setValue(data.regions);
        this.email.setValue(data.email);
        this.mobile.setValue(data.mobile);
        this.userType.setValue(data.userTypeId);
        this.status.setValue(data.status);
        this.userDefaultAccess.setValue(data.userDefaultAccess);
        this.editUserpopup.show();
    }

    filterRegionIdFromSelectedRegionList(selectedRegions) {
        const regId = [];
        selectedRegions.forEach(item => {
            regId.push(item.reg_id);
        });
        return regId;
    }

    updateUser() {
        if (this.userForm.valid && this.isPasswordMatch) {
            this.isFormSubmitted = false;
            this.ngxService.start();
            const userFormData = this.userForm.value;
            userFormData.regId = this.filterRegionIdFromSelectedRegionList(this.userForm.value.regId);
            this.masterservice.addUpdateUser(userFormData).subscribe(
                result => {
                    if (result && result.success) {
                        this.pageIndex = 0;
                        this.pageLength = 10;
                        this.rerenderGrid();
                        this.toasterservice.showSuccess(result.message);
                        this.cancelUser();
                    } else if (result && !result.success) {
                        this.toasterservice.error(result.message);
                    } else {
                        this.toasterservice.error('Something went wrong');
                    }
                    this.ngxService.stop();
                },
                error => {
                    this.toasterservice.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }

    clearForm() {
        this.userId.setValue('');
        this.username.setValue('');
        this.name.setValue('');
        this.password.setValue('');
        this.confirmpassword.setValue('');
        this.regId.setValue('');
        this.email.setValue('');
        this.mobile.setValue('');
        this.userType.setValue('');
        this.sfdcuser.setValue(null);
        this.userDefaultAccess.setValue({});
        this.status.setValue('');
        this.isPasswordMatch = true;
    }

    cancelUser() {
        this.clearForm();
        this.userForm.reset();
        this.editUserpopup.hide();
    }

    editUserAccess(data) {
        if (data && data.userDefaultAccess) {
            this.isDisable = false;
            this.enableDisableAllFields(this.isDisable);
            this.userId.setValue(data.userId);
            this.username.setValue(data.username);
            this.name.setValue(data.name);
            this.password.setValue('');
            this.confirmpassword.setValue('');
            this.regId.setValue(data.regID);
            this.email.setValue(data.email);
            this.mobile.setValue(data.mobile);
            this.userType.setValue('');
            this.status.setValue(data.status);
            this.ModuleAccessData = [];
            this.accessKey = Object.keys(data.userDefaultAccess);
            const valuelist = Object.values(data.userDefaultAccess);
            let valstring = '';
            for (const i in valuelist) {
                valstring += parseInt(i) != valuelist.length - 1 ? valuelist[i] + ',' : valuelist[i];
            }
            const stringsplit = valstring.split(',');

            let temobj = { view: 0, add: 0, edit: 0, delete: 0 };
            this.accessValue = [];
            for (const i in stringsplit) {
                if (parseInt(i) % 4 == 0) {
                    temobj = { view: 0, add: 0, edit: 0, delete: 0 };
                    temobj.add = parseInt(stringsplit[i]);
                } else if (parseInt(i) % 4 == 1) {
                    temobj.view = parseInt(stringsplit[i]);
                } else if (parseInt(i) % 4 == 2) {
                    temobj.edit = parseInt(stringsplit[i]);
                } else if (parseInt(i) % 4 == 3) {
                    temobj.delete = parseInt(stringsplit[i]);
                    this.accessValue.push(temobj);
                }
            }
            for (const i in this.accessKey) {
                const pushdata = { moduleName: this.accessKey[i], list: this.accessValue[i] };
                this.ModuleAccessData.push(pushdata);
            }
            // for sorting
            this.ModuleAccessData = this.ModuleAccessData.sort(function(a, b) {
                if (a.moduleName < b.moduleName) {
                    return -1;
                }
                if (a.moduleName < b.moduleName) {
                    return 1;
                }
                // names must be equal
                return 0;
            });
        } else {
            this.ModuleAccessData = [];
        }
        this.editModuleAccesspopup.show();
    }

    updateModuleAccess() {
        const objdata = {};
        this.ModuleAccessData.forEach(data => {
            data.list.view = data.list.view == true ? 1 : data.list.view == false ? 0 : data.list.view;
            data.list.add = data.list.add == true ? 1 : data.list.add == false ? 0 : data.list.add;
            data.list.edit = data.list.edit == true ? 1 : data.list.edit == false ? 0 : data.list.edit;
            data.list.delete = data.list.delete == true ? 1 : data.list.delete == false ? 0 : data.list.delete;
            objdata[data.moduleName] = data.list.add + ',' + data.list.view + ',' + data.list.edit + ',' + data.list.delete;
        });

        this.userDefaultAccess.setValue(objdata);
        this.ngxService.start();
        this.masterservice.addUpdateUser(this.userForm.value).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    if (this.sessionService.getSession().data.userId == this.userForm.value.userId) {
                        this.toasterservice.warning('Plese login again for smooth use');
                    }
                    this.cancelModuleAccess();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
                this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                this.ngxService.stop();
            }
        );
    }

    cancelModuleAccess() {
        this.editModuleAccesspopup.hide();
        this.clearForm();
    }

    openDeletePopup(id) {
        this.deleteId = id;
        this.status.setValue(0);
        this.confirmDialog.show();
    }

    cancelDelete() {
        this.deleteId = '';
        this.status.setValue('');
        this.confirmDialog.hide();
    }

    deleteUser() {
        const data: DeleteMasterdata = {
            deleteArray: [this.deleteId],
            status: 0
        };
        this.masterservice.deleteUser(data).subscribe(
            result => {
                if (result && result.success) {
                    this.pageIndex = 0;
                    this.pageLength = 10;
                    this.rerenderGrid();
                    this.toasterservice.showSuccess(result.message);
                    this.cancelDelete();
                } else if (result && !result.success) {
                    this.toasterservice.error(result.message);
                } else {
                    this.toasterservice.error('Something went wrong');
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
