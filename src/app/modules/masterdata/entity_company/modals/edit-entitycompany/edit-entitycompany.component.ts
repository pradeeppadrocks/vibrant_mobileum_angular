import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MasterService } from '../../../master.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { EntityService, ToasterService } from 'src/app/shared';
import { BillingService } from 'src/app/shared/services/billing.service';

@Component({
    selector: 'app-edit-entitycompany',
    templateUrl: './edit-entitycompany.component.html',
    styleUrls: ['./edit-entitycompany.component.css']
})
export class EditEntitycompanyComponent implements OnInit {
    @Input() entity;

    entityCompanyForm: FormGroup;
    entityCode: FormControl;
    entityId: FormControl;
    status: FormControl;

    isFormSubmitted: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private entityService: EntityService,
        private ngxService: NgxUiLoaderService,
        private customToasterService: ToasterService
    ) {}

    ngOnInit() {
        this.createFormControls();
        this.createFormDetails();
    }

    createFormControls() {
        this.entityCode = new FormControl(this.entity.entityCode, [Validators.required]);
        this.entityId = new FormControl(this.entity.entityId);
        this.status = new FormControl(1);
    }

    createFormDetails() {
        this.entityCompanyForm = new FormGroup({
            entityCode: this.entityCode,
            entityId: this.entityId,
            status: this.status
        });
    }

    editEntity() {
        if (this.entityCompanyForm.valid) {
            this.isFormSubmitted = false;
            // Make backend call to save data
            this.ngxService.start();
            this.entityService.editEntity(this.entityCompanyForm.value).subscribe((res: any) => {
                this.ngxService.stop();
                if (res.success) {
                    this.customToasterService.showSuccess(res.message);
                    this.activeModal.close(res);
                } else {
                    this.customToasterService.error(res.message);
                }
            });
        } else {
            this.isFormSubmitted = true;
        }
    }
}
