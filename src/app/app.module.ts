import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// // Import custom modules  here
import { AppRoutingModule } from './app-routing.module';
import { CommonComponentModule } from './shared/common-component/common-component.module';
import { CoreModule } from './core/core.module';

// RECOMMENDED
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ToastrModule } from 'ngx-toastr';

import {
    NgxUiLoaderModule,
    NgxUiLoaderConfig,
    SPINNER,
    POSITION,
    PB_DIRECTION,
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule
} from 'ngx-ui-loader';

// Import Components here
import { AppComponent } from './app.component';

// Import Services here
import {
    AuthenticationService,
    EncDecrService,
    EndpointService,
    BaseService,
    LoginService,
    ToasterService,
    ApprovalService,
    DashboardService,
    SwDepReportService,
    ReportsService,
    PasswordService,
    ProfileService,
    RegionChangeService,
    EntityService,
    ReportBuilderService,
    ChartExportService,
    RagReportService
} from './shared';

// Need to import jwt module in root module
import { JwtModule } from '@auth0/angular-jwt';
import { ProjectsReportsComponent } from './modules/reports/projects-reports-billing/projects-reports.component';
import { SharedModule } from './shared/shared-module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DependencyReportService } from './shared/services/dependency-report.service';

export function tokenGetter() {
    const item = JSON.parse(localStorage.getItem('mobUserDetails'));
    if (item && item.hasOwnProperty('access_token')) {
        return 'MOBJWT ' + item.access_token;
    }
}

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
    bgsColor: '#00ACC1',
    bgsOpacity: 0.5,
    bgsPosition: 'bottom-right',
    bgsSize: 60,
    bgsType: 'ball-spin-clockwise',
    blur: 5,
    fgsColor: '#00ACC1',
    fgsPosition: 'center-center',
    fgsSize: 60,
    fgsType: 'three-strings',
    gap: 24,
    logoPosition: 'center-center',
    logoSize: 120,
    logoUrl: '../assets/images/Mobileum-loader-v1.gif',
    masterLoaderId: 'master',
    overlayBorderRadius: '0',
    overlayColor: 'rgba(40, 40, 40, 0.8)',
    pbColor: '#00ACC1',
    pbDirection: 'ltr',
    pbThickness: 3,
    hasProgressBar: true,
    text: '',
    textColor: '#FFFFFF',
    textPosition: 'center-center',
    threshold: 500
};

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule.withServerTransition({ appId: 'serverApp' }),
        AppRoutingModule,
        BrowserAnimationsModule,
        BsDatepickerModule.forRoot(),
        ToastrModule.forRoot(),
        CoreModule.forRoot(),
        CommonComponentModule,
        NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
        NgxUiLoaderRouterModule,
        HttpClientModule,
        CommonModule,
        FormsModule,
        SharedModule,
        JwtModule.forRoot({
            config: {
                tokenGetter,
                headerName: 'authorization',
                authScheme: 'MOBJWT',
                whitelistedDomains: [
                    'localhost:4200',
                    'localhost:4000',
                    'reqres.in',
                    'localhost:8080',
                    '192.168.1.142:9051',
                    'localhost:9051'
                ],
                blacklistedRoutes: ['localhost:4200/auth/login', 'localhost:4000/auth/login']
            }
        })
    ],
    providers: [
        EndpointService,
        AuthenticationService,
        EncDecrService,
        ToasterService,
        BaseService,
        LoginService,
        ApprovalService,
        DashboardService,
        SwDepReportService,
        ReportsService,
        PasswordService,
        ProfileService,
        RegionChangeService,
        EntityService,
        ReportBuilderService,
        ChartExportService,
        RagReportService,
        DependencyReportService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
