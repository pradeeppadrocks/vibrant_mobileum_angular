import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteEntityCodeComponent } from './delete-entity-code.component';

describe('DeleteEntityCodeComponent', () => {
    let component: DeleteEntityCodeComponent;
    let fixture: ComponentFixture<DeleteEntityCodeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DeleteEntityCodeComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DeleteEntityCodeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
