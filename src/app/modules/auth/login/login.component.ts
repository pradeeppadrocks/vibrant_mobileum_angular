import { Component, OnInit } from '@angular/core';
import { AuthenticationService, LoginService, ToasterService } from '../../../shared';
import { SessionService } from '../../../core/services/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CONSTANTS } from 'src/app/shared/constant';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    username: FormControl;
    password: FormControl;

    isFormSubmitted = false;

    constructor(
        private loginService: LoginService,
        private sessionService: SessionService,
        private router: Router,
        private ngxService: NgxUiLoaderService,
        private customToasterService: ToasterService,
        private authService: AuthenticationService
    ) {}

    ngOnInit() {
        this.createFormControls();
        this.createLoginForm();
    }

    createFormControls() {
        this.username = new FormControl('', [Validators.required, Validators.pattern(CONSTANTS.regex.username)]);
        this.password = new FormControl('', [
            Validators.required,
            Validators.minLength(CONSTANTS.regex.passwordMinLength),
            Validators.maxLength(CONSTANTS.regex.passwordMaxLength)
        ]);
    }

    createLoginForm() {
        this.loginForm = new FormGroup({
            username: this.username,
            password: this.password
        });
    }

    login() {
        if (this.loginForm.valid) {
            this.isFormSubmitted = false;
            this.ngxService.start();
            this.loginService.login(this.loginForm.value).subscribe(
                (res: any) => {
                    if (res.code === 1022) {
                        this.customToasterService.showSuccess('You are successfully logged in');
                        this.sessionService.setSession(res);
                        if (this.authService.redirectUrl) {
                            this.router.navigate([this.authService.redirectUrl]);
                            this.authService.redirectUrl = null;
                        } else {
                            this.router.navigate(['', 'dashboard']);
                        }
                    } else {
                        this.customToasterService.error(res.message);
                    }
                    this.ngxService.stop();
                },
                error1 => {
                    this.customToasterService.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        } else {
            this.isFormSubmitted = true;
        }
    }
}
