import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateMilestoneModalComponent } from '../create-milestone-modal/create-milestone-modal.component';
import { ToasterService } from '../../../../../shared';
import { BillingService } from 'src/app/shared/services/billing.service';
import { ForecastedMilestone } from 'src/app/shared/models/billing.model';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-consolidated-bm-modal',
    templateUrl: './consolidated-bm-modal.component.html',
    styleUrls: ['./consolidated-bm-modal.component.css']
})
export class ConsolidatedBmModalComponent implements OnInit {
    @Input() milestoneData;
    @Input() projectDetails;
    msData: any[];
    createMsData;
    defaultSqlDate = '1900-01-01T00:00:00.000Z';

    constructor(
        public activeModal: NgbActiveModal,
        private billingService: BillingService,
        private modalService: NgbModal,
        private toasterservice: ToasterService,
        private ngxService: NgxUiLoaderService
    ) {}

    ngOnInit() {
        this.getConsolidatedMsData();
    }

    openCreateBillingModal(value, msType?: any, msId?: any) {
        const modalRef1 = this.modalService.open(CreateMilestoneModalComponent, { size: 'xl' as any, centered: true });
        modalRef1.componentInstance.projectDetails = this.projectDetails;
        modalRef1.componentInstance.fiterValue = value;
        modalRef1.componentInstance.msType = msType;
        modalRef1.componentInstance.msId = msId;
        modalRef1.componentInstance.projectId = this.projectDetails.projId;

        modalRef1.result.then(
            result => {
                if (result.success) {
                    // this.activeModal.close(result);
                    this.createMsData = result;
                    this.toasterservice.showSuccess(result.message);
                } else {
                    this.toasterservice.error(result.message);
                }
            },
            reason => {}
        );
    }

    closeModal() {
        this.activeModal.dismiss(this.createMsData);
    }

    getConsolidatedMsData() {
        const milestoneData = {
            pageNo: 1,
            pageSize: 5,
            sort: '',
            order: 1,
            searchKey: '',
            bmProjID: this.milestoneData.projectId,
            bmForcasted: this.milestoneData.forcastedId,
            bmID: this.milestoneData.milestoneId,
            consolidated: 1
        };
        this.ngxService.start();
        this.billingService.getForecastedMilestone(milestoneData).subscribe((res: any) => {
            if (res.success) {
                this.msData = res.data.forcastedmilestones;
            } else {
                this.msData = [];
            }
            this.ngxService.stop();
        });
    }

    getDelayInWeeks(startDate, actualStartDate) {
        const date1 = new Date(startDate);
        const date2 = new Date(actualStartDate);
        const timeDiff = Math.abs(date2.getTime() - date1.getTime());
        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const diffInWeeks = Math.ceil(diffDays / 7);
        return diffInWeeks;
    }
}
