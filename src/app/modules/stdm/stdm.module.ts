import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StdmRoutingModule} from './stdm-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StdmRoutingModule
  ]
})
export class StdmModule {
}
