import { NgModule, ModuleWithProviders } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HeaderInterceptor } from './interceptors/interceptor';

import { SessionService } from './services/session.service';
import { UtilityService } from './services/utility.service';
import { AuthModule } from '../modules/auth/auth.module';

@NgModule({
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HeaderInterceptor,
            multi: true
        },
        SessionService,
        UtilityService
    ],
    imports: [AuthModule]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule
        };
    }
}
