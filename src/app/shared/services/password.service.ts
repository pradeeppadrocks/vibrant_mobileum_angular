import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { EndpointService } from './endpoint.service';

@Injectable({
    providedIn: 'root'
})
export class PasswordService {
    constructor(private baseService: BaseService, private endpointService: EndpointService) {}

    resetPassword(data) {
        const body = {
            password: data.password,
            currPassword: data.currPassword
        };
        return this.baseService.post(this.endpointService.getResetPasswordUrl(), body);
    }
}
