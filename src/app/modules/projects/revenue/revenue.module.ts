import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RevenueRoutingModule } from './revenue-routing.module';
import { RevenueComponent } from './revenue/revenue.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from 'src/app/shared/shared-module';
import { RevenueService } from './revenue.service';
import { LOBRevenueModalComponent } from './modals/lob-revenue-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [RevenueComponent, LOBRevenueModalComponent],
    imports: [CommonModule, SharedModule, ModalModule.forRoot(), RevenueRoutingModule, FormsModule, ReactiveFormsModule],
    exports: [RevenueComponent],
    providers: [RevenueService],
    entryComponents: [LOBRevenueModalComponent]
})
export class RevenueModule {}
