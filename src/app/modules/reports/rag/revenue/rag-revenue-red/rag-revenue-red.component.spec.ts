import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RagRevenueRedComponent } from './rag-revenue-red.component';

describe('RagRevenueRedComponent', () => {
    let component: RagRevenueRedComponent;
    let fixture: ComponentFixture<RagRevenueRedComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RagRevenueRedComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RagRevenueRedComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
