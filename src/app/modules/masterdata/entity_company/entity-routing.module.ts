import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntityCompanyCodeComponent } from './entity-company-code/entity-company-code.component';

const routes: Routes = [{ path: 'entitycompanycode', component: EntityCompanyCodeComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EntityRoutingModule {}
