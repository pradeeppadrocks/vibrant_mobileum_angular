import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BaseService, EndpointService, RegionChangeService, SwDepReportService } from '../../../../shared/index';
import { Subject } from 'rxjs';
import { DependencyReportService } from '../../../../shared/services/dependency-report.service';
import { FileSaverService } from 'ngx-filesaver';

@Component({
    selector: 'app-hw-dep',
    templateUrl: './hw-dep.component.html',
    styleUrls: ['./hw-dep.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class HwDepComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtTrigger = new Subject<any>();

    dtOptions: any = {};
    pageIndex = 0;
    pageLength = 10;
    hwDepData: any[];
    currentYear = new Date().getFullYear();
    totalBillingLastYear = 'totalBilling' + (this.currentYear - 1);
    billingBacklogLastYear = 'billingBacklog' + (this.currentYear - 1);
    billingCurrentYear = 'billingBacklog' + this.currentYear;
    billingNextYear = 'totalBilling' + (this.currentYear + 1);
    biliingAfter2yr = 'totalBilling' + (this.currentYear + 2);
    biliingAfter3yr = 'totalBilling' + (this.currentYear + 3);
    currentQtr = 'Q' + this.getCurrentQtr();
    totalBillingCrntQtr = 'totalBilling' + this.currentQtr;

    selectedYear = new Date().getFullYear();
    yearList = [];

    regions;
    subscriptions;

    constructor(
        private http: HttpClient,
        private swDepService: SwDepReportService,
        private dependencyReportService: DependencyReportService,
        private regionUpdateService: RegionChangeService,
        private baseService: BaseService,
        private endpointService: EndpointService,
        private fileSaverService: FileSaverService
    ) {
        this.regions = this.dependencyReportService.getUpdatedRegion();
        this.subscriptions = this.regionUpdateService.region.subscribe(value => {
            this.regions = value;
            this.rerender();
        });
    }

    ngAfterViewInit() {
        this.dtTrigger.next();
        // This is to get table instance
        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                this.pageLength = len;
            });
        });
    }

    ngOnInit() {
        this.constructYearList();
        const that = this;
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            bFilter: false,
            searching: false,
            paging: true,
            info: true,
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                dataTablesParameters.draw = this.pageIndex + 1;
                dataTablesParameters.length = this.pageLength;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    projId: '',
                    depId: 2,
                    regions: this.regions
                };
                this.swDepService.getSwDepData(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.hwDepData = pagedData.data.swDependencies;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.hwDepData = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [{ data: 'ca_dep_owner', orderable: false }]
        };
    }

    export() {
        const body = {
            pageSize: 10,
            pageNo: 1,
            order: -1,
            sort: '',
            projId: '',
            year: this.selectedYear,
            depId: 2,
            xlsExport: true,
            regions: this.regions ? this.regions : ''
        };
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        this.baseService
            .post(this.endpointService.getSfDependencyUrl(), body, { responseType: 'arraybuffer', headers })
            .subscribe((res: any) => {
                this.fileSaverService.save(
                    new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' }),
                    this.selectedYear + ' Hw Dep.xlsx'
                );
            });
    }

    getCurrentQtr() {
        const today = new Date();
        return Math.floor((today.getMonth() + 3) / 3);
    }

    getUniqOwner(ownerList) {
        const uniqueOwnerList = ownerList
            .split(',')
            .filter((items, i, a) => {
                return i === a.indexOf(items);
            })
            .join(', ');
        return uniqueOwnerList;
    }

    constructYearList() {
        const lastYear = 2000;
        const currentYear = new Date().getFullYear();
        for (let i = currentYear; i >= lastYear; i--) {
            const obj = { id: i, value: i };
            this.yearList.push(obj);
        }
    }

    selectedYearOnChange(year) {
        this.rerender();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
        if (!!this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    rerender(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }
}
