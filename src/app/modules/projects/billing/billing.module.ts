import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BillingRoutingModule } from './billing-routing.module';
import { BillingComponent } from './billing/billing.component';
import { SharedModule } from 'src/app/shared/shared-module';
import { MasterService } from '../../masterdata/master.service';
import { ViewBillingMilestoneComponent } from './modals/view-billing-milestone/view-billing-milestone.component';
import { DeleteModalComponent } from './modals/delete-modal/delete-modal.component';
import { CreateMilestoneModalComponent } from './modals/create-milestone-modal/create-milestone-modal.component';
import { BillingHistoryModalComponent } from './modals/billing-history-modal/billing-history-modal.component';
import { ConsolidatedBmModalComponent } from './modals/consolidated-bm-modal/consolidated-bm-modal.component';
import { DependenciesModalComponent } from './modals/create-milestone-modal/dependencies-modal/dependencies-modal.component';
import { RemarksModalComponent } from './modals/remarks-modal/remarks-modal.component';

@NgModule({
    declarations: [
        BillingComponent,
        ViewBillingMilestoneComponent,
        DeleteModalComponent,
        CreateMilestoneModalComponent,
        BillingHistoryModalComponent,
        ConsolidatedBmModalComponent,
        DependenciesModalComponent,
        RemarksModalComponent
    ],
    imports: [FormsModule, ReactiveFormsModule, CommonModule, SharedModule, ModalModule.forRoot(), BillingRoutingModule],
    exports: [BillingComponent],
    providers: [MasterService],
    entryComponents: [
        ViewBillingMilestoneComponent,
        DeleteModalComponent,
        CreateMilestoneModalComponent,
        ConsolidatedBmModalComponent,
        DependenciesModalComponent,
        RemarksModalComponent
    ]
})
export class BillingModule {}
