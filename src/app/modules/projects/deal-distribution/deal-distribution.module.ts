import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DealDistributionRoutingModule } from './deal-distribution-routing.module';
import { DealDistributionComponent } from './deal-distribution/deal-distribution.component';
import { SharedModule } from 'src/app/shared/shared-module';
import { DealDistributionService } from './deal-distribution.service';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    declarations: [DealDistributionComponent],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        SharedModule,
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        DealDistributionRoutingModule
    ],
    exports: [DealDistributionComponent],
    providers: [DealDistributionService]
})
export class DealDistributionModule {}
