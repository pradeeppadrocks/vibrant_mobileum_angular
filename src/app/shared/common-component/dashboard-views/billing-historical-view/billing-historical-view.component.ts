import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Router } from '@angular/router';
import { YearChart } from '../../../models/dashboard.model';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../../services/dashboard.service';
import { ChartExportService, RegionChangeService } from '../../..';
import { HttpClient } from '@angular/common/http';
import { last } from 'rxjs/operators';

@Component({
    selector: 'app-billing-historical-view',
    templateUrl: './billing-historical-view.component.html',
    styleUrls: ['./billing-historical-view.component.css']
})
export class BillingHistoricalViewComponent implements OnInit {
    constructor(
        private router: Router,
        private dashboardservice: DashboardService,
        private toasterservice: ToasterService,
        private regionUpdateService: RegionChangeService,
        private http: HttpClient,
        public chartExportService: ChartExportService
    ) {}

    Highcharts: typeof Highcharts = Highcharts; // Highcharts, it's Highcharts
    historyChartOptions = {};
    historyEntityChartOptions = {};
    billingReportDashboardOneOptions = {};
    billingReportDashboardSecondOptions = {};
    billingHistoryRegionOptions = {};

    yearList = [];
    selectedYear = '2019';
    historicalChartInstance;
    updateHistoricalViewChart: boolean = false;
    updatebillingReportDashboardOneViewChart: boolean = false;
    updatebillingReportDashboardSecondViewChart: boolean = false;
    updateHistoryEnityViewChart = false;
    updateHistoryRegionViewChart = false;

    isDashboardChartOrTable = true;
    historyoneToOneFlag = false;
    oneDashboardoneToOneFlag = false;
    secondDashboardoneToOneFlag = false;
    historyEntityOneToOneFlag: boolean = false;
    historyRegionOneToOneFlag = false;

    monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    tableMonthList = [
        'Jan',
        'Feb',
        'Mar',
        'Total',
        'Apr',
        'May',
        'Jun',
        'Total',
        'Jul',
        'Aug',
        'Sep',
        'Total',
        'Oct',
        'Nov',
        'Dec',
        'Total'
    ];

    // secondDashboardCategoryList = [];

    billingHistoryList = [];
    billingDashboardOneList = [];
    billingDashboardSecondList = [];
    billingHistoryEntityList = [];
    billingRegionList = [];

    showHistoryChartOptions;
    showBillingReportDashboardChartOptions;
    showBillingRevenueDashboardChartOptions;
    showBillingEntityChartOptions;
    showBillingRegionChartOptions;

    lobTimeStamp;
    historyTimeStamp;
    historyBillingDashboardTimeStamp;
    entityTimeStamp;
    regionTimeStamp;

    totalHistoryViewData = 0;

    ngOnInit() {
        this.pageLoadBinding(this.selectedYear);
        this.loadHistoricChartOptions();
        // Year Binding
        const lastYear = 2009;
        const today = new Date();
        const year = today.getFullYear();
        for (let i = year; i >= lastYear; i--) {
            const yearData = { id: i, value: i };
            this.yearList.push(yearData);
        }

        this.regionUpdateService.region.subscribe(value => {
            this.refreshPage();
        });
    }

    refreshPage() {
        this.showHistoryChartOptions = false;
        this.showBillingReportDashboardChartOptions = false;
        this.showBillingRevenueDashboardChartOptions = false;
        this.showBillingEntityChartOptions = false;
        this.pageLoadBinding(this.selectedYear);
    }

    pageLoadBinding(year) {
        //  this.ngxService.start();
        var yeardata: YearChart = {
            year: year
        };
        // setTimeout(() => {
        this.dashboardservice.getBillingHistoryView(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.billingHistoryList = this.bindingGridData(result.data.reportData);
                    console.log(this.billingHistoryList);
                    this.historyChartOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.historyChartOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.historyTimeStamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.historyoneToOneFlag = true;
                    this.updateHistoricalViewChart = true;
                    this.showHistoryChartOptions = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );

        this.dashboardservice.getBillingEntityHistoryView(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.billingHistoryEntityList = this.bindingGridData(result.data.reportData);
                    this.historyEntityChartOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.historyEntityChartOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.entityTimeStamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.historyEntityOneToOneFlag = true;
                    this.updateHistoryEnityViewChart = true;
                    this.showBillingEntityChartOptions = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );

        this.dashboardservice.getBillingRegionHistoryView(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.billingRegionList = this.bindingGridData(result.data.reportData);
                    this.billingHistoryRegionOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.billingHistoryRegionOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.regionTimeStamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.historyRegionOneToOneFlag = true;
                    this.updateHistoryRegionViewChart = true;
                    this.showBillingRegionChartOptions = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );

        // }, 500);
        // setTimeout(() => {
        this.dashboardservice.getBillingHistoryViewDashbord(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.billingDashboardOneList = this.bindingGridData(result.data.reportData);
                    this.billingReportDashboardOneOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.billingReportDashboardOneOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.historyBillingDashboardTimeStamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.updatebillingReportDashboardOneViewChart = true;
                    this.oneDashboardoneToOneFlag = true;
                    this.showBillingReportDashboardChartOptions = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
        //}, 1000);
        //setTimeout(() => {
        this.dashboardservice.getBillingHistoryViewLOB(yeardata).subscribe(
            result => {
                if (result.success && result.data) {
                    // this.bindingsecondChartData(result.data);
                    this.billingDashboardSecondList = this.bindingGridData(result.data.reportData);
                    this.billingReportDashboardSecondOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.billingReportDashboardSecondOptions['drilldown'].series = this.bindingDrildownChartData(result.data.reportData);
                    this.lobTimeStamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.updatebillingReportDashboardSecondViewChart = true;
                    this.secondDashboardoneToOneFlag = true;
                    this.showBillingRevenueDashboardChartOptions = true;
                    console.log(this.totalHistoryViewData);
                }
                //  this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                // this.ngxService.stop();
            }
        );
        //}, 1500);
    }

    bindingChartData(data) {
        var list = [];
        data.forEach(ele => {
            // var tempdata = { name: ele.name, data: [0, 0, 0, 0] };
            var tempdata = {
                name: ele.name,
                data: [
                    { name: 'Q1', y: 0, drilldown: 'Q1 ' + ele.name },
                    { name: 'Q2', y: 0, drilldown: 'Q2 ' + ele.name },
                    { name: 'Q3', y: 0, drilldown: 'Q3 ' + ele.name },
                    { name: 'Q4', y: 0, drilldown: 'Q4 ' + ele.name }
                ]
            };
            for (var i = 0; i < 12; i++) {
                if (i == 0 || i == 1 || i == 2) {
                    tempdata.data[0].y += ele.value[i].value;
                } else if (i == 3 || i == 4 || i == 5) {
                    tempdata.data[1].y += ele.value[i].value;
                } else if (i == 6 || i == 7 || i == 8) {
                    tempdata.data[2].y += ele.value[i].value;
                } else if (i == 9 || i == 10 || i == 11) {
                    tempdata.data[3].y += ele.value[i].value;
                }
                // this.totalHistoryViewData += ele.value[i].value;
            }
            list.push(tempdata);
        });
        return list;
    }

    bindingDrildownChartData(data) {
        var list = [];
        data.forEach(ele => {
            for (var i = 1; i < 5; i++) {
                list.push({ name: 'Q' + i + ' ' + ele.name, id: 'Q' + i + ' ' + ele.name, data: [], count: i, tempname: ele.name });
            }
        });
        data.forEach(ele => {
            var tempdata = { name: '', id: '', data: [] };
            for (var i = 0; i < 12; i++) {
                if (i == 0 || i == 1 || i == 2) {
                    list.forEach(subele => {
                        if (subele.count == 1 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                } else if (i == 3 || i == 4 || i == 5) {
                    list.forEach(subele => {
                        if (subele.count == 2 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                } else if (i == 6 || i == 7 || i == 8) {
                    list.forEach(subele => {
                        if (subele.count == 3 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                } else if (i == 9 || i == 10 || i == 11) {
                    list.forEach(subele => {
                        if (subele.count == 4 && subele.tempname == ele.name) {
                            subele.data.push(this.makeDrilDownData(this.monthList[i], ele.value[i].value));
                        }
                    });
                }
            }
        });
        return list;
    }

    makeDrilDownData(month, value) {
        let arr = [];
        arr.push(month);
        arr.push(value);
        return arr;
    }

    bindingGridData(data) {
        var list = [];
        var totalarr = [];
        const quarterTotal = 0;
        const lastYearTotal = 0;
        data.forEach(ele => {
            var tempdata = { name: ele.name, data: [] };
            var total = 0;
            let quarterTotal = 0;
            let growth = 0;
            const lastYearBilling = ele.lastYearBilling ? ele.lastYearBilling : 0;
            for (const index in ele.value) {
                const indexInInt = Number(index);
                tempdata.data.push(ele.value[indexInInt].value);
                total += ele.value[indexInInt].value;
                quarterTotal += ele.value[indexInInt].value;
                if ((indexInInt + 1) % 3 === 0) {
                    tempdata.data.push(quarterTotal);
                    quarterTotal = 0;
                }
            }
            growth =
                total - lastYearBilling !== 0 ? (lastYearBilling !== 0 ? ((total - lastYearBilling) / lastYearBilling) * 100 : null) : 0;
            tempdata.data.push(total);
            tempdata.data.push(lastYearBilling);
            tempdata.data.push(growth);
            list.push(tempdata);
        });

        for (var i = 0; i < 19; i++) {
            var temptotal = 0;
            list.forEach(ele => {
                temptotal += ele.data[i];
            });
            totalarr.push(temptotal);
        }

        // ADD growth to total array
        const currentYearBillingTotal = totalarr[totalarr.length - 3];
        const lastYearBillingTotal = totalarr[totalarr.length - 2];

        const growth =
            currentYearBillingTotal - lastYearBillingTotal !== 0
                ? lastYearBillingTotal !== 0
                    ? ((currentYearBillingTotal - lastYearBillingTotal) / lastYearBillingTotal) * 100
                    : null
                : 0;
        totalarr[totalarr.length - 1] = growth;

        list.push({ name: '', data: totalarr });
        return list;
    }

    selectedYearOnChange(year) {
        this.showHistoryChartOptions = false;
        this.showBillingReportDashboardChartOptions = false;
        this.showBillingRevenueDashboardChartOptions = false;
        this.pageLoadBinding(year);
    }

    loadHistoricChartOptions() {
        this.historyChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Historical'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        // format: '${point.y:.1f}'
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.historyEntityChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Historical Entity'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.billingHistoryRegionOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Historical Region'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (this.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },
            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.billingReportDashboardOneOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Report Dashboard'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },

            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.billingReportDashboardSecondOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Billing Report Dashboard'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },

            series: [],
            drilldown: {
                allowPointDrilldown: false,
                series: []
            },
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };
    }

    chartLegendLineRemove(chart) {
        const series = chart.series;
        $(series).each((i, serie) => {
            if (serie['legendLine']) {
                serie['legendLine'].destroy();
            }
        });
    }

    RedirectBillingForecast() {
        this.router.navigate(['dashboard-billing-forecast-view']);
    }

    RedirectBillingBacklog() {
        this.router.navigate(['dashboard-billing-backlog-view']);
    }

    getTimestampDiffInHours(t2) {
        const date1 = +new Date();
        const date2 = +new Date(parseInt(t2, 10));

        const diff = Math.abs(date1 - date2) / 1000;

        // get total days between two dates
        const days = Math.floor(diff / 86400);

        // get hours
        const hours = Math.floor(diff / 3600) % 24;

        // get minutes
        const minutes = Math.floor(diff / 60) % 60;

        // get seconds
        const seconds = diff % 60;

        if (hours > 0) {
            return hours + ' hrs ' + minutes + ' minutes';
        } else if (minutes > 0) {
            return minutes + ' minutes';
        } else if (seconds > 0) {
            return seconds + ' seconds';
        }
    }
}
