import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../../../../shared/services/billing.service';
import { ToasterService } from '../../../../../shared';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CONSTANTS } from '../../../../../shared/constant';

@Component({
    selector: 'app-remarks-modal',
    templateUrl: './remarks-modal.component.html',
    styleUrls: ['./remarks-modal.component.css']
})
export class RemarksModalComponent implements OnInit {
    remarks;
    errorMsg = false;
    constructor(
        public activeModal: NgbActiveModal,
        private billingService: BillingService,
        private customToasterservice: ToasterService,
        private ngxService: NgxUiLoaderService
    ) {}

    ngOnInit() {}

    omitSpecialChar(event) {
        let k;
        k = event.charCode; // k = event.keyCode; (Both can be used)
        return (k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32 || k === 44 || k === 46 || (k >= 48 && k <= 57);
    }

    saveRemarks() {
        if (this.remarks) {
            if (!this.remarks || this.remarks.length < 10) {
                this.errorMsg = true;
                return;
            } else {
                this.activeModal.close(this.remarks);
            }
        } else {
            return;
        }
    }

    remarksChanged(event) {
        this.remarks = this.remarks.trim();
        if (!this.remarks || this.remarks.length < 10) {
            this.errorMsg = true;
        } else {
            this.errorMsg = false;
        }
    }
}
