import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SessionService } from '../../../core';
import { ForecastedMilestone } from '../../models/billing.model';
import { DataTableDirective } from 'angular-datatables';
import { ApprovalService, ToasterService } from '../..';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ApproveModalComponent } from '../approve-modal/approve-modal.component';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-approval',
    templateUrl: './approval.component.html',
    styleUrls: ['./approval.component.css', '../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class ApprovalComponent implements OnInit, AfterViewInit {
    userType;
    level;

    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtOptions: any = {};
    dtTrigger = new Subject<any>();
    approvalProjectList: any[];
    pageIndex = 0;
    pageLength = 10;

    constructor(
        private sessionService: SessionService,
        private approvalService: ApprovalService,
        private toasterservice: ToasterService,
        private modalService: NgbModal,
        config: NgbModalConfig
    ) {}

    ngOnInit() {
        this.getUserType();
        this.level = this.checkUserType();
        this.renderProjectApprovetable();
    }

    ngAfterViewInit() {
        // This is to get table instance
        this.dtTrigger.next();

        // Get event when page number is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('page.dt', () => {
                const pageInfo = dtInstance.page.info();
                dtInstance.page.len();
                this.pageIndex = pageInfo.page;
                this.pageLength = pageInfo.length;
            });
        });

        // Get event when page length is changed
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.on('length.dt', (e, settings, len) => {
                this.pageLength = len;
            });
        });
    }

    getUserType() {
        // this.userType = this.getUserData().type;
        this.userType = this.sessionService.getSession().data.username;
        return this.userType;
    }

    checkUserType() {
        if (this.userType.indexOf('1') > -1) {
            return 1;
        } else if (this.userType.indexOf('2') > -1) {
            return 2;
        } else {
            return null;
        }
    }

    renderProjectApprovetable() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            serverSide: true,
            processing: true,
            order: [],
            language: {
                paginate: {
                    next: '>',
                    previous: '<'
                },
                sLengthMenu: 'Display _MENU_ ',
                processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw table-loader"></i>'
            },
            ajax: (dataTablesParameters: any, callback) => {
                // We can replace the value of dataTablesParameters as backend api need
                // dataTablesParameters.test = 1;
                const temdata = {
                    pageNo: this.pageIndex + 1,
                    pageSize: this.pageLength,
                    sort:
                        dataTablesParameters.order.length !== 0
                            ? dataTablesParameters.columns[dataTablesParameters.order[0].column].data
                            : '',
                    order: dataTablesParameters.order.length !== 0 ? (dataTablesParameters.order[0].dir === 'desc' ? -1 : 1) : 1,
                    searchKey: dataTablesParameters.search.value,
                    level: this.level
                };

                this.approvalService.getApprovalProjectList(temdata).subscribe((pagedData: any) => {
                    if (pagedData.success && pagedData.data) {
                        this.approvalProjectList = pagedData.data.forcastedmilestones;
                        callback({
                            recordsTotal: pagedData.data.totalCount,
                            recordsFiltered: pagedData.data.totalCount,
                            data: []
                        });
                    } else {
                        this.approvalProjectList = [];
                        callback({
                            recordsTotal: 0,
                            recordsFiltered: 0,
                            data: []
                        });
                    }
                });
            },
            columns: [
                { data: 'bmMilestoneName', orderable: true },
                { data: 'bmMsStartDate', orderable: true },
                { data: 'bmMsEndDate', orderable: true },
                { data: 'Ageing/Delays', orderable: false },
                { data: 'bmAmount', orderable: true },
                { data: 'action', orderable: false }
            ]
        };
    }

    getDelayInWeeks(startDate, actualStartDate) {
        const date1 = new Date(startDate);
        const date2 = new Date(actualStartDate);
        const timeDiff = Math.abs(date2.getTime() - date1.getTime());
        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const diffInWeeks = Math.ceil(diffDays / 7);
        return diffInWeeks;
    }

    openViewMsModal(msData) {
        const modalRef = this.modalService.open(ApproveModalComponent, {
            windowClass: 'dark-modal',
            size: 'xl' as any,
            centered: true
        });
        // Need to pass data to modal
        modalRef.componentInstance.milestoneData = msData;
        modalRef.componentInstance.userLevelType = this.level;

        modalRef.result.then(
            result => {
                if (result.success) {
                }
            },
            reason => {
                if (reason.success) {
                    this.rerenderGrid();
                }
            }
        );
    }

    rerenderGrid(): void {
        this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
