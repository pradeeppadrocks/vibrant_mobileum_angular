import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { TimeliveService } from '../../../../shared/services/timelive.service';
import { ActivatedRoute } from '@angular/router';
import { ToasterService } from '../../../../shared';
import { CONSTANTS } from '../../../../shared/constant';
import { ImportDeal } from '../../deal-distribution/deal-distribution.model';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
    selector: 'app-timelive',
    templateUrl: './timelive.component.html',
    styleUrls: ['./timelive.component.css', '../../../../../assets/vendor/data_table/css/data-table-custom.css']
})
export class TimeliveComponent implements OnInit, AfterViewInit {
    constructor(
        private timeliveService: TimeliveService,
        private route: ActivatedRoute,
        private customTosterService: ToasterService,
        private ngxService: NgxUiLoaderService
    ) {}

    @Input() projectDetails;

    @ViewChild('db') table: ElementRef;
    @ViewChild(DataTableDirective)
    datatableElement: DataTableDirective;
    dtOptions: any = {};
    timeLiveData: any[];
    pageIndex = 0;
    pageLength = 10;

    projectId;
    currentQtr;
    currentMonth;
    timeliveDataQtr = {
        Q1: {
            amountUsdActual: 0,
            amountUsdForcasted: 0,
            quater: 1,
            percentage: 0,
            year: 0
        },
        Q2: {
            amountUsdActual: 0,
            amountUsdForcasted: 0,
            quater: 2,
            percentage: 0,
            year: 0
        },
        Q3: {
            amountUsdActual: 0,
            amountUsdForcasted: 0,
            quater: 3,
            percentage: 0,
            year: 0
        },
        Q4: {
            amountUsdActual: 0,
            amountUsdForcasted: 0,
            quater: 4,
            percentage: 0,
            year: 0
        },
        total: 0,
        balance: 0,
        totalActual: 0
    };

    currYear = new Date().getDate();
    timeliveDataMonth = [];
    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    monthDiff;
    diasbleTimeliveMonthBtn = true;

    timeliveEffortData = [];
    disableTimeliveEffortBtn = true;

    numberPattern = CONSTANTS.regex.integer;

    static getCurrentQtr() {
        const today = new Date();
        return Math.floor((today.getMonth() + 3) / 3);
    }

    static getCurrentMonth() {
        const today = new Date();
        return Math.floor(today.getMonth() + 1);
    }

    static getMonthDiff(d1, d2) {
        let months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth() + 1;
        return months <= 0 ? 0 : months;
    }

    ngAfterViewInit() {}

    ngOnInit() {
        this.currentQtr = TimeliveComponent.getCurrentQtr();
        this.currentMonth = TimeliveComponent.getCurrentMonth();
        this.projectId = this.route.snapshot.paramMap.get('id');
        if (this.projectId) {
            //  this.getProjectDetailsById(this.projectId);
            this.getTimeliveData();
        }

        // IF condition matched (RevenueProfileCASH = 1) && Month diff from current date to project end date
        this.monthDiff = TimeliveComponent.getMonthDiff(new Date(), new Date(this.projectDetails.endDate));
        this.timeliveDataMonth = this.constructTimeliveDataMonthly();

        this.timeliveEffortData = this.constructProjectEffortData();
    }

    getProjectDetailsById(projId) {
        this.timeliveService.getProjectDetailsByProjectId(projId).subscribe((res: any) => {
            if (res.success) {
                this.projectDetails = res.data.projects[0];
            } else {
                this.customTosterService.error('Unable to fetch details...please try again');
            }
        });
    }

    constructTimeliveDataMonthly(): any[] {
        const timeLiveDataResult = [];
        const obj = {
            amountUsdActual: 0,
            amountUsdForcasted: 0,
            month: 0,
            percentage: undefined,
            year: undefined,
            additionalAmount: undefined,
            remarks: ''
        };

        const yearRem = this.monthDiff % 12;
        //  let yearD = Math.floor(this.monthDiff / 12);

        const projectEndLastMonth = Math.floor(new Date(this.projectDetails.endDate).getMonth() + 1);
        const currentYear = new Date().getFullYear();
        const projectEndLastYear = new Date(this.projectDetails.endDate).getFullYear();
        let yearD = projectEndLastYear - currentYear;

        if (yearRem !== 0) {
            yearD = yearD + 1;
        }

        for (let year = 0; year < yearD; year++) {
            let currMonth;
            let currYear;
            if (year === 0) {
                currMonth = this.currentMonth;
                currYear = new Date().getFullYear();
            } else {
                currMonth = 1;
                currYear = new Date().getFullYear() + year;
            }

            for (let m = currMonth; m <= 12; m++) {
                if (currYear === projectEndLastYear) {
                    if (m <= projectEndLastMonth) {
                        const tmObj = { ...obj };
                        tmObj.month = m;
                        tmObj.year = currYear;
                        timeLiveDataResult.push(tmObj);
                    }
                } else {
                    const tmObj = { ...obj };
                    tmObj.month = m;
                    tmObj.year = currYear;
                    timeLiveDataResult.push(tmObj);
                }
            }
        }
        return timeLiveDataResult;
    }

    constructProjectEffortData(): any[] {
        const timeLiveEffortData = [];
        const obj = {
            month: 0,
            year: undefined,
            efforts: undefined,
            manDays: undefined
        };

        if (this.projectDetails.startDate && this.projectDetails.endDate) {
            const projectDurationInMonth = TimeliveComponent.getMonthDiff(
                new Date(this.projectDetails.startDate),
                new Date(this.projectDetails.endDate)
            );
            const yearRem = projectDurationInMonth % 12;
            // let yearD = Math.floor(projectDurationInMonth / 12);

            const projectStartMonth = Math.floor(new Date(this.projectDetails.startDate).getMonth() + 1);
            const projectStartYear = new Date(this.projectDetails.startDate).getFullYear();
            const projectEndMonth = Math.floor(new Date(this.projectDetails.endDate).getMonth() + 1);
            const projectEndYear = new Date(this.projectDetails.endDate).getFullYear();

            let yearD = projectEndYear - projectStartYear;
            if (yearRem !== 0) {
                yearD = yearD + 1;
            }

            for (let year = projectStartYear; year <= projectEndYear; year++) {
                let startMonth;
                let startYear;
                if (year === projectStartYear) {
                    startMonth = projectStartMonth;
                    startYear = projectStartYear;
                } else {
                    startMonth = 1;
                    startYear = year;
                }

                for (let m = startMonth; m <= 12; m++) {
                    if (startYear === projectEndYear) {
                        if (m <= projectEndMonth) {
                            const tmObj = { ...obj };
                            tmObj.month = m;
                            tmObj.year = startYear;
                            timeLiveEffortData.push(tmObj);
                        }
                    } else {
                        const tmObj = { ...obj };
                        tmObj.month = m;
                        tmObj.year = startYear;
                        timeLiveEffortData.push(tmObj);
                    }
                }
            }
        }
        return timeLiveEffortData;
    }

    getTimeliveData() {
        const timeliveData = {
            ploeProjId: this.projectId
        };
        this.timeliveService.getTimeliveData(timeliveData).subscribe(pagedData => {
            if (pagedData.success && pagedData.data) {
                this.timeLiveData = pagedData.data.projectsLoe;
                this.constructTimeliveDataQtr(this.timeLiveData);
                this.mapTimeLiveDataMonthly(this.timeLiveData);
                this.mapTimeLiveEffortData(this.timeLiveData);
                this.timeliveDataQtr.total = parseFloat(pagedData.data.realocationAmount);
                this.timeliveDataQtr.balance =
                    parseFloat(pagedData.data.totalActual) >= parseFloat(pagedData.data.realocationAmount)
                        ? parseFloat(pagedData.data.totalActual) - parseFloat(pagedData.data.realocationAmount)
                        : parseFloat(pagedData.data.realocationAmount) - parseFloat(pagedData.data.totalActual);
                this.timeliveDataQtr.totalActual =
                    parseFloat(pagedData.data.totalActual) >= parseFloat(pagedData.data.realocationAmount)
                        ? parseFloat(pagedData.data.totalActual)
                        : parseFloat(pagedData.data.realocationAmount);
            } else {
                this.timeLiveData = [];
            }
        });
    }

    mapTimeLiveDataMonthly(data) {
        const newData = JSON.parse(JSON.stringify(data));
        newData.forEach(element => {
            const i = this.timeliveDataMonth.findIndex(e => e.month === element.month && e.year === element.year);
            if (i !== -1) {
                this.timeliveDataMonth[i] = element;
            }
        });
    }

    // TODO: Need to map effort data..If already exists
    mapTimeLiveEffortData(data) {
        const newData = JSON.parse(JSON.stringify(data));
        newData.forEach(element => {
            const i = this.timeliveEffortData.findIndex(e => e.month === element.month && e.year === element.year);
            if (i !== -1) {
                this.timeliveEffortData[i] = element;
            }
        });
    }

    constructTimeliveDataQtr(data) {
        for (let i = 0; i < data.length; i++) {
            if (data[i].month === 1 || data[i].month === 2 || data[i].month === 3) {
                this.timeliveDataQtr.Q1.percentage = this.timeliveDataQtr.Q1.percentage + data[i].percentage;
                this.timeliveDataQtr.Q1.quater = 1;
                this.timeliveDataQtr.Q1.year = data[i].year;
                if (data[i].month < this.currentMonth) {
                    this.timeliveDataQtr.Q1.amountUsdActual = this.timeliveDataQtr.Q1.amountUsdActual + parseFloat(data[i].amountUsdActual);
                } else {
                    this.timeliveDataQtr.Q1.amountUsdForcasted =
                        this.timeliveDataQtr.Q1.amountUsdForcasted + parseFloat(data[i].amountUsdForcasted);
                }
            } else if (data[i].month === 4 || data[i].month === 5 || data[i].month === 6) {
                this.timeliveDataQtr.Q2.percentage = this.timeliveDataQtr.Q2.percentage + data[i].percentage;
                this.timeliveDataQtr.Q2.quater = 2;
                this.timeliveDataQtr.Q2.year = data[i].year;
                if (data[i].month < this.currentMonth) {
                    this.timeliveDataQtr.Q2.amountUsdActual = this.timeliveDataQtr.Q2.amountUsdActual + parseFloat(data[i].amountUsdActual);
                } else {
                    this.timeliveDataQtr.Q2.amountUsdForcasted =
                        this.timeliveDataQtr.Q2.amountUsdForcasted + parseFloat(data[i].amountUsdForcasted);
                }
            } else if (data[i].month === 7 || data[i].month === 8 || data[i].month === 9) {
                this.timeliveDataQtr.Q3.percentage = this.timeliveDataQtr.Q3.percentage + data[i].percentage;
                this.timeliveDataQtr.Q3.quater = 3;
                this.timeliveDataQtr.Q3.year = data[i].year;
                if (data[i].month < this.currentMonth) {
                    this.timeliveDataQtr.Q3.amountUsdActual = this.timeliveDataQtr.Q3.amountUsdActual + parseFloat(data[i].amountUsdActual);
                } else {
                    this.timeliveDataQtr.Q3.amountUsdForcasted =
                        this.timeliveDataQtr.Q3.amountUsdForcasted + parseFloat(data[i].amountUsdForcasted);
                }
            } else if (data[i].month === 10 || data[i].month === 11 || data[i].month === 12) {
                this.timeliveDataQtr.Q4.percentage = this.timeliveDataQtr.Q4.percentage + data[i].percentage;
                this.timeliveDataQtr.Q4.quater = 4;
                this.timeliveDataQtr.Q4.year = data[i].year;
                if (data[i].month < this.currentMonth) {
                    this.timeliveDataQtr.Q4.amountUsdActual = this.timeliveDataQtr.Q4.amountUsdActual + parseFloat(data[i].amountUsdActual);
                } else {
                    this.timeliveDataQtr.Q4.amountUsdForcasted =
                        this.timeliveDataQtr.Q4.amountUsdForcasted + parseFloat(data[i].amountUsdForcasted);
                }
            }
        }
    }

    amountChanged(e) {
        this.diasbleTimeliveMonthBtn = false;
    }

    submitMonthlyData() {
        const data = {
            addtionalData: this.timeliveDataMonth,
            projId: this.projectId
        };

        // Make backend call to save data
        return this.timeliveService.saveAdditionalData(data).subscribe((res: any) => {
            if (res.success) {
                this.diasbleTimeliveMonthBtn = true;
                this.reloadMonthlyTimeliveData();
                this.customTosterService.showSuccess('Additional data saved successfully');
            } else {
                this.customTosterService.error(res.message);
            }
        });
    }

    cancelMonthlyData() {
        this.diasbleTimeliveMonthBtn = true;
        this.timeliveDataMonth = this.constructTimeliveDataMonthly();
        this.mapTimeLiveDataMonthly(this.timeLiveData);
    }

    submitEffortData() {
        const data = {
            effortsData: this.timeliveEffortData,
            projId: this.projectId
        };

        return this.timeliveService.saveEfforts(data).subscribe((res: any) => {
            if (res.success) {
                this.disableTimeliveEffortBtn = true;
                this.reloadTimeliveEffortData();
                this.customTosterService.showSuccess(res.message);
            } else {
                this.customTosterService.error(res.message);
            }
        });
    }

    cancelEffortData() {
        this.disableTimeliveEffortBtn = true;
        this.timeliveEffortData = this.constructProjectEffortData();
        this.mapTimeLiveEffortData(this.timeLiveData);
    }

    effortChanged(e) {
        this.disableTimeliveEffortBtn = false;
    }

    omitNegativeValues(e) {
        let k;
        k = e.charCode; // k = event.keyCode; (Both can be used)
        return k >= 48 && k <= 57;
    }

    reloadMonthlyTimeliveData() {
        const timeliveData = {
            ploeProjId: this.projectId
        };
        this.timeliveService.getTimeliveData(timeliveData).subscribe(pagedData => {
            if (pagedData.success && pagedData.data) {
                this.timeLiveData = pagedData.data.projectsLoe;
                this.mapTimeLiveDataMonthly(this.timeLiveData);
            } else {
                this.timeLiveData = [];
            }
        });
    }

    reloadTimeliveEffortData() {
        const timeliveData = {
            ploeProjId: this.projectId
        };
        this.timeliveService.getTimeliveData(timeliveData).subscribe(pagedData => {
            if (pagedData.success && pagedData.data) {
                this.timeLiveData = pagedData.data.projectsLoe;
                this.mapTimeLiveEffortData(this.timeLiveData);
            } else {
                this.timeliveEffortData = [];
            }
        });
    }

    importFile($event, id): void {
        this.readThis($event.target, id);
    }

    readThis(inputValue: any, id): void {
        const file: File = inputValue.files[0];
        if (file.type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            this.customTosterService.error('Only xlsx files are supported.');
            return;
        }
        const myReader: FileReader = new FileReader();

        myReader.onloadend = e => {
            const strresult = myReader.result.toString();
            const base64 = strresult.split('base64,');
            const prodata = {
                fileValue: base64[1],
                projId: this.projectId
            };
            this.ngxService.start();
            this.timeliveService.importTimelive(prodata).subscribe(
                result => {
                    if (result.success && result.data) {
                        this.customTosterService.showSuccess(result.message);

                        // TODO: After success need to refresh timelive (call getTimeliveData())
                        this.reInitializeTimeliveQtrData();
                    } else if (result && !result.success) {
                        this.customTosterService.error(result.message);
                    } else {
                        this.customTosterService.error(result.message);
                    }
                    document.getElementById(id)['value'] = '';
                    this.ngxService.stop();
                },
                error => {
                    this.customTosterService.error('Something went wrong');
                    this.ngxService.stop();
                }
            );
        };
        myReader.readAsDataURL(file);
    }

    reInitializeTimeliveQtrData() {
        this.timeliveDataQtr = {
            Q1: {
                amountUsdActual: 0,
                amountUsdForcasted: 0,
                quater: 1,
                percentage: 0,
                year: 0
            },
            Q2: {
                amountUsdActual: 0,
                amountUsdForcasted: 0,
                quater: 2,
                percentage: 0,
                year: 0
            },
            Q3: {
                amountUsdActual: 0,
                amountUsdForcasted: 0,
                quater: 3,
                percentage: 0,
                year: 0
            },
            Q4: {
                amountUsdActual: 0,
                amountUsdForcasted: 0,
                quater: 4,
                percentage: 0,
                year: 0
            },
            total: 0,
            balance: 0,
            totalActual: 0
        };
        this.getTimeliveData();
    }
}
