import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BillingService } from '../../../../../../shared/services/billing.service';
import { ToasterService } from '../../../../../../shared';
import { CONSTANTS } from '../../../../../../shared/constant';

@Component({
    selector: 'app-dependencies-modal',
    templateUrl: './dependencies-modal.component.html',
    styleUrls: ['./dependencies-modal.component.css']
})
export class DependenciesModalComponent implements OnInit {
    @Input() public dependencies: any[];
    @Input() dependencyTypes = [];
    @Output() passEntry: EventEmitter<any> = new EventEmitter();
    public myForm: FormGroup;
    selectedDependencyTypes = [0];
    dependencyDetail = {
        dependency: '',
        dependencyId: '',
        details: [{ crnNo: '', resolutionDate: '', remarks: '', caId: '' }]
    };

    dependencyOwnerId: any[] = [];
    dependencyOwnerList = [];

    isSpclChar;

    constructor(
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
        private billingService: BillingService,
        private customToasterService: ToasterService
    ) {}

    ngOnInit() {
        const formGroups = {};
        if (this.dependencyTypes && this.dependencyTypes.length > 0) {
            // tslint:disable-next-line: prefer-for-of
            for (let index = 0; index < this.dependencyTypes.length; index++) {
                const depType = this.dependencyTypes[index];
                let dependencyName;
                switch (depType.dependencyId) {
                    case 1:
                        dependencyName = 'softwareDependency';
                        break;
                    case 2:
                        dependencyName = 'hardwareDependency';
                        break;
                    case 3:
                        dependencyName = 'customerDependency';
                        break;
                    case 4:
                        dependencyName = 'paperworkDependency';
                        break;
                    default:
                        dependencyName = String(depType.dependencyId);
                }
                formGroups[dependencyName] = this.fb.group({
                    dependency: [depType.dependencyName],
                    dependencyId: [depType.dependencyId],
                    dependencyOwnerId: [''],
                    details: this.fb.array([this.initDependencyDetails()])
                });
            }
        }

        this.myForm = this.fb.group(formGroups);
        this.constructSelectedDependencyTypes();

        this.getDependencyOwnerList();
    }

    initializeDependencyOwnerId() {
        if (this.dependencies) {
            for (let i = 0; i < this.dependencies.length; i++) {
                const depId = Number(this.dependencies[i].dependencyId);
                const depOwnerId = Number(this.dependencies[i].dependencyOwnerId);
                if (depId === 1) {
                    this.dependencyOwnerId[0] = depOwnerId;
                } else if (depId === 2) {
                    this.dependencyOwnerId[1] = depOwnerId;
                } else if (depId === 3) {
                    this.dependencyOwnerId[2] = depOwnerId;
                } else if (depId === 4) {
                    this.dependencyOwnerId[3] = depOwnerId;
                } else {
                    this.dependencyOwnerId[depId] = depOwnerId;
                }
            }
        }
    }

    passBack() {
        this.dependencies = [];
        const myFormValue = this.myForm.value;
        const isFormChanged = this.myForm.pristine;
        Object.keys(myFormValue).forEach(key => {
            this.dependencies.push(myFormValue[key]);
        });
        this.myForm.reset();
        const result = {
            dependencies: this.dependencies,
            isDependencyChanged: isFormChanged
        };
        this.activeModal.close(result);
    }

    dependencyOwner(event, dependencyType, depOwnerId) {
        const depType = Number(dependencyType);
    }

    addDependencyOwner(depOwnerID, depType) {
        if (depType === 1) {
            this.myForm.controls.softwareDependency['controls'].dependencyOwnerId.setValue(depOwnerID);
        } else if (depType === 2) {
            this.myForm.controls.hardwareDependency['controls'].dependencyOwnerId.setValue(depOwnerID);
        } else if (depType === 3) {
            this.myForm.controls.customerDependency['controls'].dependencyOwnerId.setValue(depOwnerID);
        } else if (depType === 4) {
            this.myForm.controls.paperworkDependency['controls'].dependencyOwnerId.setValue(depOwnerID);
        }
    }

    addSoftwareDependency() {
        if (!this.isLastSoftwareDependencyIsEmpty()) {
            const control = this.myForm.controls.softwareDependency['controls'].details as FormArray;
            control.push(this.initDependencyDetails());
        }
    }

    removeSoftwareDependency(index: number) {
        if (!this.isFirstSoftwareDependency()) {
            const control = this.myForm.controls.softwareDependency['controls'].details as FormArray;
            control.removeAt(index);
        }
    }

    isFirstSoftwareDependency(): boolean {
        const controls = this.myForm.controls.softwareDependency['controls'].details as FormArray;
        const length = controls.length;
        return length === 1;
    }

    isLastSoftwareDependencyIsEmpty(): boolean {
        const controls = this.myForm.controls.softwareDependency['controls'].details as FormArray;
        const length = controls.length;
        const lastSoftwareDependency = controls.value[length - 1];
        return lastSoftwareDependency.crnNo === '' && lastSoftwareDependency.resolutionDate === '' && lastSoftwareDependency.remarks === '';
    }

    constructSelectedDependencyTypes() {
        if (this.dependencies) {
            this.selectedDependencyTypes = [];
            const myFormValue = {};

            this.dependencies.forEach(dependency => {
                switch (Number(dependency.dependencyId)) {
                    case 1:
                        if (dependency.details.length > 0) {
                            const softwareDependency = dependency.details[0];
                            if (softwareDependency.crnNo || softwareDependency.resolutionDate || softwareDependency.remarks) {
                                this.generateSelectedDependencyTypes(1);
                                myFormValue['softwareDependency'] = dependency;
                                const controls = this.myForm.controls.softwareDependency['controls'].details as FormArray;
                                for (let i = 1; i < dependency.details.length; i++) {
                                    controls.push(this.initDependencyDetails());
                                }
                            }
                        }
                        break;
                    case 2:
                        if (dependency.details.length > 0) {
                            const hardwareDependency = dependency.details[0];
                            if (hardwareDependency.resolutionDate || hardwareDependency.remarks) {
                                this.generateSelectedDependencyTypes(2);
                                myFormValue['hardwareDependency'] = dependency;
                            }
                        }
                        break;
                    case 3:
                        if (dependency.details.length > 0) {
                            const customerDependency = dependency.details[0];
                            if (customerDependency.remarks) {
                                this.generateSelectedDependencyTypes(3);
                                myFormValue['customerDependency'] = dependency;
                            }
                        }
                        break;
                    case 4:
                        const paperworkDependency = dependency.details[0];
                        if (paperworkDependency.resolutionDate || paperworkDependency.remarks) {
                            this.generateSelectedDependencyTypes(4);
                            myFormValue['paperworkDependency'] = dependency;
                        }
                        break;
                    default:
                        if (dependency.details.length > 0) {
                            if (dependency.details[0].remarks) {
                                this.generateSelectedDependencyTypes(dependency.dependencyId);
                                myFormValue[String(dependency.dependencyId)] = dependency;
                            }
                        }
                        break;
                }
            });
            if (!(Object.entries(myFormValue).length === 0 && myFormValue.constructor === Object)) {
                this.myForm.patchValue(myFormValue);
            }
            if (this.selectedDependencyTypes.length === 0) {
                this.selectedDependencyTypes.push(0);
            }
        }
    }

    addMore(dependencyId: number) {
        if (dependencyId) {
            this.selectedDependencyTypes.push(0);
            this.dependencyTypes.forEach(type => {
                if (type.dependencyId === Number(dependencyId)) {
                    type.isSelected = true;
                }
            });
        }
    }

    generateSelectedDependencyTypes(dependencyId: number) {
        if (dependencyId) {
            this.selectedDependencyTypes.push(dependencyId);
            this.dependencyTypes.forEach(type => {
                if (type.dependencyId === Number(dependencyId)) {
                    type.isSelected = true;
                }
            });
        }
    }

    shouldDisableDependencyDropDown(index: number) {
        if (this.selectedDependencyTypes.length > 1) {
            if (index === 0) {
                return true;
            }
            return index !== this.selectedDependencyTypes.length - 1 ? true : null;
        }
        return null;
    }

    getDependencyValueByName(dependencyId): string {
        let value = '';
        this.dependencyTypes.forEach(dependency => {
            if (dependency.dependencyId === Number(dependencyId)) {
                value = dependency.dependencyName;
            }
        });
        return value;
    }

    private initDependencyDetails() {
        return this.fb.group({
            crnNo: [''],
            resolutionDate: [''],
            remarks: [''],
            caId: ''
        });
    }

    getDependencyOwnerList() {
        this.billingService.getDependencyOwnerList().subscribe((res: any) => {
            if (res.success) {
                this.dependencyOwnerList = res.data.userlist;
            } else {
                this.customToasterService.error(res.message);
            }
        });
    }

    onRemarksChange(id, value?: any, index?: any) {
        const idName = 'remarks-error-msg' + index;
        const re = CONSTANTS.regex.preventSpecialChar;
        this.isSpclChar = re.test((document.getElementById(idName) as HTMLInputElement).value);
        if (this.isSpclChar) {
            document.getElementById(idName).style.visibility = 'visible';
        } else {
            document.getElementById(idName).style.visibility = 'hidden';
        }
        return this.isSpclChar;
    }

    omitSpecialChar(event) {
        let k;
        k = event.charCode; // k = event.keyCode; (Both can be used)
        return (k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32 || k === 44 || k === 46 || (k >= 48 && k <= 57);
    }

    getDependencyName(id: any): string {
        if (this.dependencyTypes && this.dependencyTypes.length > 0) {
            const dep = this.dependencyTypes.find(dependency => dependency.dependencyId === Number(id));
            if (dep) {
                return dep.dependencyName;
            } else {
                return null;
            }
        }
    }

    isOtherDependency(dependencyType): boolean {
        return dependencyType != 0 && dependencyType != 1 && dependencyType != 2 && dependencyType != 3 && dependencyType != 4;
    }
}
