import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './projects/projects.component';
import { AuthGuard } from '../../core/guards/auth/auth.guard';
import { AuthorizationGuard } from '../../core/guards/auth/authorization.guard';
import { ProjectDetailsComponent } from './project-details/project-details.component';
const routes: Routes = [
    { path: 'list', component: ProjectsComponent },
    {
        path: 'summary/:id',
        component: ProjectDetailsComponent,
        children: [
            { path: 'billing', loadChildren: './billing/billing.module#BillingModule', canActivate: [AuthGuard, AuthorizationGuard] },
            { path: 'revenue', loadChildren: './revenue/revenue.module#RevenueModule', canActivate: [AuthGuard, AuthorizationGuard] },
            { path: 'reports', loadChildren: './reports/reports.module#ReportsModule', canActivate: [AuthGuard, AuthorizationGuard] },
            {
                path: 'deal-distribution',
                loadChildren: './deal-distribution/deal-distribution.module#DealDistributionModule',
                canActivate: [AuthGuard, AuthorizationGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProjectsRoutingModule {}
