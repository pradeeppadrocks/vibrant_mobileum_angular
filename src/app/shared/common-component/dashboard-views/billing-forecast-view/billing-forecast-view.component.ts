import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ToasterService } from 'src/app/shared/services/toaster.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../../../services/dashboard.service';
import { Router } from '@angular/router';
import { ChartExportService, RegionChangeService } from '../../..';

@Component({
    selector: 'app-billing-forecast-view',
    templateUrl: './billing-forecast-view.component.html',
    styleUrls: ['./billing-forecast-view.component.css']
})
export class BillingForecastViewComponent implements OnInit {
    Highcharts: typeof Highcharts = Highcharts; // Highcharts, it's Highcharts
    lobChartOptions = {};
    loboneToOneFlag = false;
    billingLOBCategoryList = [];
    billingLOBList = [];
    updateLOBViewChart = false;

    forcastChartOptions = {};
    forcastoneToOneFlag = false;
    updateForcastViewChart = false;

    dependencyChartOptions = {};
    dependencyoneToOneFlag = false;
    updateDependencyViewChart = false;
    showDependencyChart = true;
    showBacklogChart = true;
    showForecastChart = true;

    // ragChartOptions = {};
    ragoneToOneFlag = false;
    updateRAGViewChart = false;
    showBillingBacklogPie;
    showDependencyPie;
    showBillingLob;

    billingLobTimestamp;
    constructor(
        private router: Router,
        private dashboardservice: DashboardService,
        private toasterservice: ToasterService,
        private regionUpdateService: RegionChangeService,
        public chartExportService: ChartExportService
    ) {}

    ngOnInit() {
        this.loadHistoricChartOptions();
        this.pageLoadBinding();

        this.regionUpdateService.region.subscribe(value => {
            this.refreshPage();
        });
    }

    refreshPage() {
        this.pageLoadBinding();
    }

    pageLoadBinding() {
        //  this.ngxService.start();
        this.dashboardservice.getBillingForcastedLOB().subscribe(
            result => {
                if (result.success && result.data) {
                    result.data.reportData.forEach(ele => {
                        this.billingLOBCategoryList = [];
                        for (var i in ele.value) {
                            this.billingLOBCategoryList.push(ele.value[i].name);
                        }
                    });
                    this.billingLOBList = this.bindingGridData(result.data.reportData, this.billingLOBCategoryList);
                    this.lobChartOptions['xAxis'].categories = this.billingLOBCategoryList;
                    this.lobChartOptions['series'] = this.bindingChartData(result.data.reportData);
                    this.billingLobTimestamp = this.getTimestampDiffInHours(result.data.timestamp);
                    this.updateLOBViewChart = true;
                    this.loboneToOneFlag = true;
                    this.showBillingLob = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //    this.ngxService.stop();
            }
        );

        this.dashboardservice.getBillingForcastedBacklogPie().subscribe(
            result => {
                if (result.success && result.data) {
                    var piechartarray = [];
                    result.data.forEach(ele => {
                        var temparray = [];
                        temparray.push(ele.name);
                        temparray.push(ele.value);
                        piechartarray.push(temparray);
                    });
                    this.showForecastChart = this.showBillingPiechartData(piechartarray);
                    this.forcastChartOptions['series'][0].data = piechartarray;
                    this.updateForcastViewChart = true;
                    this.forcastoneToOneFlag = true;
                    this.showBillingBacklogPie = true;
                }
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //  this.ngxService.stop();
            }
        );

        this.dashboardservice.getBillingForcastedDependenciesPie().subscribe(
            result => {
                if (result.success && result.data) {
                    var piechartarray = [];
                    result.data.forEach(ele => {
                        var temparray = [];
                        temparray.push(ele.name);
                        temparray.push(ele.value);
                        piechartarray.push(temparray);
                    });
                    this.showDependencyChart = this.showBillingPiechartData(piechartarray);
                    this.dependencyChartOptions['series'][0].data = piechartarray;
                    this.updateDependencyViewChart = true;
                    this.dependencyoneToOneFlag = true;
                    this.showDependencyPie = true;
                    // this.ngxService.stop();
                }
                //  this.ngxService.stop();
            },
            error => {
                this.toasterservice.error('Something went wrong');
                //  this.ngxService.stop();
            }
        );
    }

    loadHistoricChartOptions() {
        this.lobChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },
            chart: {
                type: 'line',
                backgroundColor: 'transparent'
                // renderTo: 'billing-historical-chart',
            },
            title: {
                text: 'Line Of Bussiness'
            },

            subtitle: {
                text: ''
            },
            xAxis: {
                categories: []
            },
            yAxis: {
                title: {
                    text: 'Amount ($)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                formatter() {
                    let seriesSpan = '';

                    this.points.forEach(point => {
                        if (point.y > 1000000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000000, 2) +
                                'M' +
                                '<br />';
                        } else if (point.y > 1000) {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y / 1000, 2) +
                                'K' +
                                '<br />';
                        } else {
                            seriesSpan +=
                                '<span style="color: ' +
                                point.color +
                                ';">' +
                                point.series.name +
                                ': </span>' +
                                '$' +
                                Highcharts.numberFormat(point.y, 2) +
                                '<br />';
                        }
                    });
                    return '<span style="font-size:10px">' + this.points[0].key + '<br /></span><table>' + seriesSpan + '<table></table>';
                },
                shared: true,
                useHTML: true
            },
            legend: {
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                labelFormatter() {
                    const seriesData = this.chart.series;
                    let grandTotal = 0;
                    seriesData.forEach(series => {
                        const yDataSum = series.yData.reduce((a, b) => a + b, 0);
                        grandTotal = grandTotal + yDataSum;
                    });

                    const currentYDataSum = this.yData.reduce((a, b) => a + b, 0);
                    const percentile = ((currentYDataSum / grandTotal) * 100).toFixed(2);
                    return grandTotal ? `${this.name} (${percentile} %)` : `${this.name}  (NA)`;
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointWidth: 20,
                    dataLabels: {
                        enabled: true,
                        formatter() {
                            if (this.point.y > 1000000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000000, 2) + 'M';
                            } else if (this.point.y > 1000) {
                                return '$' + Highcharts.numberFormat(this.point.y / 1000, 2) + 'K';
                            } else {
                                return '$' + Highcharts.numberFormat(this.point.y, 2);
                            }
                        }
                    }
                }
            },

            series: [],
            responsive: {
                rules: [
                    {
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }
                ]
            }
        };

        this.forcastChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },

            chart: {
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Billing Forcast'
            },
            tooltip: {
                /*pointFormat: '{series.name}: <b>${point.y:.1f}</b>'*/
                formatter() {
                    let seriesSpan = '';

                    if (this.point.y > 1000000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000000, 2) +
                            'M' +
                            '<br />';
                    } else if (this.point.y > 1000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000, 2) +
                            'K' +
                            '<br />';
                    } else {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y, 2) +
                            '<br />';
                    }
                    return seriesSpan + '<table></table>';
                }
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',

                    dataLabels: {
                        enabled: false
                    },

                    showInLegend: true
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    return `${this.name} (${this.percentage.toFixed(2)} %)`;
                }
            },
            series: [
                {
                    type: 'pie',
                    name: 'Forcast',
                    data: []
                }
            ]
        };

        this.dependencyChartOptions = {
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                    }
                }
            },

            chart: {
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Billing Dependencies'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                formatter() {
                    let seriesSpan = '';

                    if (this.point.y > 1000000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000000, 2) +
                            'M' +
                            '<br />';
                    } else if (this.point.y > 1000) {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y / 1000, 2) +
                            'K' +
                            '<br />';
                    } else {
                        seriesSpan +=
                            '<span style="color: ' +
                            this.point.color +
                            ';">' +
                            this.point.name +
                            ': </span>' +
                            '$' +
                            Highcharts.numberFormat(this.point.y, 2) +
                            '<br />';
                    }
                    return seriesSpan + '<table></table>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',

                    dataLabels: {
                        enabled: false
                    },

                    showInLegend: true
                }
            },
            legend: {
                enabled: true,
                labelFormatter() {
                    return `${this.name} (${this.percentage.toFixed(2)} %)`;
                }
            },
            series: [
                {
                    type: 'pie',
                    name: 'Dependencies',
                    data: []
                }
            ]
        };
    }

    bindingChartData(data) {
        var list = [];
        data.forEach(ele => {
            var tempdata = { name: ele.name, data: [] };
            for (var i in ele.value) {
                tempdata.data.push(ele.value[i].value);
            }
            list.push(tempdata);
        });
        return list;
    }

    bindingGridData(data, categories) {
        var list = [];
        var totalarr = [];
        data.forEach(ele => {
            var tempdata = { name: ele.name, data: [] };
            var total = 0;
            for (var i in ele.value) {
                tempdata.data.push(ele.value[i].value);
                total += ele.value[i].value;
            }
            tempdata.data.push(total);
            list.push(tempdata);
        });

        for (var i in categories) {
            var temptotal = 0;
            data.forEach(ele => {
                for (var j in ele.value) {
                    if (categories[i] == ele.value[j].name) {
                        temptotal += ele.value[j].value;
                    }
                }
            });
            totalarr.push(temptotal);
        }
        list.push({ name: '', data: totalarr });
        return list;
    }

    redirectBillingBacklog() {
        this.router.navigate(['dashboard-billing-backlog-view']);
    }

    showBillingPiechartData(data) {
        let flag = false;
        data.some(ele => {
            if (ele[1] !== 0) {
                flag = true;
                return flag;
            }
        });
        return flag;
    }

    getTimestampDiffInHours(t2) {
        const date1 = +new Date();
        const date2 = +new Date(parseInt(t2, 10));

        const diff = Math.abs(date1 - date2) / 1000;

        // get total days between two dates
        const days = Math.floor(diff / 86400);

        // get hours
        const hours = Math.floor(diff / 3600) % 24;

        // get minutes
        const minutes = Math.floor(diff / 60) % 60;

        // get seconds
        const seconds = diff % 60;

        if (hours > 0) {
            return hours + ' hrs ' + minutes + ' minutes';
        } else if (minutes > 0) {
            return minutes + ' minutes';
        } else if (seconds > 0) {
            return seconds + ' seconds';
        }
    }
}
