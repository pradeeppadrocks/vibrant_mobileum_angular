import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwDepComponent } from './sw-dep.component';

describe('SwDepComponent', () => {
  let component: SwDepComponent;
  let fixture: ComponentFixture<SwDepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwDepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwDepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
