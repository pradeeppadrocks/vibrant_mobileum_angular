import { Injectable } from '@angular/core';
import { BaseService } from './../../../shared/services/base.service';
import {
    ForcastedDealDistribution,
    LatestForcastedDealDistribution,
    DeleteMilestone,
    EditDealDistribution,
    ProjectDetails,
    ImportDeal,
    FinalDeal,
    ApproveDeal,
    RejectDeal,
    ProjectType
} from './deal-distribution.model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class DealDistributionService {
    //DevURL = 'http://192.168.1.142:9051/';
    DevURL = environment.host;

    //Deal Distribution URL
    getForcastedDealDistributionListURL = `${this.DevURL}getForcastedDealDistribution`;
    getLatestForcastedDealDistributionURL = `${this.DevURL}getLatestForcastedDealDistribution`;
    addForcastedDealDistributionURL = `${this.DevURL}addForcastedDealDistribution`;
    updateForcastedDealDistributionURL = `${this.DevURL}updateForcastedDealDistribution`;
    deleteForcastedDealDistributionURL = `${this.DevURL}deleteForcastedDealDistribution`;
    getProjectDetailURL = `${this.DevURL}getProjectDetail`;
    importDealDistributionURL = `${this.DevURL}importDealDistribution`;
    finalizedDealDistributionURL = `${this.DevURL}finalizedDealDistribution`;
    approvedDealDistributionURL = `${this.DevURL}approvedDealDistribution`;
    rejectDealDistributionURL = `${this.DevURL}rejectDealDistribution`;
    getAllProjectsTypesURL = `${this.DevURL}getAllProjectsTypes`;

    constructor(private baseService: BaseService) {}

    //Deal Distribution API
    getForcastedDealDistributionList(data: ForcastedDealDistribution) {
        return this.baseService.post(this.getForcastedDealDistributionListURL, data);
    }

    getLatestForcastedDealDistribution(data: LatestForcastedDealDistribution) {
        return this.baseService.post(this.getLatestForcastedDealDistributionURL, data);
    }

    addUpdateDealDistributionMilestone(data: EditDealDistribution) {
        if (data.ddId) {
            return this.baseService.post(this.updateForcastedDealDistributionURL, data);
        } else {
            return this.baseService.post(this.addForcastedDealDistributionURL, data);
        }
    }

    deleteForcastedDealDistribution(data: DeleteMilestone) {
        return this.baseService.delete(this.deleteForcastedDealDistributionURL, data);
    }

    getProjectDetails(data: ProjectDetails) {
        return this.baseService.post(this.getProjectDetailURL, data);
    }

    importDealDistribution(data: ImportDeal) {
        return this.baseService.post(this.importDealDistributionURL, data);
    }

    finalizedDealDistribution(data: FinalDeal) {
        return this.baseService.post(this.finalizedDealDistributionURL, data);
    }

    approvedDealDistribution(data: ApproveDeal) {
        return this.baseService.post(this.approvedDealDistributionURL, data);
    }

    rejectDealDistribution(data: RejectDeal) {
        return this.baseService.post(this.rejectDealDistributionURL, data);
    }

    getAllProjectsTypesList(data: ProjectType) {
        return this.baseService.post(this.getAllProjectsTypesURL, data);
    }
}
