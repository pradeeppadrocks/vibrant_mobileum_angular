import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../services/billing.service';
import { ApprovalService, ToasterService } from '../..';
import { ProjectDetails } from '../../../modules/projects/project.model';
import { ProjectService } from '../../../modules/projects/project.service';

@Component({
    selector: 'app-approve-modal',
    templateUrl: './approve-modal.component.html',
    styleUrls: ['./approve-modal.component.css']
})
export class ApproveModalComponent implements OnInit {
    @Input() milestoneData;
    @Input() userLevelType;
    private forecastedMilestoneAcceptDisabledValue;
    private forecastedMilestoneRejectDisabledValue;
    private forecastedMilestoneSendToDisabledValue;
    projectDetails;
    isApproved;
    constructor(
        public activeModal: NgbActiveModal,
        private modalService: NgbModal,
        private billingService: BillingService,
        private customToasterservice: ToasterService,
        private approvalService: ApprovalService
    ) {}

    ngOnInit() {
        this.getProjectDetailsById(this.milestoneData.bmProjID);
    }

    getProjectDetailsById(projId) {
        // Get Project Details
        const prodata = {
            projId
        };
        this.approvalService.getProjectDetails(prodata).subscribe(
            result => {
                if (result.success && result.data) {
                    this.projectDetails = result.data;
                }
            },
            error => {
                this.customToasterservice.error('Something went wrong');
            }
        );
    }

    accepetLevel(msId, userTypeLevel, index, msType) {
        this.billingService.acceptMilestone(msId, userTypeLevel).subscribe((res: any) => {
            if (res.success) {
                this.customToasterservice.showSuccess(res.message);
                this.forecastedMilestoneAcceptDisabledValue = true;
                this.isApproved = res;
            } else {
                this.customToasterservice.error(res.message);
            }
        });
    }

    sendToLevel2(msId, userTypeLevel, index, msType) {
        this.billingService.sendToLevel2(msId, userTypeLevel).subscribe((res: any) => {
            if (res.success) {
                this.customToasterservice.showSuccess(res.message);
                this.forecastedMilestoneAcceptDisabledValue = true;
                this.isApproved = res;
            } else {
                this.customToasterservice.error(res.message);
            }
        });
    }

    rejectMilestone(msId, userTypeLevel, index, msType) {
        this.billingService.rejectMilestoneLevel(msId, userTypeLevel).subscribe((res: any) => {
            if (res.success) {
                this.customToasterservice.showSuccess(res.message);
                this.forecastedMilestoneAcceptDisabledValue = true;
                this.isApproved = res;
            } else {
                this.customToasterservice.error(res.message);
            }
        });
    }

    shouldDisplayApprovalButton(data: any): boolean {
        if (data.bmApprovalStatus === 0 && data.bmFinalized === 1) {
            if (this.userLevelType === 1 && !data.bmLevel1Approver) {
                return true;
            } else if (this.userLevelType === 2 && !data.bmLevel2Approver) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    closeModal() {
        this.activeModal.dismiss(this.isApproved);
    }
}
